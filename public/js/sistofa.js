/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

/**
 *  Fichero con funciones en JavaScript para controlar determinados comportamientos
 *  de la interfaz de usuario.
 *  
 *  @author Antonio Gómez <i52goloa@uco.es>
 * 
 */

//intervalo de tiempo (milisegundos) entre cada comprobación por ajax de las alertas de sercicios
const INTERVALO_ACTUALIZAR_ALERTAS_SERVICIOS = 15000; 


$(document).ready(function () {

    /*
     * VENTANA MODAL
     *
     * Al hacer clic en un elemento con la clase .btn-modal muestra el elemento
     * cuyo ID esté relacionado con el clicado (cambiando ".btn" por ".modal").
     * Para múltiples botones que abran el mismo modal se puede usar .btn1, .btn2, etc.
     *
     * Ejemplo: views/aplicacion/listados_editar.html.twig
     */
    /* Abre la ventana modal al clicar el botón */
    $('.btn-modal').on('click', function () {
        var boton_id = $(this).attr('id').replace('btn', 'modal').split('-');
        var modal_id = '#modal-' + boton_id[1]
        $(modal_id).css('display', 'block');

        // Bloquea el scroll del navegador
        $('body').addClass('stop-scrolling');
    });

    /* Cierra la ventana modal al clicar el botón de la esquina */
    $('.modal-cerrar').on('click', function () {
        $(this).parent().parent().css('display', 'none');

        // Bloquea el scroll del navegador
        $('body').removeClass('stop-scrolling');
    });

    /* Cierra la ventana modal al clicar en el fondo */
    $('body').on('click', '.modal', function (e) {
        if ($(e.target).is(".modal")) {
            $(this).css('display', 'none'); // Oculta la ventana modal al hacer clic en el fondo

            // Bloquea el scroll del navegador
            $('body').removeClass('stop-scrolling');
        }
    });


    /*
     * FLASH SECUNDARIO
     *
     * Si está definida la capa .flash-secundario replica el mensaje flash.
     * Esto se usa para formularios secundarios que al enviarse son redirigidos
     * a una parte inferior de la página con puntos de ancla y no se puede
     * visualizar correctamente los mensajes flash de la parte superior.
     *
     * Ejemplo: views/aplicacion/listados_editar.html.twig (edición de campos)
     */
    if ($('.flash-secundario')) {
        var $flash = $('.flash').clone();
        $('.flash-secundario').html($flash);
    }

    /*
     * PESTAÑAS - Sistema tabs
     */
    $('.sistema-tabs').each(function (i) {

        // Marca como seleccionada la primera pestaña
        $(this).find('.listado-tabs li:eq(0)').addClass('selected');
        $(this).find('.tabs > div:eq(0)').addClass('selected');

        // Clic en las pestañas
        $(this).find('.listado-tabs li').each(function (j) {
            $(this).on('click', function () {
                $(this).siblings().removeClass('selected');
                $(this).addClass('selected');
                $(this).parents('.sistema-tabs').find('.tabs > div').removeClass('selected');
                $(this).parents('.sistema-tabs').find('.tabs > div:eq(' + j + ')').addClass('selected');
            })
        });
    });

});

/**
 * Inicia y detiene la animación de loading en el contenedor indicado.
 * 
 * @param string contenedor mombre del contenedor sobre el que se 
 * 
 */
function iniciaLoading(contenedor) {

    if ($(contenedor + ' .loader').length == 0) {
        $(contenedor).append(' <div class="loader">  <div class="loader-wheel"></div>  <div class="loader-text"></div></div>');
    }

    $(contenedor + ' .loader').css('display', 'block');

}

/**
 * detiene la animación de loading en el contenedor indicado.
 * 
 * @param string contenedor mombre del contenedor sobre el que se 
 * 
 */
function detieneLoading(contenedor) {

    $(contenedor + ' .loader').css('display', 'none');

}


/**
 * Muestra activa la pestaña indicada con el formado idtabs--número. Ej: servicio_dependencias--2
 * 
 * @param string nombre de la pestaña a activar
 */
function activaTab(tab) {
    var tabID = tab.split('--');
    $('#' + tabID[0] + ' .listado-tabs li:eq(' + tabID[1] + ')').trigger('click');
}

/*
 * CAMBIAR ID EN URL
 *
 * Cambia el id de una url cuyo formado termina en /id
 */
function cambiaIdUrl(url, id) {
    // Elimina el ID actual
    var nuevaUrl = url.substr(0, url.lastIndexOf("\/") + 1);
    nuevaUrl += id;

    return nuevaUrl;
}

/*
 * NOMBRE DE USUARIO
 *
 * Devuelve el nombre de usuario con el formato NOMBRE APELLIDOS (LOGIN) a partir de un array
 */
function mostrarNombreUsuario(datos) {
    var nombreUsuario = '';
    if (datos['nombre']) {
        nombreUsuario = datos['nombre'];
    }
    if (datos['apellidos']) {
        if (nombreUsuario.length > 0) {
            nombreUsuario += ' ';
        }
        nombreUsuario += datos['apellidos'];
    }

    if (datos['login']) {
        if (nombreUsuario.length > 0) {
            nombreUsuario += ' ';
        }
        nombreUsuario += '(' + datos['login'] + ')';
    }

    return nombreUsuario;
}

/*
 * GUARDAR DATOS EN SESIÓN
 *
 * Llama a la función que guarda datos en sesión
 */
function guardaEnSesion(guarda_sesion_path, dato, valor) {
    $.ajax({url: guarda_sesion_path + '?dato=' + dato + '&valor=' + valor}).done(function (msg) {
        return msg;
    });
}


/*
 * MODAL CONFIRMACIÓN
 *
 * Modal que sustituye los modales de confirmación generados por el navegador.
 *
 * PARÁMETROS
 * contenido: Texto para el modal
 * ruta: ruta para el botón CONFIRMAR
 */
function modalConfirmacion(contenido, ruta) {
    if ($('#modal_confirmacion').length === 0) {
        $('body').append('<div id="modal_confirmacion" class="modal"><div class="modal-contenido"><div class="contenedor"></div><div class="botones"><a href="" class="aceptar boton boton-pequeno"><b>Aceptar</b></a><button class="boton-pequeno" onclick="modalConfirmacionCerrar();">Cancelar</button></div></div>');
    }

    $('#modal_confirmacion .contenedor').html(contenido);
    $('#modal_confirmacion a.aceptar').attr('href', ruta);

    $('#modal_confirmacion').css('display', 'block');
}

/*
 * MODAL CONFIRMACIÓN CERRAR
 *
 * Cierra la venta modal de confirmación.
 */
function modalConfirmacionCerrar() {
    $('#modal_confirmacion').css('display', 'none');
}

/*
 * BÚSQUEDA DE USUARIOS
 *
 * Obtiene datos de la búsqueda solicitada y lo imprime en la tabla del modal
 */
function busquedaUsuarios(url_busqueda) {
    // Vacía la tabla de resultados, elimina el mensaje y muestra Loading
    $('#modal-buscarafectado .contenedor table tbody').html('');
    $('#modal-buscarafectado .contenedor .mensaje').remove();

    var texto = $('#formbusquedausuario_nombre').val();

    if (texto.length < 3) {
        $('#modal-buscarafectado .contenedor').append('<div class="mensaje">Por favor, escriba al menos 3 caracteres</div>');
    } else {
        iniciaLoading('#modal-buscarafectado .contenedor');

        $.post(
                url_busqueda,
                {texto: texto},
                function (datos, status) {
                    detieneLoading('#modal-buscarafectado .contenedor');
                    if (datos.length == 0) {
                        $('#modal-buscarafectado .contenedor').append('<div class="mensaje">No se han encontrado resultados</div>');
                    } else {
                        datos.forEach(function (valor, indice, array) {
                            $('#modal-buscarafectado .contenedor table tbody').append('<tr onclick="seleccionaUsuario(' + valor['id'] + ');"><td>'
                                    + (valor['apellidos'] != null ? valor['apellidos'] : '') + '</td><td>'
                                    + (valor['nombre'] != null ? valor['nombre'] : '') + '</td><td>'
                                    + (valor['login'] != null ? valor['login'] : '') + '</td><td>'
                                    + (valor['unidad'] != null ? valor['unidad'] : '') + '</td><td>'
                                    + (valor['sede'] != null ? valor['sede'] : '') + '</td></tr>');
                        });
                    }
                }
        );
    }

    return false; // Evita que la página recargue
}


/**
 * Obtiene datos de la búsqueda solicitada y lo imprime en la tabla del modal para
 * seleccionar la dirección de correo a la que enviar la intervención.
 * 
 * @param {string} url_busqueda  url a la que envia el criterio de busqueda para buscar el email
 * 
 * @returns {boolean}  false para que la páqina no se recarge
 * 
 */
function busquedaUsuariosEmail(url_busqueda) {
    // Vacía la tabla de resultados, elimina el mensaje y muestra Loading
    $('#modal-buscaremail .contenedor table tbody').html('');
    $('#modal-buscaremail .contenedor .mensaje').remove();

    var texto = $('#formbusquedaemail_nombre').val();

    if (texto.length < 3) {
        $('#modal-buscaremail .contenedor').append('<div class="mensaje">Por favor, escriba al menos 3 caracteres</div>');
    } else {
        iniciaLoading('#modal-buscaremail .contenedor');

        $.post(
                url_busqueda,
                {texto: texto},
                function (datos, status) {
                    detieneLoading('#modal-buscaremail .contenedor');
                    if (datos.length == 0) {
                        $('#modal-buscaremail .contenedor').append('<div class="mensaje">No se han encontrado resultados</div>');
                    } else {
                        datos.forEach(function (valor, indice, array) {
                            $('#modal-buscaremail .contenedor table tbody').append('<tr onclick="seleccionaEmailIntervencion(' + valor['id'] + ');"><td>'
                                    + (valor['nombre'] != null ? valor['nombre'] : '') + (valor['apellidos'] != null ? ' ' + valor['apellidos'] : '') + '</td><td>'
                                    + (valor['email'] != null ? valor['email'] : '') + '</td><td>'
                                    + (valor['login'] != null ? valor['login'] : '') + '</td><td>'
                                    + (valor['unidad'] != null ? valor['unidad'] : '') + '</td><td>'
                                    + (valor['sede'] != null ? valor['sede'] : '') + '</td></tr>');
                        });
                    }
                }
        );
    }

    return false; // Evita que la página recargue
}

/*
 * Obtiene datos de la búsqueda solicitada y lo imprime en la tabla del modal para modificar el afectado de una solicitud
 * 
 * @param {String} modal_nombre      Nombre del div que contiene el modal con el formulario de busqueda.
 * @param {Strin} url_busqueda       Url a la que enviar el filtro de busqueda y devolvera un JSON con la información de los usuarios encontrados.
 * @param {String} funcion_retorno   funcion a la que enviar el ID del usuario seleccionado para obtener el resto de los datos y mostarlo.
 * 
 */
function busquedaUsuariosModal(modal_nombre, url_busqueda, funcion_retorno) {
    // Vacía la tabla de resultados, elimina el mensaje y muestra Loading
    $('#modal-' + modal_nombre + ' .contenedor table tbody').html('');
    $('#modal-' + modal_nombre + ' .contenedor .mensaje').remove();

    var texto = $('#form' + modal_nombre + '_nombre').val();
    var sede = $('#form' + modal_nombre + '_sede').val();
    var unidad = $('#form' + modal_nombre + '_unidad').val();

    if (texto.length < 3 && sede == "" && unidad == "") {
        $('#modal-' + modal_nombre + ' .contenedor').append('<div class="mensaje">Debe introducir algun criterio de busqueda.</div>');
    } else {
        iniciaLoading('#modal-' + modal_nombre + ' .contenedor');

        $.post(
                url_busqueda,
                {texto: texto,
                    sede: sede,
                    unidad: unidad},
                function (datos, status) {
                    detieneLoading('#modal-' + modal_nombre + ' .contenedor');
                    if (datos.length == 0) {
                        $('#modal-' + modal_nombre + ' .contenedor').append('<div class="mensaje">No se han encontrado resultados</div>');
                    } else {
                        datos.forEach(function (valor, indice, array) {
                            $('#modal-' + modal_nombre + ' .contenedor table tbody').append('<tr onclick="' + funcion_retorno + '(' + valor['id'] + ');"><td>'
                                    + (valor['apellidos'] != null ? valor['apellidos'] : '') + '</td><td>'
                                    + (valor['nombre'] != null ? valor['nombre'] : '') + '</td><td>'
                                    + (valor['login'] != null ? valor['login'] : '') + '</td><td>'
                                    + (valor['unidad'] != null ? valor['unidad'] : '') + '</td><td>'
                                    + (valor['sede'] != null ? valor['sede'] : '') + '</td><td>'
                                    + (valor['email'] != null ? valor['email'] : '') + '</td><td>'
                                    + (valor['nif'] != null ? valor['nif'] : '') + '</td></tr>');
                        });
                    }
                }
        );
    }

    return false; // Evita que la página recargue
}

/*
 * BÚSQUEDA DE SERVICIOS PARA EL FORMULARIO DE ACCESO
 *
 * Obtiene datos de la búsqueda solicitada y lo imprime en la tabla del modal
 */
function busquedaServiciosAcceso(url_busqueda) {
    // Vacía la tabla de resultados, elimina el mensaje y muestra Loading
    $('#modal-buscarservicioacceso .contenedor table tbody').html('');
    $('#modal-buscarservicioacceso .contenedor .mensaje').remove();

    var texto = $('#formbusquedaservicioacceso_nombre').val();

    if (texto.length < 3) {
        $('#modal-buscarservicioacceso .contenedor').append('<div class="mensaje">Por favor, escriba al menos 3 caracteres</div>');
    } else {
        iniciaLoading('#modal-buscarservicioacceso .contenedor');

        $.post(
                url_busqueda,
                {texto: texto},
                function (datos, status) {
                    detieneLoading('#modal-buscarservicioacceso .contenedor');
                    if (datos.length == 0) {
                        $('#modal-buscarservicioacceso .contenedor').append('<div class="mensaje">No se han encontrado resultados</div>');
                    } else {
                        datos.forEach(function (valor, indice, array) {
                            $('#modal-buscarservicioacceso .contenedor table tbody').append('<tr onclick="seleccionaServicioAcceso(' + valor['id_servicio'] + ');"><td>'
                                    + (valor['nombre'] != null ? valor['nombre'] : '') + '</td><td>'
                                    + (valor['finalidad'] != null ? valor['finalidad'] : '') + '</td></tr>');
                        });
                    }
                }
        );
    }

    return false; // Evita que la página recargue
}

/*
 * BÚSQUEDA DE SERVICIOS
 *
 * Obtiene datos de la búsqueda solicitada y lo imprime en la tabla del modal
 */
function busquedaServicios(url_busqueda) {
    // Vacía la tabla de resultados, elimina el mensaje y muestra Loading
    $('#modal-buscarservicio .contenedor table tbody').html('');
    $('#modal-buscarservicio .contenedor .mensaje').remove();

    var texto = $('#formbusquedaservicio_nombre').val();
    var finales = $('#formbusquedaservicio_finales').val();
    var grupo = $('#formbusquedaservicio_grupo').val();
    var tipo = $('#formbusquedaservicio_tipo').val();

    //No comprueba esto, siempre busca, no requiere minimo.
    /*
     if (texto.length < 3) {
     $('#modal-buscarservicio .contenedor').append('<div class="mensaje">Por favor, escriba al menos 3 caracteres</div>');
     } else {
     */
    iniciaLoading('#modal-buscarservicio .contenedor');

    $.post(
            url_busqueda,
            {texto: texto,
                finales: finales,
                tipo: tipo,
                grupo: grupo},
            function (datos, status) {
                detieneLoading('#modal-buscarservicio .contenedor');
                if (datos.length == 0) {
                    $('#modal-buscarservicio .contenedor').append('<div class="mensaje">No se han encontrado resultados</div>');
                } else {
                    datos.forEach(function (valor, indice, array) {
                        $('#modal-buscarservicio .contenedor table tbody').append('<tr onclick="seleccionaServicio(' + valor['id_servicio'] + ');">'
                                + '<td>'
                                + (valor['nombre'] != null ? valor['nombre'] : '') + '</td><td>'
                                + (valor['grupo_servicios'] != null ? valor['grupo_servicios'] : '') + '</td><td>'
                                + (valor['finalidad'] != null ? valor['finalidad'] : '') + '</td></tr>');
                    });
                }
            }
    );


    return false; // Evita que la página recargue
}

/*
 * BÚSQUEDA DE LOS DETALLES DE LOS SERVICIOS
 *
 * Obtiene datos de la búsqueda solicitada y lo imprime en la tabla del modal
 */
function busquedaDetallesServicios(url_busqueda) {
    // Vacía la tabla de resultados, elimina el mensaje y muestra Loading
    $('#modal-buscardetalleservicio .contenedor table tbody').html('');
    $('#modal-buscardetalleservicio .contenedor .mensaje').remove();

    var texto = $('#formbusquedadetalleservicio_nombre_detalle').val();
    var id_servicio = $('#formbusquedadetalleservicio_id_servicio').val();

    //Deberia devolver pocos resultados, no es necesario limitar, se deja en cero
    if (texto.length < 0) {
        $('#modal-buscardetalleservicio .contenedor').append('<div class="mensaje">Por favor, escriba al menos 3 caracteres</div>');
    } else {
        iniciaLoading('#modal-buscardetalleservicio .contenedor');

        $.post(
                url_busqueda,
                {texto: texto, id_servicio: id_servicio},
                function (datos, status) {
                    detieneLoading('#modal-buscardetalleservicio .contenedor');
                    if (datos != null)
                    {
                        if (datos.length == 0) {
                            $('#modal-buscardetalleservicio .contenedor').append('<div class="mensaje">No se han encontrado resultados</div>');
                        } else {
                            datos.forEach(function (valor, indice, array) {
                                $('#modal-buscardetalleservicio .contenedor table tbody').append('<tr style="cursor: pointer;" onclick="seleccionaDetalleServicio(' + valor['id_detalle_servicio'] + ');"><td>'
                                        + (valor['detalle_servicio'] != null ? valor['detalle_servicio'] : '') + '</td><td>'
                                        + (valor['comentarios'] != null ? valor['comentarios'] : '') + '</td><td>'
                                        + (valor['nombre_servicio'] != null ? valor['nombre_servicio'] : '') + '</td><td>'
                                        + (valor['nombre_tipo'] != null ? valor['nombre_tipo'] : '') + '</td></tr>');
                            });
                        }
                    } else
                    {
                        $('#modal-buscardetalleservicio .contenedor').append('<div class="mensaje">No se han encontrado resultados</div>');
                    }
                }
        );
    }

    return false; // Evita que la página recargue
}


/*
 * BÚSQUEDA DE HARDWARE
 *
 * Obtiene datos de la búsqueda solicitada y lo imprime en la tabla del modal
 */
function busquedaHardware(url_busqueda) {
    // Vacía la tabla de resultados, elimina el mensaje y muestra Loading
    $('#modal-buscarhardware .contenedor table tbody').html('');
    $('#modal-buscarhardware .contenedor .mensaje').remove();

    var texto = $('#formbusquedahardware_nombre').val();
    var tipo = $('#formbusquedahardware_tipo').val();

    if (texto.length < 0) {
        $('#modal-buscarhardware .contenedor').append('<div class="mensaje">Por favor, escriba al menos 3 caracteres</div>');
    } else {
        iniciaLoading('#modal-buscarhardware .contenedor');

        $.post(
                url_busqueda,
                {texto: texto,
                    tipo: tipo},
                function (datos, status) {
                    detieneLoading('#modal-buscarhardware .contenedor');
                    if (datos.length == 0) {
                        $('#modal-buscarhardware .contenedor').append('<div class="mensaje">No se han encontrado resultados</div>');
                    } else {
                        datos.forEach(function (valor, indice, array) {
                            $('#modal-buscarhardware .contenedor table tbody').append('<tr onclick="seleccionaHardware(' + valor['idhardware'] + ');"><td>'
                                    + (valor['nombre'] != null ? valor['nombre'] : '') + '</td><td>'
                                    + (valor['nserie'] != null ? valor['nserie'] : '') + '</td><td>'
                                    + (valor['ninventario'] != null ? valor['ninventario'] : '') + '</td><td>'
                                    + (valor['marcamodelo'] != null ? valor['marcamodelo'] : '') + '</td><td>'
                                    + (valor['tipo'] != null ? valor['tipo'] : '') + '</td><td>'
                                    + (valor['estado'] != null ? valor['estado'] : '') + '</td></tr>');
                        });
                    }
                }
        );
    }

    return false; // Evita que la página recargue
}


/*
 * API NOTIFICACIONES SOPORTADA
 * 
 * Indica si el navegador soporta la API de notificaciones
 */
function apiNotificacionesSoportada()
{
    return ('Notification' in window);
}

function muestraNotificacion(titulo, cuerpo, tag)
{
    opciones = {
        body: cuerpo,
        tag: tag,
        icon: $('#ruta_favicon').html()
    };

    if (apiNotificacionesSoportada())
    {//si el navegador soporta la API de notificaciones:
        if (Notification.permission === "granted")
        {//si el usuario ha dado permiso a que se le muestren notificaciones:
            var notificacion = new Notification(titulo, opciones);
        } else if (Notification.permission !== "denied")
        {//si el usuario NO ha denegado el permiso a que se le muestren notificaciones:
            Notification.requestPermission(function (permission) {
                if (permission === "granted")
                {
                    var notificacion = new Notification(titulo, opciones);
                }
            });
        }
    }
}

/**
 * llama a la acción de eliminar la ejecución y todos los datos
 * 
 * @param {type} id
 * @returns void
 */
function eliminaResultados(id) {

    if (confirm('¿Esta seguro que desea eliminar esta ejecución?')) {
        // Delete it!
        console.log('Elimina la ejecucion' + id);
        document.location = "{{path('ejecuciones_eliminar',{" + id + ":''}) }}/" + id;
    } else {
        // Do nothing!

    }


}


/*
* VENTANA MODAL > FORMULARIO > EDITAR
*
* Rellena los campos del formulario de la ventana modal con información del elemento indicado.
* Establece la ruta de envío del formulario.
*
* PARÁMETROS
* modal_id: #id del div de la ventana modal
* data_url: ruta del JSON de donde obtiene los datos para rellenar el formulario
* action_url: ruta del action del formulario (guardar datos)
* delete_url: ruta de eliminación del elemento
* titulo: texto de encabezado de la ventana modal
*
* NOTA
* El id y el name del formulario tienen que ser el mismo.
*/
function modalFormEditar(modal_id, data_url, action_url, delete_url, titulo) {

	iniciaLoading('#'+modal_id+' .modal-contenido'); // Muestra animación de loading mientras cargan los datos

	$('#'+modal_id).css('display', 'block'); // Muestra la ventana modal
	$('#'+modal_id+' .modal-contenido .contenedor').css('display', 'none');
	$('#'+modal_id+' .modal-contenido > h3').text(titulo);
	$('#'+modal_id+' .eliminar-elemento').css('display', 'block');
	$('#'+modal_id+' .eliminar-elemento').attr('href', delete_url);
	$('#'+modal_id+' form').attr('action', action_url);

	var form_name = $('#'+modal_id).find('form').attr('name');

	$('#'+modal_id+' form').trigger("reset");
	$($('#'+modal_id+' form textarea')).html('');

	return $.getJSON(data_url, function(data) {
		$.each( data[0], function( key, val ) {
			if ($('#'+modal_id+ ' #'+form_name+'_'+key)) {

				// Comprueba el tipo de campo
				// [PENDIENTE] Falta comprobar si funciona para los checkbox
				if ($('#'+modal_id+ ' #'+form_name+'_'+key).prop("tagName") == 'TEXTAREA') { // El campo es un textarea
					$('#'+modal_id+ ' #'+form_name+'_'+key).html(val);
				} else {
					$('#'+modal_id+ ' #'+form_name+'_'+key).val(val); // Completa el formulario
				}
			}
		});

		detieneLoading('#'+modal_id);  // Detiene la animación de loading

		$('#'+modal_id+' .contenedor').css('display', 'block');

		return data; // Necesario para casos en los que se quiera ejecutar otra función a continuación
	});

}

/*
* VENTANA MODAL / FORMULARIO / CREAR
*
* Vacía los campos del formulario de la ventana modal con información del elemento indicado.
* Establece la ruta de envío del formulario.
*
* PARÁMETROS
* modal_id: #id del div de la ventana modal
* action_url: ruta del action del formulario (guardar datos)
* titulo: texto de encabezado de la ventana modal
*/
function modalFormCrear(modal_id, action_url, titulo) {
	// Bloquea el scroll del navegador
       $('body').addClass('stop-scrolling');

	// Reinicia el formulario
	$('#'+modal_id+' form').trigger("reset");
	$($('#'+modal_id+' form textarea')).html('');

	$('#'+modal_id).css('display', 'block'); // Muestra la ventana modal
	$('.formulario-edicion .modal-contenido h3').text(titulo);
	$('.eliminar-elemento').css('display', 'none');
	$('.eliminar-elemento').attr('href', '');
	$('#'+modal_id+' form').attr('action', action_url);
}