<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\AppCategoriasGenericas;

use App\Services\Funciones;

/**
 * Metricas
 *
 * @ORM\Table(name="metricas", indexes={@ORM\Index(name="FK_metricas_entradas", columns={"identrada"}), @ORM\Index(name="FK_metricas_simulaciones", columns={"idsimulacion"}), @ORM\Index(name="FK_metricas_salidas", columns={"idsalida"}), @ORM\Index(name="FK_metricas_componentes", columns={"idcomponente"})})
 * @ORM\Entity(repositoryClass="App\Repository\MetricasRepository")
 */
class Metricas {

    /**
     * @var int
     *
     * @ORM\Column(name="idmetrica", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmetrica;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50, nullable=false)
     */
    private $nombre;

    /**
     * @var /Simulaciones
     * 
     * @ORM\ManyToOne(targetEntity="Simulaciones", inversedBy="metricas")
     * @ORM\JoinColumn(name="idsimulacion", referencedColumnName="id")
     */
    private $simulacion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="idcomponente", type="bigint", nullable=true)
     */
    private $idcomponente;

    /**
     * @var int|null
     *
     * @ORM\Column(name="identrada", type="bigint", nullable=true)
     */
    private $identrada;

    /**
     * @var int|null
     *
     * @ORM\Column(name="idsalida", type="bigint", nullable=true)
     */
    private $idsalida;

    /**
     * @var int|null
     *
     * @ORM\Column(name="idfiabilidad", type="bigint", nullable=true)
     */
    private $idfiabilidad;
    
    /**
     * 
     * @var \AppCategoriasGenericas|null
     * 
     * @ORM\ManyToOne(targetEntity="AppCategoriasGenericas")
     * @ORM\JoinColumn(name="idfiabilidad", referencedColumnName="idcategoria")
     */
    private $categoria;
    
    function getCategoria(): ?AppCategoriasGenericas {
        return $this->categoria;
    }

    function setCategoria(?AppCategoriasGenericas $categoria): void {
        $this->categoria = $categoria;
    }

    
    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", nullable=true)
     */
    private $descripcion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="color", type="string", nullable=true)
     */
    private $color;
    
    function getDescripcion(): ?string {
        return $this->descripcion;
    }

    function setDescripcion(?string $descripcion): void {
        $this->descripcion = $descripcion;
    }

    function getIdmetrica(): int {
        return $this->idmetrica;
    }

    function getNombre(): string {
        return $this->nombre;
    }

    function getSimulacion(): Simulaciones {
        return $this->simulacion;
    }

    function getIdcomponente(): ?int {
        return $this->idcomponente;
    }

    function getIdentrada(): ?int {
        return $this->identrada;
    }

    function getIdsalida(): ?int {
        return $this->idsalida;
    }

    function getIdfiabilidad(): ?int {
        return $this->idfiabilidad;
    }

    function setIdmetrica(int $idmetrica): self {
        $this->idmetrica = $idmetrica;
        return $this;
    }

    function setNombre(string $nombre): self {
        $this->nombre = $nombre;
        return $this;
    }

    function setSimulacion(Simulaciones $simulacion): self {
        $this->simulacion = $simulacion;
        return $this;
    }

    function setIdcomponente(?int $idcomponente): self {
        $this->idcomponente = $idcomponente;
        return $this;
    }

    function setIdentrada(?int $identrada): self {
        $this->identrada = $identrada;
        return $this;
    }

    function setIdsalida(?int $idsalida): self {
        $this->idsalida = $idsalida;
        return $this;
    }

    function setIdfiabilidad(?int $idfiabilidad): self {
        $this->idfiabilidad = $idfiabilidad;
        return $this;
    }

    function getColor(): ?string {
        return $this->color;
    }

    function setColor(?string $color): self {
        $this->color = $color;
        return $this;
    }

         /**
     * Funcion que devuelve los datos del objeto en formato json.
     * 
     * @return string|json
     * 
     * @author antonio.gomez <i52goloa@uco.es>
     */
    public function toArray() {
       
        return [
                'idmetrica' => $this->idmetrica,
                'nombre' => $this->nombre,
                'descripcion' => $this->descripcion,
                'entrada' => $this->identrada,
                'componente' => $this->idcomponente,
                'salida' => $this->idsalida,
                'color' => $this->color,
                'fiabilidad' => $this->idfiabilidad
        ];
        
     }

}
