<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Clase que representa mediante objetos a las filas de la vista de base de datos VwSimulacionesSistemas.
 *
 * @ORM\Table(name="vw_simulaciones_sistemas")
 * @ORM\Entity(repositoryClass="App\Repository\VwSimulacionesSistemasRepository")
 */
class VwSimulacionesSistemas
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idsimulacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=128, nullable=false)
     */
    private $nombre;
    
    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=4096, nullable=true)
     */
    private $descripcion;

    /**
     * @var int
     *
     * @ORM\Column(name="idpropietario", type="bigint", nullable=false)
     */
    private $idpropietario;
    
     /**
     * @var string|null
     *
     * @ORM\Column(name="propietario", type="string", length=4096, nullable=true)
     */
    private $propietario;
    
     /**
     * @var int
     *
     * @ORM\Column(name="idsistema", type="bigint", nullable=false)
     */
    private $idsistema;
    
     /**
     * @var string|null
     *
     * @ORM\Column(name="nombresistema", type="string", length=4096, nullable=true)
     */
    private $nombresistema;
    
     /**
     * @var int
     *
     * @ORM\Column(name="idgrupo", type="bigint", nullable=false)
     */
    private $idgrupo;
    
    /**
     * @var string|null
     *
     * @ORM\Column(name="grupo", type="string", length=4096, nullable=true)
     */
    private $grupo;
    
    /**
     * @var int|null
     *
     * @ORM\Column(name="visibilidad", type="integer", nullable=true)
     */
    private $visibilidad;
    
    /**
     * @var string|null
     *
     * @ORM\Column(name="visibilidadtexto", type="string", length=4096, nullable=true)
     */
    private $visibilidadtexto;
    
    function getIdsimulacion(): int {
        return $this->idsimulacion;
    }

    function getNombre(): ?string {
        return $this->nombre;
    }

    function getDescripcion(): ?string {
        return $this->descripcion;
    }

    function getIdpropietario(): int {
        return $this->idpropietario;
    }

    function getPropietario(): ?string {
        return $this->propietario;
    }

    function getIdsistema(): int {
        return $this->idsistema;
    }

    function getNombresistema(): ?string {
        return $this->nombresistema;
    }

    function getIdgrupo(): int {
        return $this->idgrupo;
    }

    function getGrupo(): ?string {
        return $this->grupo;
    }

    function getVisibilidad(): ?int {
        return $this->visibilidad;
    }

    function getVisibilidadtexto(): ?string {
        return $this->visibilidadtexto;
    }

    function setIdsimulacion(int $idsimulacion): void {
        $this->idsimulacion = $idsimulacion;
    }

    function setNombre(?string $nombre): void {
        $this->nombre = $nombre;
    }

    function setDescripcion(?string $descripcion): void {
        $this->descripcion = $descripcion;
    }

    function setIdpropietario(int $idpropietario): void {
        $this->idpropietario = $idpropietario;
    }

    function setPropietario(?string $propietario): void {
        $this->propietario = $propietario;
    }

    function setIdsistema(int $idsistema): void {
        $this->idsistema = $idsistema;
    }

    function setNombresistema(?string $nombresistema): void {
        $this->nombresistema = $nombresistema;
    }

    function setIdgrupo(int $idgrupo): void {
        $this->idgrupo = $idgrupo;
    }

    function setGrupo(?string $grupo): void {
        $this->grupo = $grupo;
    }

    function setVisibilidad(?int $visibilidad): void {
        $this->visibilidad = $visibilidad;
    }

    function setVisibilidadtexto(?string $visibilidadtexto): void {
        $this->visibilidadtexto = $visibilidadtexto;
    }


  
}
