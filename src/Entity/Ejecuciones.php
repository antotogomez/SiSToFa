<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ejecuciones
 *
 * @ORM\Table(name="ejecuciones", indexes={@ORM\Index(name="FK_ejecuciones_simulaciones", columns={"idsimulacion"}), @ORM\Index(name="FK_ejecuciones_usuarios", columns={"idusuario"})})
 * @ORM\Entity(repositoryClass="App\Repository\EjecucionesRepository")
 */
class Ejecuciones {

    /**
     * @var int
     *
     * @ORM\Column(name="idejecucion", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idejecucion;

    /**
     * @var int
     *
     * @ORM\Column(name="idsimulacion", type="bigint", nullable=false)
     */
    private $idsimulacion = '0';

    /**
     * @var \Simulaciones
     *
     * @ORM\ManyToOne(targetEntity="Simulaciones")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idsimulacion", referencedColumnName="id")
     * })
     */
    private $simulacion;

    /**
     * @var int
     *
     * @ORM\Column(name="idusuario", type="bigint", nullable=false)
     */
    private $idusuario = '0';

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuario", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false)
     */
    private $fecha;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=4096, nullable=true)
     */
    private $descripcion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="situacion", type="string", length=36502, nullable=true)
     */
    private $situacion;

    /**
     * @var int
     *
     * @ORM\Column(name="estado", type="bigint", nullable=false)
     */
    private $estado;

    /**
     * @var int
     *
     * @ORM\Column(name="step", type="bigint", nullable=true)
     */
    private $step;

    function getStep() {
        return $this->step;
    }

    private $errores = array();
    
    public function getErrores(){
        return $this->errores;
    }
    
    public function addError($instante) {
        $this->errores[count($this->errores)] = $instante;
    }
    
    /**
     * Ejecuta el siguiente paso de la ejecución realizando los cambios oportunos en la simulación
     * que se esta ejecutando.
     * 
     * @return array Array con los valores de las metricas obtenidas en este paso de la ejecución.
     */
    function nextStep(): array {

        $this->step = $this->step + 1;

        $this->simulacion->aplicaEventos($this->step);
        
        $this->simulacion->getSistema()->calcular();
        $this->simulacion->getSistema()->calculaFiabilidad();
        
        if(is_null($this->errores)) {
            $this->errores = array();
        }
        
        //Guarda si ha ocurrido un error y cuando.
         foreach($this->simulacion->getSistema()->getComponentes() as $componente) {
             if($componente->getFallado()) {
                 $this->errores[count($this->errores)] = $this->step;
                 //Ya hay uno, no necesita mas.
                 break;
             }
         }
        
        $valores["metricas"] = $this->simulacion->recogeMetricas($this, $this->step);

        //Guarda en situacion, la situacion actual.
        $this->situacion = serialize($this);
        return $valores;
    }

    function initStep(): void {
        $this->step = 0;
    }

    function setStep($step): void {
        $this->step = $step;
    }

    function getIdejecucion(): int {
        return $this->idejecucion;
    }

    function getIdsimulacion(): int {
        return $this->idsimulacion;
    }

    function getSimulacion(): Simulaciones {
        return $this->simulacion;
    }

    function getIdusuario(): int {
        return $this->idusuario;
    }

    function getUsuario(): Usuarios {
        return $this->usuario;
    }

    function getDescripcion(): ?string {
        return $this->descripcion;
    }

    function getEstado(): int {
        return $this->estado;
    }

    function setIdejecucion(int $idejecucion): self {
        $this->idejecucion = $idejecucion;
        return $this;
    }

    function setIdsimulacion(int $idsimulacion): self {
        $this->idsimulacion = $idsimulacion;
        return $this;
    }

    function setSimulacion(Simulaciones $simulacion): self {
        $this->simulacion = $simulacion;
        return $this;
    }

    function setIdusuario(int $idusuario): self {
        $this->idusuario = $idusuario;
        return $this;
    }

    function setUsuario(Usuarios $usuario): self {
        $this->usuario = $usuario;
        return $this;
    }

    function setDescripcion(?string $descripcion): self {
        $this->descripcion = $descripcion;
        return $this;
    }

    function setEstado(int $estado): self {
        $this->estado = $estado;
        return $this;
    }

    function getFecha(): \DateTime {
        return $this->fecha;
    }

    function setFecha(\DateTime $fecha): self {
        $this->fecha = $fecha;
        return $this;
    }

    function getSituacion(): ?string {
        return $this->situacion;
    }

    function setSituacion(?string $situacion): void {
        $this->situacion = $situacion;
    }

}
