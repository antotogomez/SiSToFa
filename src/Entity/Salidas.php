<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Entity;

use App\Entity\Componentes;
use App\Entity\AppCategoriasGenericas;
use Doctrine\ORM\Mapping as ORM;

/**
 * Salidas
 *
 * @ORM\Table(name="salidas", indexes={@ORM\Index(name="fk_salidas_componentes1_idx", columns={"componentes_id"})})
 * @ORM\Entity
 * 
 */
class Salidas {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=128, nullable=false)
     */
    private $nombre;

    /**
     * @var int
     *
     * @ORM\Column(name="operacion", type="integer", nullable=false)
     */
    private $operacion;

    /**
     * @var ?string
     *
     * @ORM\Column(name="descripcion", type="string", length=4096, nullable=true)
     */
    private $descripcion;

    /**
     * 
     * @var \Componentes
     * 
     * @ORM\ManyToOne(targetEntity="Componentes", inversedBy="salidas")
     * @ORM\JoinColumn(name="componentes_id", referencedColumnName="id")
     */
    private $componente;

    /**
     * Una salida puede entar conectada a muchas entradas de otros componentes
     * 
     * @ORM\OneToMany(targetEntity="Entradas", mappedBy="salida")
     */
    private $entradas;

    /**
     * @ORM\OneToOne(targetEntity="AppCategoriasGenericas")
     * @ORM\JoinColumn(name="operacion", referencedColumnName="idcategoria", nullable=true)
     */
    private $operacionsalida;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=10, nullable=true)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="colortexto", type="string", length=10, nullable=true)
     */
    private $colortexto;
    private $valorCalculado;

    function getValorCalculado() {
        return $this->valorCalculado;
    }

    function setValorCalculado($valorCalculado): void {
        $this->valorCalculado = $valorCalculado;
    }

    public function getId(): ?string {
        return $this->id;
    }

    public function getNombre(): ?string {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self {
        $this->nombre = $nombre;

        return $this;
    }

    public function getOperacion(): ?int {
        return $this->operacion;
    }

    public function setOperacion(int $operacion): self {
        $this->operacion = $operacion;

        return $this;
    }

    function getComponente(): Componentes {
        return $this->componente;
    }

    function setComponente(Componentes $componente): self {
        $this->componente = $componente;

        return $this;
    }

    function getColortexto(): string {

        if (is_null($this->colortexto)) {
            return "#FFFFFF";
        } else {
            return $this->colortexto;
        }
    }

    function setColortexto(string $colortexto): self {
        $this->colortexto = $colortexto;
        return $this;
    }

    function getColor(): string {

        if (is_null($this->color)) {
            return "#000000";
        } else {
            return $this->color;
        }
    }

    function setColor(string $color): self {
        $this->color = $color;

        return $this;
    }

    function getDescripcion(): ?string {
        return $this->descripcion;
    }

    function setDescripcion(?string $descripcion): self {
        $this->descripcion = $descripcion;

        return $this;
    }

    function getEntradas() {
        return $this->entradas;
    }

    function setEntradas($entradas): self {
        $this->entradas = $entradas;

        return $this;
    }

    function getOperacionsalida() {
        return $this->operacionsalida;
    }

    function setOperacionsalida($operacionsalida): void {
        $this->operacionsalida = $operacionsalida;
    }

    /**
     * Funcion que devuelve los datos del objeto en formato json.
     * 
     * @return string|json
     * 
     * @author antonio.gomez <i52goloa@uco.es>
     */
    public function toArray() {

        $arrayEntradas = array();

        foreach ($this->entradas as $entrada) {
            $arrayEntradas[] = ['id' => $entrada->getId(),
                'nombre' => $entrada->getNombre(),
                'valorinicial' => $entrada->getValorInicial(),
                'descripcion' => $entrada->getDescripcion()];
        }

        return [
            'id' => $this->id,
            'nombre' => $this->nombre,
            'operacion' => $this->operacion,
            'operacionNombre' => $this->operacionsalida->getNombre(),
            'color' => $this->color,
            'colortexto' => $this->colortexto,
            'descripcion' => $this->descripcion,
            'componente_id' => $this->componente->getId(),
            'entradas' => $arrayEntradas
        ];
    }

}
