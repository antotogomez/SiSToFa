<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Clase que representa mediante objetos a las filas de la vista de base de datos VWEventos.
 *
 * @ORM\Table(name="vw_valores_metricas_comparados")})
 * @ORM\Entity(repositoryClass="App\Repository\VwValoresComparadosRepository")
 */
class VwValoresComparados
{
    public function getIdmetrica(): int {
        return $this->idmetrica;
    }

    public function getInstante(): int {
        return $this->instante;
    }

    public function getValor(): ?float {
        return $this->valor;
    }

    public function getIdejecucion(): int {
        return $this->idejecucion;
    }

    public function getValor2(): ?float {
        return $this->valor2;
    }

    public function getIdejecucion2(): ?int {
        return $this->idejecucion2;
    }

    public function setIdmetrica(int $idmetrica): void {
        $this->idmetrica = $idmetrica;
    }

    public function setInstante(int $instante): void {
        $this->instante = $instante;
    }

    public function setValor(?float $valor): void {
        $this->valor = $valor;
    }

    public function setIdejecucion(int $idejecucion): void {
        $this->idejecucion = $idejecucion;
    }

    public function setValor2(?float $valor2): void {
        $this->valor2 = $valor2;
    }

    public function setIdejecucion2(?int $idejecucion2): void {
        $this->idejecucion2 = $idejecucion2;
    }
    public function getIdvalor(): float {
        return $this->idvalor;
    }

    public function setIdvalor(float $idvalor): void {
        $this->idvalor = $idvalor;
    }

         /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="idvalor", type="bigint", nullable=false)
     */
    private $idvalor;
    
    /**
     * @var int
     *
     * @ORM\Column(name="idmetrica", type="bigint", nullable=false)
     */
    private $idmetrica;
    
    /**
     * @var int
     *
     * @ORM\Column(name="instante", type="bigint", nullable=false)
     */
    private $instante;

     /**
     * @var float|null
     *
     * @ORM\Column(name="valor", type="float", nullable=true)
     */
    private $valor;
    
    /**
     * @var int
     *
     * @ORM\Column(name="idejecucion", type="bigint", nullable=true)
     */
    private $idejecucion;
    
    
     /**
     * @var float|null
     *
     * @ORM\Column(name="valor2", type="float", nullable=true)
     */
    private $valor2;
    
    /**
     * @var int|null
     *
     * @ORM\Column(name="idejecucion2", type="bigint", nullable=true)
     */
    private $idejecucion2;
    
    

}
