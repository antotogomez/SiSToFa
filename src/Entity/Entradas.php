<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entradas
 *
 * @ORM\Table(name="entradas", indexes={@ORM\Index(name="fk_entradas_componentes1_idx", columns={"componentes_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\EntradasRepository")
 */
class Entradas
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=128, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=4096, nullable=true)
     */
    private $descripcion;
    
    /**
     * @var float|null
     *
     * @ORM\Column(name="valorinicial", type="float", precision=10, scale=0, nullable=true)
     */
    private $valorInicial;

    /**
     * @var int
     *
     * @ORM\Column(name="componentes_id", type="bigint", nullable=false)
     */
    private $componentes_id;
        
    /**
     * Muchas entradas pertenecen a un componente
     * 
     * @var Componentes
     * 
     * @ORM\ManyToOne(targetEntity="Componentes", inversedBy="entradas")
     * @ORM\JoinColumn(name="componentes_id", referencedColumnName="id")
     */
    private $componente;
    
    /**
     * Muchas entradas pertenecen estas conectadas a una salida
     * 
     * @var Salidas
     * 
     * @ORM\ManyToOne(targetEntity="Salidas", inversedBy="entradas")
     * @ORM\JoinColumn(name="id_salida", referencedColumnName="id")
     */
    private $salida;
    
    private $valor;
    
     /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=10, nullable=true)
     */
    private $color;
    
    /**
     * @var string
     *
     * @ORM\Column(name="colortexto", type="string", length=10, nullable=true)
     */
    private $colortexto;
    
    function getComponente(): Componentes {
        return $this->componente;
    }

    function setComponente(Componentes $componente): self {
        $this->componente = $componente;
        return $this;
    }

    function getColortexto(): string {
        
         if(is_null($this->colortexto)) {
            return "#FFFFFF";
        }
        else {
            return $this->colortexto;
        }
    }

    function setColortexto(string $colortexto): self {
        $this->colortexto = $colortexto;
         return $this;
    }

        function getColor(): string {
        
        if(is_null($this->color)) {
            return "#940010";
        }
        else {
            return $this->color;
        }
        
    }

    function setColor(string $color): self {
        $this->color = $color;
        
        return $this;
    }
    
        public function getId(): ?string
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getValorInicial(): ?float
    {
        return $this->valorInicial;
    }

    public function setValorInicial(?float $valorInicial): self
    {
        $this->valorInicial = $valorInicial;
        $this->valor = $valorInicial;
        
        return $this;
    }

    public function getComponentes(): ?Componentes
    {
        return $this->componentes;
    }

    public function setComponentes(?Componentes $componentes): self
    {
        $this->componentes = $componentes;

        return $this;
    }
    
    function getDescripcion(): ?string {
        return $this->descripcion;
    }

    function setDescripcion(string $descripcion): self {
        $this->descripcion = $descripcion;
        return $this;
    }

    function getComponentes_id(): int {
        return $this->componentes_id;
    }

    function setComponentes_id(int $componentes_id): self {
        $this->componentes_id = $componentes_id;
        
        return $this;
    }

    function getSalida(): ?Salidas {
        return $this->salida;
    }

    function setSalida(?Salidas $salida): self {
        $this->salida = $salida;
        
        return $this;
    }

    function getValor() {
        
        if( $this->valor == null && $this->salida == null) {
            return $this->valorInicial;
        }
        else {
            return $this->valor;
        }
        
    }

    function setValor($valor): void {
        $this->valor = $valor;
    }

    /**
     * Funcion que devuelve los datos del objeto en forma de array.
     * 
     * @return array Array con los datos de la Entrada
     */
    public function toArray():array {
        
        $salida_id = null;
        $salida_nombre = null;
        $salida_componente= null;
        $salida_componente_id = null;
                
        if (!is_null($this->salida)) {
            $salida_id = $this->salida->getId();
            $salida_nombre = $this->salida->getNombre();
            $salida_componente= $this->salida->getComponente()->getNombre();
            $salida_componente_id = $this->salida->getComponente()->getId();
        }
            
        return [
                'id' => $this->id,
                'nombre' => $this->nombre,
                'valorinicial' => $this->valorInicial,
                'descripcion' => $this->descripcion,
                'componente_id' => $this->componente->getId(),
                'salida_id' => $salida_id,
                'salida_nombre' => $salida_nombre,
                'color' => $this->color,
                'colortexto' => $this->colortexto,
                'salida_componente' => $salida_componente,
                'salida_componente_id' => $salida_componente_id
        ];
        
     }


}
