<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Representación orientada a objetos de la entidad de base de datos 
 * AppColumnaslistados.
 *
 * @ORM\Table(name="app_columnaslistado", indexes={@ORM\Index(name="listado_fk_idx", columns={"idlistado"})})
 * @ORM\Entity
 */
class AppColumnaslistado
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcolumna", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcolumna;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=64, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="texto", type="string", length=128, nullable=true)
     */
    private $texto;

    /**
     * @var int|null
     *
     * @ORM\Column(name="esidentificador", type="integer", nullable=true)
     */
    private $esidentificador;

    /**
     * @var int|null
     *
     * @ORM\Column(name="esdescripcion", type="integer", nullable=true)
     */
    private $esdescripcion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="orden", type="integer", nullable=true)
     */
    private $orden;

    /**
     * @var int|null
     *
     * @ORM\Column(name="visible", type="integer", nullable=true)
     */
    private $visible;

    /**
     * @var int|null
     *
     * @ORM\Column(name="esotro", type="integer", nullable=true)
     */
    private $esotro;

    /**
     * @var int|null
     *
     * @ORM\Column(name="size_", type="integer", nullable=true)
     */
    private $size;

    /**
     * @var string|null
     *
     * @ORM\Column(name="align", type="string", length=128, nullable=true)
     */
    private $align;

    /**
     * @var int|null
     *
     * @ORM\Column(name="oracle_clob", type="integer", nullable=true)
     */
    private $oracleClob = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="ordenable", type="integer", nullable=true)
     */
    private $ordenable = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="esactivado", type="boolean", nullable=true)
     */
    private $esactivado = '0';

    /**
     * @var \AppListados
     *
     * @ORM\ManyToOne(targetEntity="AppListados")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idlistado", referencedColumnName="idlistado")
     * })
     */
    private $idlistado;


}
