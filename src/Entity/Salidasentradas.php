<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Salidasentradas
 *
 * @ORM\Table(name="salidasentradas", indexes={@ORM\Index(name="fk_salidasentradas_entradas1_idx", columns={"entradas_id"}), @ORM\Index(name="fk_salidasentradas_salidas1_idx", columns={"salidas_id"})})
 * @ORM\Entity
 */
class Salidasentradas
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="orden", type="integer", nullable=false)
     */
    private $orden;

    /**
     * @var \Entradas
     *
     * @ORM\ManyToOne(targetEntity="Entradas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="entradas_id", referencedColumnName="id")
     * })
     */
    private $entradas;

    /**
     * @var \Salidas
     *
     * @ORM\ManyToOne(targetEntity="Salidas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="salidas_id", referencedColumnName="id")
     * })
     */
    private $salidas;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrden(): ?int
    {
        return $this->orden;
    }

    public function setOrden(int $orden): self
    {
        $this->orden = $orden;

        return $this;
    }

    public function getEntradas(): ?Entradas
    {
        return $this->entradas;
    }

    public function setEntradas(?Entradas $entradas): self
    {
        $this->entradas = $entradas;

        return $this;
    }

    public function getSalidas(): ?Salidas
    {
        return $this->salidas;
    }

    public function setSalidas(?Salidas $salidas): self
    {
        $this->salidas = $salidas;

        return $this;
    }


}
