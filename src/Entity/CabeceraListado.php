<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Entity;

/**
 * Clase con la información de cabecera de un listado.
 *
 * @author Antonio Gómez <i52goloa@uco.es>
 */
class CabeceraListado {

    private $texto;
    private $imagen;
    private $crear;
    private $paginaedicion;
    private $rutavuelta;
    private $nolistado = false;
    private $botones;

    function getBotones() {
        return $this->botones;
    }

    function setBotones($botones): self {
        $this->botones = $botones;
        return $this;
    }

    function getRutavuelta() {
        return $this->rutavuelta;
    }

    function setRutavuelta($rutavuelta): void {
        $this->rutavuelta = $rutavuelta;
    }

    function getPaginaedicion() {
        return $this->paginaedicion;
    }

    function setPaginaedicion($paginaedicion): void {
        $this->paginaedicion = $paginaedicion;
    }

    function __construct($texto, $imagen, $nolistado = false) {
        $this->texto = $texto;
        $this->imagen = $imagen;
        $this->paginaedicion = null;
        $this->rutavuelta = null;
        $this->nolistado = $nolistado;
    }

    function getTexto() {
        return $this->texto;
    }

    function getImagen() {

        //Si no tiene una establecida pone por defecto la de la EPSC
        if ($this->imagen == "") {
            return "icono-simusitfa2.png";
        } else {
            return $this->imagen;
        }
    }

    function setTexto($texto): void {
        $this->texto = $texto;
    }

    function setImagen($imagen): void {
        $this->imagen = $imagen;
    }

    function getCrear() {
        return $this->crear;
    }

    function setCrear($crear): void {
        $this->crear = $crear;
    }

    function getNolistado() {
        return $this->nolistado;
    }

    function setNolistado($nolistado): void {
        $this->nolistado = $nolistado;
    }

}
