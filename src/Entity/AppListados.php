<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Representación orientada a objetos de la entidad de base de datos 
 * AppListados. Que son las opciones del menú de usuario.
 *
 * @ORM\Table(name="app_listados", uniqueConstraints={@ORM\UniqueConstraint(name="slug", columns={"slug"}), @ORM\UniqueConstraint(name="nombre", columns={"nombre"})})
 * @ORM\Entity(repositoryClass="App\Repository\AppListadosRepository")
 */
class AppListados {

    /**
     * @var int
     *
     * @ORM\Column(name="idlistado", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idlistado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=128, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="texto", type="text", length=65535, nullable=true)
     */
    private $texto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="campodeordenacion", type="text", length=65535, nullable=true)
     */
    private $campodeordenacion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="orden", type="integer", nullable=true)
     */
    private $orden;

    /**
     * @var int|null
     *
     * @ORM\Column(name="activado", type="integer", nullable=true)
     */
    private $activado;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nivel", type="integer", nullable=true)
     */
    private $nivel;

    /**
     * @var int
     *
     * @ORM\Column(name="nuevo_bloque", type="integer", nullable=false)
     */
    private $nuevoBloque = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="tipo", type="integer", nullable=false)
     */
    private $tipo = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="paginaedicionS", type="string", length=128, nullable=true)
     */
    private $paginaedicions;

    /**
     * @var string|null
     *
     * @ORM\Column(name="imagenS", type="string", length=256, nullable=true)
     */
    private $imagens;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=40, nullable=false)
     */
    private $slug;

    /**
     * @var int
     *
     * @ORM\Column(name="listadodatos", type="integer", length=1, nullable=false)
     */
    private $listadodatos;

    /*
     * EntityManager enviado desde el controlador para poder realizar las consultas.
     */
    private $entitiymanager;

    function getCrear() {
        return $this->crear;
    }

    function setCrear($crear): void {
        $this->crear = $crear;
    }

    private $crear;

    public function getIdlistado(): ?int {
        return $this->idlistado;
    }

    public function getNombre(): ?string {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self {
        $this->nombre = $nombre;

        return $this;
    }

    function getTipo():?int {
        return $this->tipo;
    }

    function setTipo(int $tipo): void {
        $this->tipo = $tipo;
    }

    public function getTexto(): ?string {
        return $this->texto;
    }

    public function setTexto(?string $texto): self {
        $this->texto = $texto;

        return $this;
    }

    function getListadodatos(): int {
        return $this->listadodatos;
    }

    function setListadodatos(int $listadodatos): void {
        $this->listadodatos = $listadodatos;
    }

    public function getCampodeordenacion(): ?string {
        return $this->campodeordenacion;
    }

    public function setCampodeordenacion(?string $campodeordenacion): self {
        $this->campodeordenacion = $campodeordenacion;

        return $this;
    }

    public function getOrden(): ?int {
        return $this->orden;
    }

    public function setOrden(?int $orden): self {
        $this->orden = $orden;

        return $this;
    }

    public function getActivado(): ?int {
        return $this->activado;
    }

    public function setActivado(?int $activado): self {
        $this->activado = $activado;

        return $this;
    }

    public function getNivel(): ?int {
        return $this->nivel;
    }

    public function setNivel(?int $nivel): self {
        $this->nivel = $nivel;

        return $this;
    }

    public function getNuevoBloque(): ?int {
        return $this->nuevoBloque;
    }

    public function setNuevoBloque(int $nuevoBloque): self {
        $this->nuevoBloque = $nuevoBloque;

        return $this;
    }

    public function getPaginaedicions(): ?string {
        return $this->paginaedicions;
    }

    public function setPaginaedicions(?string $paginaedicions): self {
        $this->paginaedicions = $paginaedicions;

        return $this;
    }

    public function getImagens(): ?string {
        return $this->imagens;
    }

    public function setImagens(?string $imagens): self {
        $this->imagens = $imagens;

        return $this;
    }

    public function getSlug(): ?string {
        return $this->slug;
    }

    public function setSlug(string $slug): self {
        $this->slug = $slug;

        return $this;
    }

}
