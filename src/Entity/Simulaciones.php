<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Services\Funciones;

/**
 * Simulaciones
 *
 * @ORM\Table(name="simulaciones", indexes={@ORM\Index(name="fk_simulaciones_sistemas1_idx", columns={"sistemas_idsistema"}), @ORM\Index(name="fk_simulaciones_usuarios1_idx", columns={"usuarios_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\SimulacionesRepository")
 */
class Simulaciones {

    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=128, nullable=false)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=45, nullable=true)
     */
    private $descripcion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tiempo", type="bigint", nullable=true)
     */
    private $tiempo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="visibilidad", type="integer", nullable=true)
     */
    private $visibilidad;

    /**
     * @var int|null
     *
     * @ORM\Column(name="eacalatiempo", type="bigint", nullable=true)
     */
    private $eacalatiempo;

    /**
     * @var \Sistemas
     *
     * @ORM\ManyToOne(targetEntity="Sistemas")
     * @ORM\JoinColumn(name="sistemas_idsistema", referencedColumnName="idsistema")
     * 
     */
    private $sistema;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumn(name="usuarios_id", referencedColumnName="id")
     * 
     */
    private $usuarios;

    /**
     * Un sistema tiene muchos componentes
     * 
     * @ORM\OneToMany(targetEntity="Metricas", mappedBy="simulacion")
     */
    private $metricas;

    /**
     * Una simulacion puede tener muchos eventos a aplicar
     * 
     * @ORM\OneToMany(targetEntity="Eventos", mappedBy="simulaciones")
     */
    private $eventos;
    
    private $historial;
    
    /**
     * Una simulacion puede tener muchos eventos a aplicar
     * 
     * @ORM\OneToMany(targetEntity="VwEventos", mappedBy="simulaciones")
     * @ORM\OrderBy({"instante" = "ASC"})
     */
    private $vweventos;

        public function getVweventos() {
        return $this->vweventos;
    }

    public function setVweventos($vweventos): void {
        $this->vweventos = $vweventos;
    }

        public function getId(): ?string {
        return $this->id;
    }

    public function getNombre(): ?string {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDescripcion(): ?string {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getTiempo(): ?string {
        return $this->tiempo;
    }

    function getHistorial() {
        return $this->historial;
    }

    function getVisibilidad(): ?int {
        return $this->visibilidad;
    }

    function setVisibilidad(?int $visibilidad): void {
        $this->visibilidad = $visibilidad;
    }

    public function setTiempo(?string $tiempo): self {
        $this->tiempo = $tiempo;

        return $this;
    }

    public function getEacalatiempo(): ?string {
        return $this->eacalatiempo;
    }

    public function setEacalatiempo(?string $eacalatiempo): self {
        $this->eacalatiempo = $eacalatiempo;

        return $this;
    }

    public function getSistema(): ?Sistemas {
        return $this->sistema;
    }

    public function setSistema(?Sistemas $sistema): self {
        $this->sistema = $sistema;

        return $this;
    }

    public function getUsuarios(): ?Usuarios {
        return $this->usuarios;
    }

    public function setUsuarios(?Usuarios $usuarios): self {
        $this->usuarios = $usuarios;

        return $this;
    }

    function getMetricas() {
        return $this->metricas;
    }

    function setMetricas($metricas): void {
        $this->metricas = $metricas;
    }

    /**
     * Aplica los eventos establecidos en la simulación en el sistema que se está 
     * simulando para el paso actual.
     * 
     * @param int $step Paso en el que se aplican los eventos.
     * 
     */
    public function aplicaEventos(int $step) {

        //Aplica los eventos que se ejecutan en este paso sobre el sistema
        foreach ($this->eventos as $evento){
            if ($evento->getInstante() == $step){
                //Aplica el evento.
                if ($evento->getEntradaid() != null) {
                    //Busca la entrada y le establece el valor.
                    Funciones::logInfo("[" . basename(__FILE__) . ":" .  __LINE__ . "]" . "Cambia valor de la entrada " . $evento->getEntradaid() . ", nuevo valor: " . $evento->getValor());
                    $this->getSistema()->setValorEntrada($evento->getEntradaid(), $evento->getValor());
                }
                
                if ($evento->getComponenteid() != null) {
                    //Busca la entrada y le establece el valor.
                    //Solo aplica el estado de error si se cumple el porcentaje establecido en el evento.  
                    Funciones::logInfo("[" . basename(__FILE__) . ":" .  __LINE__ . "]" . "Aplicando estado " . $evento->getEstado() . " sobre el estado del componente: " . $evento->getComponenteid());
                    
                    if($evento->getEstado() == 0) {
                        $this->getSistema()->setEstadoComponente($evento->getComponenteid(), 1,$evento->getOperacionfallo(), $evento->getPorcentajefallo());
                    }
                    else {
                        //Es una activacion
                        $this->getSistema()->setEstadoComponente($evento->getComponenteid(), 0,null, null);
                    }
                }

            }
        }   
    }

    /**
     * Función para guardar en un array los valores de las métricas del sistema
     * en el momento de ejecución actual.
     * 
     * @param object $ejecucion Objecto de la ejecución sobre la que se van a recoger metricas.
     * @param int $instante Instante en que se recogen las metricas.
     * 
     * @return void
     */
    public function recogeMetricas($ejecucion, $instante) {

        $valores = array();
        
        foreach ($this->metricas as $metrica) {
            
            $valor = new MetricasValores();
            
            $valor->setIdmetrica($metrica->getIdmetrica());
            $valor->setInstante($instante);
            $valor->setIdejecucion($ejecucion->getIdejecucion());
                    
            if ($metrica->getIdcomponente() != null) {
                 Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "] metrica componente:" . $metrica->getIdcomponente());
            
                $componente = $this->sistema->getComponenteId($metrica->getIdcomponente());
                Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "]" . $componente->getNombre() . " esta en estado" . $componente->getFallado());
                $valor->setValor($componente->getFallado());
            }

            
            if ($metrica->getIdsalida() != null) {
                 Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "] metrica salida: " . $metrica->getIdsalida());
           
                $salida = $this->sistema->getSalida($metrica->getIdsalida());
                $valor->setValor($salida->getValorCalculado());
                
            }

            if ($metrica->getIdentrada() != null) {

                $entrada = $this->sistema->getEntrada($metrica->getIdentrada());
                $valor->setValor($entrada->getValor());
            }
   
            if ($metrica->getIdfiabilidad() != null) {
                 
                try 
                {
                    $class = $metrica->getCategoria()->getValor();
                    $operacion = new $class();
                    $valor->setValor($operacion->calcula($ejecucion));
                    Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "] :" . $metrica->getCategoria()->getValor() . " Valor:" .  $operacion->calcula($ejecucion));
                }
                catch (\Exception $e)
                {
                    Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "] metrica de fiabilidad ERROR : " . $e->getMessage());
                }
            }
            
            $valores[] = $valor->toArray();
                    
        }
        
        return $valores;
    }

    function getEventos() {
        return $this->eventos;
    }

    function setEventos($eventos): void {
        $this->eventos = $eventos;
    }

     public function toArray() {

        return [
            'idsimulacion' => $this->id,
            'nombre' => $this->nombre,
            'tiempo' => $this->tiempo
        ];
    }
    
}
