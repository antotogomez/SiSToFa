<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Grupos
 *
 * @ORM\Table(name="grupos")
 * @ORM\Entity(repositoryClass="App\Repository\GruposRepository")
 */
class Grupos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idgrupo", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idgrupo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=128, nullable=false)
     */
    private $nombre;

    /**
     * @var int
     *
     * @ORM\Column(name="estado", type="integer", nullable=false, options={"default"="1"})
     */
    private $estado = 1;

    
    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=128, nullable=true)
     */
    private $descripcion;
    
    /**
     * Una salida puede entar conectada a muchas entradas de otros componentes
     * 
     * @ORM\OneToMany(targetEntity="Usuarios", mappedBy="grupo")
     */
    private $usuarios;
    
    function getUsuarios() {
        return $this->usuarios;
    }

    function setUsuarios($usuarios): void {
        $this->usuarios = $usuarios;
    }

        public function getIdgrupo(): ?string
    {
        return $this->idgrupo;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getEstado(): ?int
    {
        return $this->estado;
    }

    public function setEstado(int $estado): self
    {
        $this->estado = $estado;

        return $this;
    }
    function getDescripcion(): ?string {
        return $this->descripcion;
    }

    function setDescripcion(string $descripcion): void {
        $this->descripcion = $descripcion;
    }

}
