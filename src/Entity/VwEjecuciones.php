<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Clase que representa mediante objetos a las filas de la vista de base de datos VwEjecuciones.
 *
 * @ORM\Table(name="vw_ejecuciones")
 * @ORM\Entity(repositoryClass="App\Repository\VwEjecucionesRepository")
 */
class VwEjecuciones
{
    /**
     * @var int
     *
     * @ORM\Column(name="idejecucion", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idejecucion;

    /**
     * @var int
     *
     * @ORM\Column(name="idsimulacion", type="bigint", nullable=false)
     */
    private $idsimulacion;
    
    /**
     * @var int
     *
     * @ORM\Column(name="idusuario", type="bigint", nullable=false)
     */
    private $idusuario;
    
    /**
     * @var date
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false)
     */
    private $fecha;
    
    /**
     * @var string
     *
     * @ORM\Column(name="fecha_formateada", type="string", length=4096, nullable=true)
     */
    private $fechaformateada;
    
    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=4096, nullable=true)
     */
    private $descripcion;

    /**
     * @var int
     *
     * @ORM\Column(name="estado", type="integer", nullable=false)
     */
    private $estado;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
    private $id;
    
    /**
     * @var int
     *
     * @ORM\Column(name="paso", type="integer", nullable=true)
     */
    private $paso;
          
     /**
     * @var string
     *
     * @ORM\Column(name="nombre_simulacion", type="string", length=1024, nullable=false)
     */
    private $nombresimulacion;
    
     /**
     * @var string
     *
     * @ORM\Column(name="nombre_sistema", type="string", length=1024, nullable=false)
     */
    private $nombresistema;
    
         /**
     * @var string
     *
     * @ORM\Column(name="usuario_nombre", type="string", length=1024, nullable=false)
     */
    private $usuarionombre;
    
    private $tiempo;
    
    function getTiempo() {
        return $this->tiempo;
    }

    function setTiempo($tiempo): void {
        $this->tiempo = $tiempo;
    }

        
    function getIdejecucion(): int {
        return $this->idejecucion;
    }

    function getIdsimulacion(): int {
        return $this->idsimulacion;
    }

    function getIdusuario(): int {
        return $this->idusuario;
    }

    function getFecha() {
        return $this->fecha;
    }

    function getDescripcion(): ?string {
        return $this->descripcion;
    }

    function getEstado(): int {
        return $this->estado;
    }

    function getId(): int {
        return $this->id;
    }

    function getNombresimulacion(): string {
        return $this->nombresimulacion;
    }

    function getNombresistema(): string {
        return $this->nombresistema;
    }

    function getUsuarionombre(): string {
        return $this->usuarionombre;
    }

    function setIdejecucion(int $idejecucion): void {
        $this->idejecucion = $idejecucion;
    }

    function setIdsimulacion(int $idsimulacion): void {
        $this->idsimulacion = $idsimulacion;
    }

    function setIdusuario(int $idusuario): void {
        $this->idusuario = $idusuario;
    }

    function setFecha($fecha): void {
        $this->fecha = $fecha;
    }

    function setDescripcion(?string $descripcion): void {
        $this->descripcion = $descripcion;
    }

    function setEstado(int $estado): void {
        $this->estado = $estado;
    }

    function setId(int $id): void {
        $this->id = $id;
    }

    function setNombresimulacion(string $nombresimulacion): void {
        $this->nombresimulacion = $nombresimulacion;
    }

    function setNombresistema(string $nombresistema): void {
        $this->nombresistema = $nombresistema;
    }

    function setUsuarionombre(string $usuarionombre): void {
        $this->usuarionombre = $usuarionombre;
    }

    function getFechaformateada(): string {
        return $this->fechaformateada;
    }

    function setFechaformateada(string $fechaformateada): void {
        $this->fechaformateada = $fechaformateada;
    }

     function getPaso(): ?int {
        return $this->paso;
    }

    function setPaso(int $paso): void {
        $this->paso = $paso;
    }
    
     /**
     * Funcion que devuelve los datos del objeto en formato json.
     * 
     * @return string|json
     * 
     * @author antonio.gomez <i52goloa@uco.es>
     */
    public function toArray() {
        
        return [
                'idejecucion' => $this->idejecucion,
                'nombrejecucion' => $this->nombresimulacion,
                'nombresistema' => $this->nombresistema,
                'paso' => $this->paso,
                'fecha' => $this->fechaformateada,
                'estado'=> $this->estado
        ];
        
     }
}
