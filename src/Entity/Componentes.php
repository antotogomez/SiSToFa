<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Services\Funciones;

/**
 * Representación orientada a objetos de la entidad de base de datos 
 * Componentes.
 *
 * @ORM\Table(name="componentes", indexes={@ORM\Index(name="fk_componentes_sistemas1_idx", columns={"sistemas_idsistema"})})
 * @ORM\Entity(repositoryClass="App\Repository\ComponentesRepository")
 */
class Componentes {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=128, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=4096, nullable=true)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=10, nullable=true)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="colortexto", type="string", length=10, nullable=true)
     */
    private $colortexto;

    /**
     * @var int
     *
     * @ORM\Column(name="configuracion", type="integer", nullable=false)
     */
    private $configuracion;
   
        /**
     * @var int
     *
     * @ORM\Column(name="ndekminimo", type="integer", nullable=true)
     */
    private $ndekminimo;
    
    /**
     * @var float
     *
     * @ORM\Column(name="fiabilidad", type="float", nullable=true)
     */
    private $fiabilidad;

    /**
     * Muchos componentes pertenecen a un sistema
     * 
     * @var /Sistemas
     * 
     * @ORM\ManyToOne(targetEntity="Sistemas", inversedBy="componentes")
     * @ORM\JoinColumn(name="sistemas_idsistema", referencedColumnName="idsistema")
     */
    private $sistema;

    /**
     * Un sistema tiene muchos componentes
     * 
     * @ORM\OneToMany(targetEntity="Salidas", mappedBy="componente")
     */
    private $salidas;

    /**
     * Un sistema tiene muchos componentes
     * 
     * @ORM\OneToMany(targetEntity="Entradas", mappedBy="componente")
     */
    private $entradas;
    
    /**
     * Otros campos no de base de datos necesarios para la simulacion.
     */
    private $calculado;
    private $fallo = false;
    private $porcentaje = null;
    private $fallado = false;
    private $operacionfallo;
    private $iterado = 0;

    function getFallado() {
        return $this->fallado;
    }

    function setFallado($fallado): void {
        $this->fallado = $fallado;
    }

    function getOperacionfallo() {
        return $this->operacionfallo;
    }

    function setOperacionfallo($operacionfallo) {
        $this->operacionfallo = $operacionfallo;
    }

    function getPorcentaje() {
        return $this->porcentaje;
    }

    function setPorcentaje($porcentaje): void {
        $this->porcentaje = $porcentaje;
    }

    function getFallo() {
        return $this->fallo;
    }

    function setFallo($fallo): void {
        $this->fallo = $fallo;
    }

    /**
     * Funcion que determina si un componente esta en error o no. Este error puede venir porque se haya aplicado un error excepcional
     * o por la propia fiabilidad del componente.
     * 
     * @param boolean $estatico Indica si el componentes esta en modo simulación estática o dinámica.
     * 
     * @return void No devuelve nada. Solo establece el estado de los componentes.
     */
    public function calculaError($estatico = false) {

        if ($estatico == true) {
            Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "]" . "Estamos en ejecución estatica del componente [" . $this->id . "] " . $this->nombre . " en estado " . ($this->fallo?"FALLO":"OK"));
            $this->fallado = $this->fallo;
            
        } else {

            $porcentajeaplicado = 0;
            if ($porcentajeaplicado != null) {
                $porcentajeaplicado = $this->porcentaje;
            } else {
                $porcentajeaplicado = (1 - $this->fiabilidad) * 100;
            }

            $min = 0;
            $max = 100;

            $hayerror = mt_rand($min * 100, $max * 100) / 100;
            Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "]" . $this->getNombre() . " se ha calculado un error de " . $hayerror . " siendo el porcentaje de error un " . $porcentajeaplicado);

            if ($hayerror <= $porcentajeaplicado) {
                $this->fallado = true;
            } else {
                $this->fallado = false;
            }
        }
    }

    function getIterado() {
        return $this->iterado;
    }

    function reestableceIterador() {
        $this->iterado = 0;
    }

    function incrementaIterador() {
        $this->iterado++;
    }

    //Si el componente esta en error devuelve 0. No tiene fiabilidad
    function getFiabilidad(): ?float {
        
        if ($this->fallado == 1) {
            return 0;
        } else {
            return $this->fiabilidad;
        }
    }

    function setFiabilidad(?float $fiabilidad): self {
        $this->fiabilidad = $fiabilidad;
        return $this;
    }

    function getCalculado() {
        return $this->calculado;
    }

    function setCalculado($calculado): void {
        $this->calculado = $calculado;
    }

    function getSalidas() {
        return $this->salidas;
    }

    function getEntradas() {
        return $this->entradas;
    }

    /**
     * Devuelve el objeto Entrada que tenga el nombre buscado.
     * 
     * @param string $nombre Nombre de la entrada que se esta buscando.
     * @return Entradas
     * 
     */
    public function getEntradaNombre($nombre) {

        foreach ($this->entradas as $entrada) {
            if ($entrada->getNombre() == $nombre) {
                return $entrada;
                break;
            }
        }
        return null;
    }

    /**
     * Devuelve el objeto Salidas que tenga el nombre buscado.
     * 
     * @param string $nombre Nombre de la salida que se esta buscando.
     * @return Salidas
     * 
     */
    public function getSalidaNombre($nombre) {

        foreach ($this->salidas as $salida) {
            if ($salida->getNombre() == $nombre) {
                return $salida;
                break;
            }
        }
        return null;
    }

    function setSalidas($salidas): self {
        $this->salidas = $salidas;
        return $this;
    }

    function setEntradas($entradas): self {
        $this->entradas = $entradas;
        return $this;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getNombre(): ?string {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self {
        $this->nombre = $nombre;

        return $this;
    }

    public function getConfiguracion(): ?int {
        return $this->configuracion;
    }

    public function setConfiguracion(int $configuracion): self {
        $this->configuracion = $configuracion;

        return $this;
    }

    function getSistema(): ?Sistemas {
        return $this->sistema;
    }

    function setSistema(Sistemas $sistema): self {

        $this->sistema = $sistema;

        return $this;
    }

    function getColortexto(): string {

        if (is_null($this->colortexto)) {
            return "#000000";
        } else {
            return $this->colortexto;
        }
    }

    function setColortexto(string $colortexto): self {
        $this->colortexto = $colortexto;
        return $this;
    }

    function getColor(): string {

        if (is_null($this->color)) {
            return "#bdbdbd";
        } else {
            return $this->color;
        }
    }

    function setColor(string $color): self {
        $this->color = $color;

        return $this;
    }

    function getDescripcion(): string {
        return $this->descripcion;
    }

    function setDescripcion(string $descripcion): self {
        $this->descripcion = $descripcion;
        return $this;
    }

     function getNdekminimo(): ?int {
        return $this->ndekminimo;
    }

    function setNdekminimo(?int $ndekminimo): self {
        $this->ndekminimo = $ndekminimo;
          return $this;
    }

    
    /**
     * Funcion que convierte el objeto componente incluidas las salidas y entradas en 
     * un array
     *  
     * @return array Array con todos los datos del componente.
     */
    public function toArray() {

        $arraySalidas = array();

        foreach ($this->salidas as $salida) {
            $arraySalidas[] = ['id' => $salida->getId(),
                'nombre' => $salida->getNombre(),
                'operacion' => $salida->getOperacion(),
                'operacionNombre' => (!is_null($salida->getOperacionSalida()) ? $salida->getOperacionSalida()->getNombre() : null),
                'descripcion' => $salida->getDescripcion(),
                'valorCalculado' => $salida->getValorCalculado()];
        }

        $arrayEntradas = array();

        foreach ($this->entradas as $entrada) {
            $arrayEntradas[] = ['id' => $entrada->getId(),
                'nombre' => $entrada->getNombre(),
                'valor' => $entrada->getValor(),
                'valorinicial' => $entrada->getValorInicial(),
                'conexion' => (!is_null($entrada->getSalida()) ? $entrada->getSalida()->getComponente()->getNombre() . '->' . $entrada->getSalida()->getNombre() : null),
                'descripcion' => $entrada->getDescripcion()];
        }

        return [
            'id' => $this->id,
            'nombre' => $this->nombre,
            'configuracion' => $this->configuracion,
            'ndekminimo' => $this->ndekminimo,
            'descripcion' => $this->descripcion,
            'fiabilidad' => $this->getFiabilidad(),
            'color' => $this->getColor(),
            'fallo' => $this->getFallo(),
            'operacionfallo' => $this->getOperacionfallo(),
            'fallado' => $this->getFallado(),
            'porcentaje' => $this->getPorcentaje(),
            'colortexto' => $this->getColortexto(),
            'entradas' => $arrayEntradas,
            'salidas' => $arraySalidas
        ];
    }

}
