<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Metricas
 *
 * @ORM\Table(name="metricasvalores", indexes={@ORM\Index(name="PRIMARY KEY", columns={"idvalor"})})
 * @ORM\Entity(repositoryClass="App\Repository\MetricasValoresRepository")
 */
class MetricasValores {

    /**
     * @var int
     *
     * @ORM\Column(name="idvalor", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idvalor;

    /**
     * @var int
     *
     * @ORM\Column(name="idmetrica", type="bigint", nullable=false)
     */
    private $idmetrica;
    
    /**
     * @var int
     *
     * @ORM\Column(name="idejecucion", type="bigint", nullable=false)
     */
    private $idejecucion;
    
    /**
     * @var int
     *
     * @ORM\Column(name="instante", type="bigint", nullable=false)
     */
    private $instante;

    /**
     * @var float|null
     *
     * @ORM\Column(name="valor", type="bigint", nullable=true)
     */
    private $valor;

    function getIdvalor(): int {
        return $this->idvalor;
    }

    function getIdmetrica(): int {
        return $this->idmetrica;
    }

    function getInstante(): int {
        return $this->instante;
    }

    function getValor(): ?float {
        return $this->valor;
    }

    function setIdvalor(int $idvalor): void {
        $this->idvalor = $idvalor;
    }

    function setIdmetrica(int $idmetrica): void {
        $this->idmetrica = $idmetrica;
    }

    function setInstante(int $instante): void {
        $this->instante = $instante;
    }

    function setValor(?float $valor): void {
        $this->valor = $valor;
    }

    function getIdejecucion(): int {
        return $this->idejecucion;
    }

    function setIdejecucion(int $idejecucion): void {
        $this->idejecucion = $idejecucion;
    }

    /**
     * Funcion que devuelve los datos del objeto en formato json.
     * 
     * @return string|json
     * 
     * @author antonio.gomez <i52goloa@uco.es>
     */
    public function toArray() {
        
        return [
                'idmetrica' => $this->idmetrica,
            'idejecucion' => $this->idejecucion,
                'instante' => $this->instante,
                'valor' => $this->valor
        ];
        
     }
     

}
