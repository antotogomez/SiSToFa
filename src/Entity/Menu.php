<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Entity;

use Doctrine\ORM\EntityManagerInterface;
 
/**
 * Description of Menu
 *
 * Obtiene los datos del menú que se va a usar en la aplicación.
 * Devuelve el menu en forma de array para que se pueda cargar en la correspondiente plantilla.
 *
 * Obtiene las categorías genéricas (apartados del menú) a los que el usuario tiene acceso.
 * Por cada categorías obtiene las páginas (app_listados) a los que el usuario tiene acceso.
 * 
 * Únicamente se enviará la url del enlace si existe está definido en $rutas.
 * 
 * @author Antonio Gómez <i52goloa@uco.es>
 * 
 */
class Menu {

    private $em;
    private $_menu;

    /**
 * @InjectParams
 */
    function __construct(EntityManagerInterface $em)
{
    $this->em = $em;
}
    

    public function getMenu($nivel, $grupo, $rutas) {
        
        $menus =   $this->em->getRepository(AppCategoriasGenericas::class)
                ->findApartadosMenuNivel($nivel);
        
        $i=0;
        foreach ($menus as $menu) {
               
            $this->_menu[$i]["nombre"] = $menu->getNombre();
            
            $opciones =   $this->em->getRepository(AppListados::class)
                ->findOpcionesMenu($menu->getIdCategoria(),$nivel);
                $j=0;
                foreach ($opciones as $opcion) {
                
                if ($opcion->getSlug() != null  &&  $opcion->getPaginaedicionS() != null ) { 
                    // Está definido el slug y (es un listado con origendatos definido o es otro tipo de apartado con paginaedicionS definida)
                    $this->_menu[$i]["opciones"][$j] = array();
                    $this->_menu[$i]["opciones"][$j]['texto'] = $opcion->getTexto();
                    $this->_menu[$i]["opciones"][$j]['imagenS'] = $opcion->getImagenS();
                    $this->_menu[$i]["opciones"][$j]['nuevo_bloque'] = $opcion->getNuevoBloque();
                    
                    if ($opcion->getListadodatos() == 1) {
                        $this->_menu[$i]["opciones"][$j]['path'] = "listado_" . $opcion->getSlug();
                    } else {
                        $this->_menu[$i]["opciones"][$j]['path'] = $opcion->getSlug();
                    }
                    $j++;
                }
            }
            
            $i++;

        }
        
        return $this->_menu;
    }

}
