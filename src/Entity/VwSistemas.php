<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Clase que representa mediante objetos a las filas de la vista de base de datos VwSistemas.
 *
 * @ORM\Table(name="vw_sistemas", indexes={@ORM\Index(name="fk_sistemas_usuarios1_idx", columns={"usuarios_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\VwSistemasRepository")
 * 
 * @author Antonio Gómez <i52goloa@uco.es>
 */
class VwSistemas
{
    /**
     * @var int
     *
     * @ORM\Column(name="idsistema", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idsistema;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=128, nullable=false)
     */
    private $nombre;
    
    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=4096, nullable=true)
     */
    private $descripcion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="visibilidad", type="integer", nullable=true)
     */
    private $visibilidad;

    /**
     * @var int
     *
     * @ORM\Column(name="idpropietario", type="bigint", nullable=false)
     */
    private $idpropietario;
    
    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuarios_id", referencedColumnName="id")
     * })
     */
    private $usuarios;
    
     /**
     * @var string|null
     *
     * @ORM\Column(name="propietario", type="string", length=4096, nullable=true)
     */
    private $propietario;
    
    /**
     * @var string|null
     *
     * @ORM\Column(name="grupo", type="string", length=4096, nullable=true)
     */
    private $grupo;
    
    /**
     * @var string|null
     *
     * @ORM\Column(name="visibilidadtexto", type="string", length=4096, nullable=true)
     */
    private $visibilidadtexto;
    
     /**
     * @var string|null
     *
     * @ORM\Column(name="codigo", type="string", length=4096, nullable=true)
     */
    private $codigo;
    
    function getIdsistema(): int {
        return $this->idsistema;
    }

    function getNombre(): ?string {
        return $this->nombre;
    }

    function getDescripcion(): ?string {
        return $this->descripcion;
    }

    function getVisibilidad(): ?int {
        return $this->visibilidad;
    }

    function getUsuarios(): ?Usuarios {
        return $this->usuarios;
    }

    function getPropietario(): ?string {
        return $this->propietario;
    }

    function getGrupo(): ?string {
        return $this->grupo;
    }

    function getVisibilidadtexto(): ?string {
        return $this->visibilidadtexto;
    }

    function getCodigo(): ?string {
        return $this->codigo;
    }

    function getIdpropietario(): int {
        return $this->idpropietario;
    }

    function setIdpropietario(int $idpropietario): void {
        $this->idpropietario = $idpropietario;
    }


  
}
