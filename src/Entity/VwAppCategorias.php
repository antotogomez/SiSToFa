<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Clase que representa mediante objetos a las filas de la vista de base de datos VwAppCategorias.
 *
 * @ORM\Table(name="vw_app_categorias")
 * @ORM\Entity(repositoryClass="App\Repository\VwAppCategoriasRepository")
 */
class VwAppCategorias {

    /**
     * @var int
     *
     * @ORM\Column(name="idcategoria", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcategoria;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=256, nullable=true)
     */
    private $nombre;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tipo", type="bigint", nullable=true)
     */
    private $tipo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="text", length=65535, nullable=true)
     */
    private $descripcion;
    
    /**
     * @var int|null
     *
     * @ORM\Column(name="nivel", type="integer", nullable=true)
     */
    private $nivel = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="orden", type="integer", nullable=true)
     */
    private $orden = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="activado", type="boolean", nullable=false, options={"default"="1"})
     */
    private $activado = true;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigo", type="string", length=128, nullable=true)
     */
    private $codigo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valor", type="string", length=256, nullable=true)
     */
    private $valor;

     /**
     * @var string|null
     *
     * @ORM\Column(name="nombrecategoriasuperior", type="string", length=256, nullable=true)
     */
    private $nombrecategoriasuperior;
    
    public function getIdcategoria(): ?string {
        return $this->idcategoria;
    }

    public function getNombre(): ?string {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self {
        $this->nombre = $nombre;

        return $this;
    }

    public function getTipo(): ?string {
        return $this->tipo;
    }

    public function setTipo(?string $tipo): self {
        $this->tipo = $tipo;

        return $this;
    }

    public function getDescripcion(): ?string {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getNivel(): ?int {
        return $this->nivel;
    }

    public function setNivel(?int $nivel): self {
        $this->nivel = $nivel;

        return $this;
    }

    public function getOrden(): ?int {
        return $this->orden;
    }

    public function setOrden(?int $orden): self {
        $this->orden = $orden;

        return $this;
    }

    public function getActivado(): ?bool {
        return $this->activado;
    }

    public function setActivado(bool $activado): self {
        $this->activado = $activado;

        return $this;
    }

    public function getCodigo(): ?string {
        return $this->codigo;
    }

    public function setCodigo(?string $codigo): self {
        $this->codigo = $codigo;

        return $this;
    }

    public function getValor(): ?string {
        return $this->valor;
    }

    public function setValor(?string $valor): self {
        $this->valor = $valor;

        return $this;
    }
    function getNombrecategoriasuperior(): ?string {
        return $this->nombrecategoriasuperior;
    }

    function setNombrecategoriasuperior(?string $nombrecategoriasuperior): self {
        $this->nombrecategoriasuperior = $nombrecategoriasuperior;
        
        return $this;
    }

}
