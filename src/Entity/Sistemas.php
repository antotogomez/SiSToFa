<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Exception;
use App\Services\Funciones;

/**
 * Sistemas
 *
 * @ORM\Table(name="sistemas", indexes={@ORM\Index(name="fk_sistemas_usuarios1_idx", columns={"usuarios_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\SistemasRepository")
 */
class Sistemas {

    /**
     * @var int
     *
     * @ORM\Column(name="idsistema", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idsistema;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=128, nullable=false)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=4096, nullable=true)
     */
    private $descripcion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="visibilidad", type="integer", nullable=true)
     */
    private $visibilidad;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuarios_id", referencedColumnName="id")
     * })
     */
    private $usuarios;

    /**
     * Un sistema tiene muchos componentes
     * 
     * @ORM\OneToMany(targetEntity="Componentes", mappedBy="sistema")
     */
    private $componentes;
    private $historial;
    private $fiabilidad;
    private $reduccion;

    function getFiabilidad() {
        return $this->fiabilidad;
    }

    function getReduccion() {
        return $this->reduccion;
    }

    function setFiabilidad($fiabilidad): void {
        $this->fiabilidad = $fiabilidad;
    }

    function setReduccion($reduccion): void {
        $this->reduccion = $reduccion;
    }

    function getComponentes() {
        return $this->componentes;
    }

    function setComponentes($componentes): void {
        $this->componentes = $componentes;
    }

    public function getIdsistema(): ?string {
        return $this->idsistema;
    }

    public function getNombre(): ?string {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self {
        $this->nombre = $nombre;

        return $this;
    }

    function getDescripcion(): ?string {
        return $this->descripcion;
    }

    function setDescripcion(?string $descripcion): self {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getVisibilidad(): ?int {
        return $this->visibilidad;
    }

    public function setVisibilidad(?int $visibilidad): self {
        $this->visibilidad = $visibilidad;

        return $this;
    }

    public function getUsuarios(): ?Usuarios {
        return $this->usuarios;
    }

    public function setUsuarios(?Usuarios $usuarios): self {
        $this->usuarios = $usuarios;

        return $this;
    }

    function getHistorial() {
        return $this->historial;
    }

    public function getSalida($id) {

        foreach ($this->componentes as $componente) {
            $salida = $componente->getSalidas()->first("id", $id);
            if (!is_null($salida)) {
                break;
            }
        }

        return $salida;
    }

    public function getEntrada($id) {

        foreach ($this->componentes as $componente) {
            $entrada = $componente->getEntradas()->first("id", $id);
            if (!is_null($entrada)) {
                break;
            }
        }

        return $entrada;
    }

    /**
     * Busca que un componente del sistemas no exista ya con ese mismo nombre.
     * 
     * @param String $nombre Nombre que se va a verificar.
     * @param int $id Identificador del componente que ya tiene ese nombre.
     * 
     * @return Boolean Devuelve true si encuntra el nombre con otro id, False en cualquier otro caso.
     * 
     */
    public function existeNombreComponente(String $nombre, int $id) {

        foreach ($this->componentes as $componente) {
            if ($componente->getNombre() == $nombre && ($componente->getId() != $id)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Devuelve el componente del sistema que tenga el id indicado.
     * 
     * @param int $id Identificador del componente a buscar en el sistema.
     * 
     * @return Componente Devuelve un objeto de tipo componente cuyo id sea igual al indicado.
     * 
     */
    public function getComponenteId($idcomponente) {

        foreach ($this->componentes as $componente) {
            if ($componente->getId() == $idcomponente) {
                return $componente;
                break;
            }
        }

        return null;
    }

    /**
     * Devuelve el componente del sistema que tenga el nombre indicado.
     * 
     * @param int $id Identificador del componente a buscar en el sistema.
     * 
     * @return Componente Devuelve un objeto de tipo componente cuyo id sea igual al indicado.
     * 
     */
    public function getComponenteNombre($nombre) {

        foreach ($this->componentes as $componente) {
            if ($componente->getNombre() == $nombre) {
                return $componente;
                break;
            }
        }

        return null;
    }

    /**
     * Establece el valor indicado en la entrada
     * 
     * @param int $identrada Identificador de la entrada a actualizar
     * @param float $valor Nuevo valor a establecer.
     * 
     * @return void
     * 
     */
    public function setValorEntrada($identrada, $valor) {

        foreach ($this->componentes as $componente) {
            foreach ($componente->getEntradas() as $entrada) {
                if ($entrada->getId() == $identrada) {
                    $entrada->setValor($valor);
                    break;
                }
            }
        }
    }

    /**
     * Establece el valor indicado en la salida
     * 
     * @param int $identrada Identificador de la salida a actualizar
     * @param float $valor Nuevo valor a establecer.
     * 
     * @return void
     * 
     */
    public function setValorSalida($idsalida, $valor) {

        foreach ($this->componentes as $componente) {
            foreach ($componente->getSalidas() as $salida) {
                if ($salida->getId() == $idsalida) {
                    $salida->setValorCalculado($valor);
                    break;
                }
            }
        }
    }

    /**
     * Establece el estado del componente indicado
     * 
     * @param int $idcomponente Identificador del componente al que se va modificar el estado
     * @param boolean $fallo Si al componente se le ha establecido el estado a fallo.
     * @param int $operacionfallo Identificador de la operación a aplicar al fallo.
     * @param float $porcentaje Probabilidad de fallo del componente.
     * 
     * @return void
     * 
     */
    public function setEstadoComponente($idcomponente, $fallo, $operacionfallo = null, $porcentaje = null) {

        foreach ($this->componentes as &$componente) {
            if ($componente->getId() == $idcomponente) {

                $componente->setFallo($fallo);
                $componente->setOperacionfallo($operacionfallo);

                $componente->setPorcentaje($porcentaje);

                break;
            }
        }
    }

    /**
     * Calcula los valores de salida de un componente en base a sus valores de entrada.
     * 
     * @param Componente $componente al que calcularle las salidas.
     * @retun void. Establece los valores de salida en el componente.
     * 
     * @throws Exception
     */
    public function calcularComponente($componente) {

        //var_dump($componente);
        $componente->incrementaIterador();
        //echo $componente->getIterado();

        if ($componente->getIterado() > 1) {
            $message = "Se ha producido un error al calcular los valores de salida del sistema. Compruebe que el sistema no tenga bucles.";
            throw new Exception($message);
        }

        //echo "Componente a calcular: " . $componente->getNombre() . "<br/>";
        //comprueba si las entradas del componente estan calculadas
        foreach ($componente->getEntradas() as &$entrada) {
            if ($entrada->getSalida() != null) {

                if (!$entrada->getSalida()->getComponente()->getCalculado()) {

                    $this->calcularComponente($entrada->getSalida()->getComponente());
                }
            }
        }

        //Calculadas las entradas, puede calcular las salidas.
        foreach ($componente->getSalidas() as &$salida) {

            //echo $salida->getOperacionsalida()->getValor() . "<br/>";
            $class = $salida->getOperacionsalida()->getValor();
            $operacion = new $class();

            $resultado = $operacion->operacion($componente->getEntradas());

            //Si el componente esta en fallo, aplica el error.
            //var_dump($componente->getFallado());
            if ($componente->getFallado()) {
                Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "]" . $componente->getNombre() . " esta fallado.");
                //Funciones::logInfo("Valor del componente en fallo es: " . print_r($componente, true));
                //puede fallar por falta de fiabilidad, en este caso se aplica el no hay resultado

                $class;

                if ($componente->getOperacionfallo() == null) {
                    $class = "App\Operaciones\ErrorNoResponse";
                } else {
                    $class = $componente->getOperacionfallo()->getValor();
                }

                $operacion = new $class();
                $salida->setValorCalculado($operacion->error($resultado));
                //$this->setValorSalida($salida->getId(), $operacion->error($resultado));
                Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "]" . "El resultado del error es:" . ($operacion->error($resultado) == null ? "NULL" : $operacion->error($resultado)));
                Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "]" . "Y la salida tiene como valor: " . ($salida->getValorCalculado() == null ? "NULL" : $salida->getValorCalculado()));
            } else {
                $salida->setValorCalculado($resultado);
                //$this->setValorSalida($salida->getId(), $resultado);
            }

            foreach ($salida->getEntradas() as &$entrada) {

                Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "]" . "Entrada: " . $entrada->getComponente()->getNombre() . "::" . $entrada->getNombre() . " valor original: " . $entrada->getValor() . " Valor de la salida conectada: " . ($salida->getValorCalculado() == null ? "NULL" : $salida->getValorCalculado()));
                $entrada->setValor($salida->getValorCalculado());
                //$this->setValorEntrada($entrada->getId(), $salida->getValorCalculado());
                Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "]" . "Entrada: " . $entrada->getComponente()->getNombre() . "::" . $entrada->getNombre() . " valor calculado: " . $entrada->getValor());
                //Funciones::logDebug( "[" . basename(__FILE__) . ":" . __LINE__ . "]" .print_r($this->toArray(),true));
            }

            $componente->setCalculado(true);
        }
    }

    /**
     * Calcula la fiabilidad del sistema segun los componentes y la configuración de estos
     * 
     * @retun void. Establece el valor de la fiabilidad del sistema.
     * 
     * @throws Exception. Si no es posible calcular esta fiabilidad.
     */
    public function calculaFiabilidad() {

        Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "]" . "Calcula Fiabilidad [" . $this->idsistema . "] " . $this->nombre . "______________________________________");

        $conexiones = $this->obtenerListaAdyacencia();
        //Funciones::logDebug( "[" . basename(__FILE__) . ":" . __LINE__ . "]" . print_r($conexiones,true) );            
        $elementos = count($conexiones);
        while (count($this->ReducirListaAdyacencia($conexiones)) > 1) {

            //Funciones::logDebug( "[" . basename(__FILE__) . ":" . __LINE__ . "]" . print_r($conexiones,true) );            
            if ($elementos > count($conexiones)) {
                $elementos = count($conexiones);
            } else {
                //Ha dado una vuelta sin reducir nada, da error
                $message = "Se ha producido un error al calcular la fiabilidad del sistema. Compruebe que el sistema es coherente.";
                throw new Exception($message, 0);
            }
        }

        //Funciones::logDebug( "[" . basename(__FILE__) . ":" . __LINE__ . "]" . print_r($conexiones,true) ); 

        foreach ($conexiones as $reducido) {
            $this->fiabilidad = $reducido["fiabilidad"];
            $this->reduccion = $reducido["nombre"];
        }
    }

    /**
     * Calcula la matriz de adyanceia del sistema para determinar las conexiónes
     * entre componentes.
     * 
     * @return array. Array con la matriz de adyacencia del sistema.
     */
    public function obtenerListaAdyacencia() {

        //comprueba si las entradas del componente estan calculadas
        //Recorre todos los componentes y crea una matriz de conexiones
        //Crea una matriz de conexiones, con la dimensión del numero de componentes.
        $conexiones = array();

        foreach ($this->componentes as $componente) {

            $conexiones[$componente->getId()]["id"] = $componente->getId();
            $conexiones[$componente->getId()]["nombre"] = $componente->getNombre();
            $conexiones[$componente->getId()]["fiabilidad"] = $componente->getFiabilidad();
            $conexiones[$componente->getId()]["configuracion"] = $componente->getConfiguracion();
            $conexiones[$componente->getId()]["ndekminimo"] = $componente->getNdekminimo();

            Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "]" . "Fiabilidad de " . $componente->getNombre() . "(" . $componente->getFiabilidad() . ")");
        }

        $j = 0;
        foreach ($this->componentes as $componente) {

            $conexiones[$componente->getId()]["conectado"] = array();

            //Calculadas las entradas, puede calcular las salidas.
            foreach ($componente->getSalidas() as $salida) {

                foreach ($salida->getEntradas() as $entrada) {

                    $conexiones[$componente->getId()]["conectado"][] = $entrada->getComponente()->getId();
                }
            }

            $j++;
        }

        
        return $conexiones;
    }

    /**
     * Calcula el factorial de un numero dado.
     * 
     * @param float $umero. Valor númerico al que calcularle el factorial
     * @return float. Factorial del numero dado.
     */
    private function factorial($numero) {

        $fact = 1;
        for ($i = $numero; $i >= 1; $i--) {
            $fact = $fact * $i;
        }

        return $fact;
    }

    /**
     * Calcula el numero de combinaciones de sup_n elementos tomados de inf_m en inf_m
     * 
     * @param int $sup_n. Numero total de elementos
     * @param int $inf_m. Numero de elemento a tomar.
     * 
     * @return float. Resultado de la combinación.
     */
    private function numeroCombinatorio($sup_n, $inf_m) {

        //echo  "(" . $sup_n . "-" . $inf_m . ") = (" . $this->factorial($sup_n) . "/ ( " . ($this->factorial($inf_m) . "·" . $this->factorial($sup_n - $inf_m)) . ") = " . $this->factorial($sup_n) / ($this->factorial($inf_m) * $this->factorial($sup_n - $inf_m));

        return $this->factorial($sup_n) / ($this->factorial($inf_m) * $this->factorial($sup_n - $inf_m));
    }

    /**
     * Reduce la lista de conexiones entre componentes aplicando las reglas de 
     * equivalencia de fiablidad de sistemas.
     * 
     * @param Array $conexiones. Matriz de adyacencia del sistema.
     * 
     * @return Array. Lista reducida.
     * 
     * @throws Exception. Si no es posible sustituir componentes por sus equivalentes.
     */
    private function ReducirListaAdyacencia(&$conexiones) {

        Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "]" . "Matriz de conexiones:" . print_r($conexiones,true));
        
        try {

            //Presupone que todos estan en paralelo.
            $todosparalelos = true;

            //Busca los elementos en serie para agruparlos
            foreach ($conexiones as $key => &$conexion) {
                //DEBUG: echo "<h2>" . $key . " ---------------------------------------------------------------------</h2>";

                if (count($conexion["conectado"]) == 1) {
                    //Si este tiene una conexión, ya no estan en paralelo.
                    $todosparalelos = false;
                    //Solo tiene una conexión, busca si la tiene alguien mas.
                    $idconectado = $conexion["conectado"][0];
                    $masconexiones = $this->buscarOtrasConexion($idconectado, $key, $conexiones);

                    if (count($masconexiones) == 0) {

                        //DEBUG: echo "<h1>" . "EN SERIE!!!" . "</h1>";
                        //Une los dos componentes.
                        //DEBUG: var_dump($conexiones);
                        Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "]" . "Fiabilidad de " . $conexion["nombre"] . "(" . $conexion["fiabilidad"] . ")*" . $conexiones[$idconectado]["nombre"] . "(" . $conexiones[$idconectado]["fiabilidad"] . ") es:" . $conexion["fiabilidad"] * $conexiones[$idconectado]["fiabilidad"]);
                        $conexion["fiabilidad"] = $conexion["fiabilidad"] * $conexiones[$idconectado]["fiabilidad"];

                        $conexion["nombre"] = "(" . $conexion["nombre"] . " && " . $conexiones[$idconectado]["nombre"] . ")";
                        $conexion["conectado"] = array();
                        for ($i = 0; $i < count($conexiones[$idconectado]["conectado"]); $i++) {
                            $conexion["conectado"][] = $conexiones[$idconectado]["conectado"][$i];
                        }

                        //Elimina el elemento
                        unset($conexiones[$idconectado]);
                    } else {
                        //DEBUG: echo $conexion["nombre"] . "-->>" . $conexiones[$masconexiones[0]]["nombre"];
//                      //Terminan en un paralelo, asi es que los une todos, pero puede que no esten iniciados en el mismo
//                      // sistio, asi es que lo comprueba.
//                      
                        $conectados = true;
                        //Hay mas conexiones, pero puede que no se inicien en el mismo punto, se comprueba.
                        $predecesoresComponente = $this->buscarOtrasConexion($conexion["id"], $conexion["id"], $conexiones);
                        foreach ($predecesoresComponente as $predecesor) {
                            //DEBUG: echo "<br/>Predecesor: " . $predecesor;

                            foreach ($masconexiones as $candidato) {

                                $predecesoresCandidatos = $this->buscarOtrasConexion($conexion["id"], $candidato, $conexiones);
                                //DEBUG: var_dump($predecesoresCandidatos);
                                foreach ($predecesoresCandidatos as $predecesorCandidato) {
                                    //DEBUG: echo "<br/>" . $predecesor . "==" . $predecesorCandidato . "?</br>";
                                    if ($predecesor != $predecesoresCandidatos) {
                                        //DEBUG: echo "tienen diferentes predecesores, descarta la opcion";
                                        $conectados = false;
                                        break;
                                    }
                                }
                            }
                        }

                        if ($conectados) {
                            $nombre = $conexion["nombre"];
                            $fiabilidad = 1;
                            foreach ($masconexiones as $otraconexion) {
                                //echo $otraconexion;
                                if ($conexion["configuracion"] == 2) {
                                    $nombre = $nombre . " # " . $conexiones[$otraconexion]["nombre"];
                                    echo "<h1> Nombre: " . $nombre . " [" . __LINE__ . "]</h1>";
                                    //foreach($componentes as $componente) {
                                    //   $fiabilidad = $fiabilidad + numeroCombinatorio($numeroComponentes, $minimoComponentes) * ($componente["fiabilidad"]**$minimoComponentes) * ((1 - $componente["fiabilidad"])**($numeroComponentes - $minimoComponentes));
                                    //}
                                } else {
                                    $nombre = $nombre . " || " . $conexiones[$otraconexion]["nombre"];
                                }

                                Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "]" . "UNIDOS:" . $nombre . ". Fiabilidad: " . $fiabilidad);
                                Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "]" . "fiabilidad=" . $fiabilidad . " * (1 - " . $conexiones[$otraconexion]["fiabilidad"] . ")");

                                $fiabilidad = $fiabilidad * (1 - $conexiones[$otraconexion]["fiabilidad"]);

                                Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "]" . "UNIDOS:" . $nombre . ". Fiabilidad: " . $fiabilidad);

                                unset($conexiones[$otraconexion]);
                            }

                            $conexion["nombre"] = "(" . $nombre . ")";
                            $conexion["fiabilidad"] = 1-$fiabilidad;
                        }
                    }
                } else {
                    if (count($conexion["conectado"]) > 1) {
                        //Tiene mas de una conexión, aún hay componentes por agrupar.
                        $todosparalelos = false;
                        //El elemento tiene mas de una conexión, son paralelos
                        //DEBUG: echo "<h1>posible paralelos</h1>";
                        //Si todos tienen el mismo destino, se puede calcular como paralelo.
                        $destino = null;

                        for ($i = 0; $i < count($conexion["conectado"]); $i++) {
//                            echo "destino:" . $destino . "<br/>";
//                            echo $conexion["conectado"][$i];
//                            var_dump($conexiones[$conexion["conectado"][$i]]);
//                            
                            if ($destino == null) {
                                if (count($conexiones[$conexion["conectado"][$i]]["conectado"]) == 1) {
                                    $destino = $conexiones[$conexion["conectado"][$i]]["conectado"][0];
                                } else {
                                    $destino = "FIN";
                                }

                                $fiabilidad = (1 - $conexiones[$conexion["conectado"][$i]]["fiabilidad"]);
                                $nombre = $conexiones[$conexion["conectado"][$i]]["nombre"];
                            } else {
                                $iguales = false;
                                if (count($conexiones[$conexion["conectado"][$i]]["conectado"]) == 1) {
                                    if ($destino == $conexiones[$conexion["conectado"][$i]]["conectado"][0]) {
                                        $iguales = true;
                                    }
                                } else {
                                    $iguales = true;
                                }

                                if ($iguales == true) {
                                    //Efectivamente, estan en paralelo.
                                    //Pero puede ser una configuración n-de-k
                                    if ($conexiones[$conexion["conectado"][$i]]["configuracion"] == 2) {
                                        if ($i == 0) {
                                            $fiabilidad = 0;
                                        }
                                        //echo "<h1> Nombre: " .$nombre . " [" . __LINE__  . "]" . count($conexion["conectado"]) . " Rt: " . $fiabilidad . "</h1>";
                                        $componente = $conexiones[$conexion["conectado"][$i]];
                                        $nombre = $nombre . " # " . $conexiones[$conexion["conectado"][$i]]["nombre"];
                                        //La fiabilidad la calcula mas adelante, cuando tiene todos los datos
                                    } else {
                                        $nombre = $nombre . " || " . $conexiones[$conexion["conectado"][$i]]["nombre"];
                                        $fiabilidad = $fiabilidad * (1 - $conexiones[$conexion["conectado"][$i]]["fiabilidad"]);
                                    }

                                    Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "] Pone en " . $conexiones[$conexion["conectado"][$i]]["configuracion"] . "(" . $conexiones[$conexion["conectado"][$i]]["ndekminimo"] . ") : " . $conexiones[$conexion["conectado"][$i]]["nombre"] . "");
                                } else {
                                    //Hay uno que rompe el paralelo. Se deja igual y se sigue reduciendo.

                                    $nombre = null;
                                    break;
                                }
                            }
                        }

                        if ($nombre != null) {
                            //Si son paralelos. Calcula y agrupa.
                            $conexiones[$conexion["conectado"][0]]["nombre"] = "(" . $nombre . ")";
                            $conexiones[$conexion["conectado"][0]]["nombre"] = "(" . $nombre . ")";

                            //Aqui calculamos la fiabilidad si es un kden.
                            if ($conexiones[$conexion["conectado"][0]]["configuracion"] == 2) {
                                $fiabilidad = 0;
                                $r = $conexiones[$conexion["conectado"][0]]["ndekminimo"];
                                //var_dump($conexion["conectado"]);
                                foreach ($conexion["conectado"] as $i) {

                                    //echo "<h1> ( " .count($conexion["conectado"]) . "-"  . $r. " ) = " . $this->numeroCombinatorio(count($conexion["conectado"]), $r) . " " .  ($conexiones[$i]["fiabilidad"]**$r) * ((1 - $conexiones[$i]["fiabilidad"])**(count($conexion["conectado"]) - $r))  . "</h1>";
                                    $fiabilidad = $fiabilidad + ($this->numeroCombinatorio(count($conexion["conectado"]), $r) * ($conexiones[$i]["fiabilidad"] ** $r) * ((1 - $conexiones[$i]["fiabilidad"]) ** (count($conexion["conectado"]) - $r)));
                                    $r++;
                                    if ($r > count($conexion["conectado"])) {
                                        break;
                                    }
                                }

                                $conexiones[$conexion["conectado"][0]]["fiabilidad"] = $fiabilidad;
                            } else {
                                $conexiones[$conexion["conectado"][0]]["fiabilidad"] = (1 - $fiabilidad);
                            }

                            for ($i = (count($conexion["conectado"]) - 1); $i > 0; $i--) {
                                $posicion = array_search($conexiones[$conexion["conectado"][$i]]["id"], $conexion["conectado"]);
                                //Funciones::logDebug( "[" . basename(__FILE__) . ":" . __LINE__ . "] Elimina elemento: [" . $i . "]". print_r($conexiones[$conexion["conectado"][$i]],true) . "" );            
                                unset($conexiones[$conexion["conectado"][$i]]);
                                unset($conexion["conectado"][$posicion]);
                            }
                        } else {
                            //DEBUG: echo "<h1>NO paralelos</h1>";
                        }
                    }
                }
            }

            //Por ultimo, comprueba si lo que queda no tienen mas conexiones, son elementos idependientes
            // y se considera que son paralelos.            
            if ($todosparalelos == true) {
                $fiabilidad = 1;
                $nombre = "";
                $primer = true;
                $kden = false;

                foreach ($conexiones as $key => &$conexion) {
                    if ($conexion["configuracion"] == 2) {
                        $kden = true;
                        if ($primer) {
                            $fiabilidad = 0;
                            $primer = false;
                            $i = $conexion["ndekminimo"];
                        }

                        if ($nombre != "") {
                            $nombre = $nombre . " # " . $conexion["nombre"];
                        } else {
                            $nombre = $conexion["nombre"];
                        }
                        //echo "<h1> ( " .count($conexiones) . "-"  . $i. " ) = " . $this->numeroCombinatorio(count($conexiones), $i) . " " .  ($conexion["fiabilidad"]**$i) * ((1 - $conexion["fiabilidad"])**(count($conexiones) - $i))  . "</h1>";
                        $fiabilidad = $fiabilidad + ($this->numeroCombinatorio(count($conexiones), $i) * ($conexion["fiabilidad"] ** $i) * ((1 - $conexion["fiabilidad"]) ** (count($conexiones) - $i)));
                        $i++;
                        if ($i > count($conexiones)) {
                            break;
                        }
                    } else {
                        $fiabilidad = $fiabilidad * (1 - $conexion["fiabilidad"]);
                        if ($nombre != "") {
                            $nombre = $nombre . " || " . $conexion["nombre"];
                        } else {
                            $nombre = $conexion["nombre"];
                        }
                    }
                }

                $i = 0;

                foreach ($conexiones as $key => &$conexion) {
                    if ($i == 0) {
                        $conexiones[$key]["nombre"] = "(" . $nombre . ")";
                        if ($kden) {
                            $conexiones[$key]["fiabilidad"] = $fiabilidad;
                        } else {
                            $conexiones[$key]["fiabilidad"] = 1 - $fiabilidad;
                        }
                        $conexiones[$key]["conectado"] = array();
                        $conexiones[$key]["configuracion"] = 0;
                    } else {
                        unset($conexiones[$key]);
                    }
                    $i++;
                }

                //Elimina el resto
                for ($i = 1; $i < count($conexiones); $i++) {
                    unset($conexiones[$i]);
                }
            }
        } catch (\Exception $ex) {

            $message = "Se ha producido un error al calcular la fiabilidad del sistema. Compruebe que el sistema es coherente.:";

            throw new \Exception($message, 0, $ex);
        }

        //var_dump($conexiones);
        return $conexiones;
    }

    /**
     * Busca si existen otras conexiones como la actual, para determinar si hay
     * caminos paralelos.
     * 
     * @param int $id. Identificador del compoente actual.
     * @param int $keyactual
     * @param array $lista de conexiones.
     * 
     * @return array. Lista de conexiones paralelas. Array vacio si no hay ninguna.
     * 
     */
    private function buscarOtrasConexion($id, $keyactual, $lista) {


        $componentesconectados = array();
        foreach ($lista as $key => $elemento) {
            if ($key != $keyactual) {
                $posicion = array_search($id, $elemento["conectado"]);
                if ($posicion !== false) {
                    $componentesconectados[] = $key;
                }
            }
        }

        return $componentesconectados;
    }

    /**
     * Calcula el valor de salida del sistema recorriendo todos los componentes.
     * 
     * @param boolean $estatico. Si el calculo es para la simulacion estatica o dinamica.
     * 
     * @throws Exception. si no es posible realizar el calculo.
     */
    public function calcular($estatico = false) {

        try {

            foreach ($this->componentes as &$componente) {

                //calcula el estado de error
                $componente->setCalculado(false);
                $componente->CalculaError($estatico);
                $componente->reestableceIterador();
            }

            foreach ($this->componentes as &$componente) {
                //echo "Componente: " . $componente->getNombre() . "<br/>";

                if (!$componente->getCalculado()) {
                    $this->calcularComponente($componente);
                } else {
                    //echo "Componente ya calculado: " . $componente->getNombre() . "<br/>";
                }
            }
        } catch (\Exception $ex) {

            $message = "Se ha producido un error al calcular los valores del sistema. Compruebe que el sistema es coherente.";

            throw new \Exception($message, 0, $ex);
        }
    }

    /**
     * Devuelve un array con toda la información del sistema.
     * 
     * @return Array. Array con toda la información del sistema.
     * 
     */
    public function toArray() {

        $arrayComponentes = array();

        foreach ($this->componentes as $componente) {
            $arrayComponentes[] = $componente->toArray();
        }

        return [
            'idsistema' => $this->idsistema,
            'nombre' => $this->nombre,
            'fiabilidad' => $this->fiabilidad,
            'componentes' => $arrayComponentes,
        ];
    }
}
