<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Entity;

use App\Entity\AppCategoriasGenericas;
use Doctrine\ORM\Mapping as ORM;

/**
 * Eventos
 *
 * @ORM\Table(name="eventos", indexes={@ORM\Index(name="fk_eventos_simulaciones1_idx", columns={"simulaciones_id"})})
 * @ORM\Entity
 */
class Eventos {

    /**
     * @var int
     *
     * @ORM\Column(name="idevento", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idevento;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=128, nullable=false)
     */
    private $nombre;

    /**
     * @var int
     *
     * @ORM\Column(name="instante", type="bigint", nullable=false)
     */
    private $instante;

    /**
     * @var int|null
     *
     * @ORM\Column(name="entrada_id", type="bigint", nullable=true)
     */
    private $entradaid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="valor", type="bigint", nullable=true)
     */
    private $valor;

    /**
     * @var int|null
     *
     * @ORM\Column(name="componente_id", type="bigint", nullable=true)
     */
    private $componenteid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="estado", type="bigint", nullable=true)
     */
    private $estado;

    /**
     * @var \Simulaciones
     *
     * @ORM\ManyToOne(targetEntity="Simulaciones", inversedBy="eventos")
     * @ORM\JoinColumn(name="simulaciones_id", referencedColumnName="id")
     */
    private $simulaciones;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tipofallo", type="integer", nullable=true)
     */
    private $tipofallo;

    /**
     * @ORM\OneToOne(targetEntity="AppCategoriasGenericas", fetch="EAGER")
     * @ORM\JoinColumn(name="tipofallo", referencedColumnName="idcategoria", nullable=true)
     */
    private $operacionfallo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="porcentajefallo", type="bigint", nullable=true)
     */
    private $porcentajefallo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=4096, nullable=true)
     */
    private $descripcion;

    public function getOperacionfallo() {
        return $this->operacionfallo;
    }

    public function getIdevento(): ?string {
        return $this->idevento;
    }

    public function getInstante(): ?string {
        return $this->instante;
    }

    public function setInstante(string $instante): self {
        $this->instante = $instante;

        return $this;
    }

    public function getSimulaciones(): ?Simulaciones {
        return $this->simulaciones;
    }

    public function setSimulaciones(?Simulaciones $simulaciones): self {
        $this->simulaciones = $simulaciones;

        return $this;
    }

    function getEntradaid(): ?int {
        return $this->entradaid;
    }

    function getValor(): ?int {
        return $this->valor;
    }

    function getComponenteid(): ?int {
        return $this->componenteid;
    }

    function getEstado(): ?int {
        return $this->estado;
    }

    function setEntradaid(?int $entradaid): self {
        $this->entradaid = $entradaid;
        return $this;
    }

    function setValor(?int $valor): self {
        $this->valor = $valor;
        return $this;
    }

    function setComponenteid(?int $componenteid): self {
        $this->componenteid = $componenteid;
        return $this;
    }

    function setEstado(?int $estado): self {
        $this->estado = $estado;
        return $this;
    }

    function getNombre(): string {
        return $this->nombre;
    }

    function setNombre(string $nombre): self {
        $this->nombre = $nombre;
        return $this;
    }

    function getDescripcion(): string {
        return $this->descripcion;
    }

    function setDescripcion(string $descripcion): self {
        $this->descripcion = $descripcion;
        return $this;
    }

    function setTipofallo(?int $tipofallo): self {
        $this->tipofallo = $tipofallo;
        return $this;
    }

    function getTipofallo(): ?int {
        return $this->tipofallo;
    }

    function getPorcentajefallo(): ?float {
        return $this->porcentajefallo;
    }

    function setPorcentajefallo(?float $porcentajefallo): self {
        $this->porcentajefallo = $porcentajefallo;
        return $this;
    }

    /**
     * Funcion que devuelve los datos del objeto en forma de array.
     * 
     * @return array
     */
    public function toArray(): array {

        return [
            'id' => $this->idevento,
            'nombre' => $this->nombre,
            'descripcion' => $this->descripcion,
            'instante' => $this->instante,
            'componenteid' => $this->componenteid,
            'estado' => $this->estado,
            'entradaid' => $this->entradaid,
            'valor' => $this->valor,
            'tipofallo' => $this->tipofallo,
            'porcentajefallo' => $this->porcentajefallo
        ];
    }

}
