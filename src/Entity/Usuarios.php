<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Usuarios
 *
 * @ORM\Table(name="usuarios", indexes={@ORM\Index(name="fk_usuarios_grupos1_idx", columns={"grupos_idgrupo"})})
 * @ORM\Entity(repositoryClass="App\Repository\UsuariosRepository")
 */
class Usuarios implements UserInterface {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=128, nullable=false)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=128, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos", type="string", length=128, nullable=false)
     */
    private $apellidos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=256, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="password", type="string", length=256, nullable=true)
     */
    private $password;

    /**
     * @var int
     *
     * @ORM\Column(name="estado", type="integer", nullable=false)
     */
    private $estado;

    /**
     * @var int
     *
     * @ORM\Column(name="nivel", type="integer", nullable=false)
     */
    private $nivel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dni", type="string", length=9, nullable=true)
     */
    private $dni;

    /**
     * @var int
     *
     * @ORM\Column(name="grupos_idgrupo", type="integer", nullable=false)
     */
    private $gruposIdgrupo;
    
    /**
     * Muchas entradas pertenecen estas conectadas a una salida
     * 
     * @var Grupos
     * 
     * @ORM\ManyToOne(targetEntity="Grupos", inversedBy="grupo")
     * @ORM\JoinColumn(name="grupos_idgrupo", referencedColumnName="idgrupo")
     */
    private $grupo;
    
    function getGrupo(): Grupos {
        return $this->grupo;
    }

    function setGrupo(Grupos $grupo): self {
        $this->grupo = $grupo;
         return $this;
    }

        public function getId(): ?string {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getLogin(): ?string {
        return $this->login;
    }

    public function setLogin(string $login): self {
        $this->login = $login;

        return $this;
    }

    public function getNombre(): ?string {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self {
        $this->nombre = $nombre;

        return $this;
    }

    public function getNombreCompleto(): ?string {
        return $this->nombre . " " . $this->apellidos;
    }

    public function getApellidos(): ?string {
        return $this->apellidos;
    }

    public function setApellidos(string $apellidos): self {
        $this->apellidos = $apellidos;

        return $this;
    }

    public function getEmail(): ?string {
        return $this->email;
    }

    public function setEmail(?string $email): self {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string {
        return $this->password;
    }

    public function setPassword(?string $password): self {
        $this->password = $password;

        return $this;
    }

    public function getEstado(): ?int {
        return $this->estado;
    }

    public function setEstado(int $estado): self {
        $this->estado = $estado;

        return $this;
    }

    public function getNivel(): ?int {
        return $this->nivel;
    }

    public function setNivel(int $nivel): self {
        $this->nivel = $nivel;

        return $this;
    }

    public function getDni(): ?string {
        return $this->dni;
    }

    public function setDni(?string $dni): self {
        $this->dni = $dni;

        return $this;
    }

    function getGruposIdgrupo(): ?int {
        return $this->gruposIdgrupo;
    }

    function setGruposIdgrupo(?int $gruposIdgrupo): self {
        $this->gruposIdgrupo = $gruposIdgrupo;
        return $this;
    }

    
    public function getUsername() {
        return $this->login;
    }

    public function getRoles(): array {
        $roles = [];

        $roles[] = 'ROLE_USER';
        $roles[] = 'ROLE_ADMIN';
        $roles[] = 'IS_AUTHENTICATED_FULLY';

        return array_unique($roles);
    }

    public function getSalt() {
        
    }

    public function eraseCredentials() {
        
    }

    function getHistorial(): array {

        return array();
    }

}
