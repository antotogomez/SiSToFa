<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Services;

/**
 * Clase con funciones y utilidades genericas usadas en toda la aplicación, principalmente
 * de obtención de valores de configuración y escribir en ficheros de traza.
 * 
 * @author Antonio Gómez <i52goloa@uco.es>
 */
class Funciones {

    /**
     * Funcion estatica que cscribe una cadena con texto de información en el fichero establecido en el entorno
     * 
     * @param $texto string Cadena con el texto de información a escribir.
     * @return  null    no devuelve nada.
     */
    public static function logInfo($texto) {

        $fp = fopen(Funciones::getConfig("FICHERO_TRAZA"), 'a');
        fwrite($fp, date('Y-m-d H:i:s') . " INF: " . $texto . "\n");
        fclose($fp);
    }

    /**
     * Funcion estatica que cscribe una cadena con texto de error en el fichero establecido en el entorno
     * 
     * @param $texto string Cadena con el texto de error a escribir.
     * @return  null    no devuelve nada.
     */
    public static function logError($texto) {

        $fp = fopen(Funciones::getConfig("FICHERO_TRAZA"), 'a');
        fwrite($fp, date('Y-m-d H:i:s') . " ERR: " . $texto . "\n");
        fclose($fp);
    }

    /**
     * Funcion estatica que cscribe una cadena con texto de depuración en el fichero establecido en el entorno
     * 
     * @param $texto string Cadena con el texto de depuración a escribir.
     * @return  null    no devuelve nada.
     */
    public static function logDebug($texto) {

        if(Funciones::getConfig("DEPURANDO") == "TRUE") {
            $fp = fopen(Funciones::getConfig("FICHERO_TRAZA"), 'a');
            fwrite($fp, date('Y-m-d H:i:s') . " DEB: " . $texto . "\n");
            fclose($fp);
        }
    }

    /**
     * Escribe una cadena con texto de depuración en un fichero de texto para posterior
     * comprobación de errores.
     * 
     * @param $texto string Cadena con el texto de depuración a escribir.
     * @param $show  boolean    Verdadero/Falso para indicar si se muestra el mensaje de deputación en la pantalla como
     * parte de la salida estandar.
     * 
     * @return  null    no devuelve nada.
     * 
     */
    static function getParameter($parametro) {

        try {
            return $_ENV[strtoupper($parametro)];
        } catch (\Exception $e) {
            return "";
        }
    }

    /**
     * Funcion estatica que obtiene un valor de un parametro de configuración del fichero
     * de configuracion de entorno.
     * 
     * @param string $parametro
     * @return string El valor del parametro, si no existe el valor o el fichero, devuelve null
     */
    public static function getConfig($parametro): string {
        $path = dirname(__FILE__) . str_replace("\\", DIRECTORY_SEPARATOR, "\..\..\.env");
        if (!is_readable($path)) {
            return null;
        }

        $lines = file($path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        foreach ($lines as $line) {

            if (strpos(trim($line), '#') === 0) {
                continue;
            }

            list($name, $value) = explode('=', $line, 2);
            $name = trim($name);
            $value = trim($value);

            if ($name == $parametro) {
                return $value;
            }
        }

        return null;
    }

    /**
     * Devuelve la fecha actual en formato largo en español.
     * 
     * @return string
     */
    public static function fecha_actual(): string {
        $week_days = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado");
        $months = array("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $year_now = date("Y");
        $month_now = date("n");
        $day_now = date("j");
        $week_day_now = date("w");
        $date = $week_days[$week_day_now] . ", " . $day_now . " de " . $months[$month_now] . " de " . $year_now;
        return $date;
    }

    /**
     * Devuelve una cadena de longitud variable con unos valores aleatorios de carateres.
     * 
     * @param   int length  logitud de la cadena devuelta
     * @return  string     una cadena con logintud length con caracteres aletatorios.
     */
    function RandomString($length): string {
        $char = "01234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM-$#@%";
        $len = strlen($char);

        $random = "";
        for ($i = 0; $i < $length; $i++) {
            $random .= $char[rand(0, $len - 1)];
        }

        return $random;
    }

     /**
     * Convierte una cadena a un numero hexadecimal
     * 
     * @param   String $string  cadena que se pretende convertir.
     * @return  string     Numero hexadecimal con la represtenación de la cadena.
     */
    public static function strToHex($string) {
        $hex = '';
        for ($i = 0; $i < strlen($string); $i++) {
            $ord = ord($string[$i]);
            $hexCode = dechex($ord);
            $hex .= substr('0' . $hexCode, -2);
        }
        return strToUpper($hex);
    }

    /**
     * Convierte un numero hexadecimal a cadena.
     * 
     * @param   String $hex  Numero hexadecimal
     * @return  string     Cadena representada en el número hexadecimal.
     * 
     */
    public static function hexToStr($hex) {
        $string = '';
        for ($i = 0; $i < strlen($hex) - 1; $i += 2) {
            $string .= chr(hexdec($hex[$i] . $hex[$i + 1]));
        }
        return $string;
    }

}

?>
