<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Entity\Usuarios;
use App\Entity\Grupos;
use App\Entity\VwSistemas;
use App\Entity\VwEjecuciones;
use App\Entity\VwSimulacionesSistemas;
use App\Services\Funciones;
use App\Entity\AppListados;
use App\Entity\VwAppCategorias;
use App\Entity\Menu;
use App\Entity\CabeceraListado;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\DataTableFactory;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Doctrine\ORM\QueryBuilder;
use Parsedown;

/**
 * Clase controladora que gestiona las opciones de navegación de la aplicación en general.
 *
 * @author Antonio Gómez <i52goloa@uco.es>
 */
class AplicacionController extends SisToFaController {

    /**
     * Acción que muestra la pantalla de inicio de sesión al sistema, en la que se
     * piden las credenciales de acceso.
     * 
     * @return Response Respuesta al navegador que hizo la petición.
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request, AuthenticationUtils $authenticationUtils): Response {
        $user = $this->getUser();

        $error = "";

        if ($user instanceof Usuarios) {

            return $this->redirectToRoute('inicio');
        }

        $error = $authenticationUtils->getLastAuthenticationError();

        return $this->render('security/login.html.twig', array(
                    'error' => $error ? $error->getMessage() : NULL,
                    'contenidoHeaderPersonalizado' => false
        ));
    }

    /**
     * Acceso a la pantalla de inicio de la aplicación, donde se muestran acceso directos
     * a las opciones más comunes de la aplicación.
     * 
     * @return Response Respuesta que se envia al navegador del usuario.
     * @Route("/", name="home")
     * @Route("/inicio", name="inicio")
     */
    public function inicioAction(): Response {
        $apartado["imagenS"] = null;
        $apartado["texto"] = null;

        $user = $this->getUser();

        return $this->render('aplicacion/inicio.html.twig', [
                    'contenidoHeaderPersonalizado' => true,
                    'apartado' => $apartado,
                    'error' => ""
        ]);
    }

    /**
     * Opción que muestra la ayuda de la aplicación. Esta ayuda esta en un fichero md y es
     * formateada a HTML para su presentación en el navegador del usuario.
     * 
     * @return Response Respuesta que se envia al navegador del usuario.
     * @Route("/ayuda/{contenido}", name="ayuda_seccion", defaults={"contenido" = "inicio"})   
     * @Route("/ayuda/inicio", name="ayuda")   
     */
    public function ayudaAction(Request $request, $contenido = ""): Response {

        $apartado["imagenS"] = null;
        $apartado["texto"] = null;

        $user = $this->getUser();
        $parse = new Parsedown();

        $text = "";

        if ($contenido == "inicio" || $contenido == null) {
            $text = file_get_contents("../INSTALL/ayuda_inicio.md");
        } else {
            if ($contenido == "simulaciones") {
                $text = file_get_contents("../INSTALL/ayuda_simulaciones.md");
            } else {
                if ($contenido == "sistemas") {
                    $text = file_get_contents("../INSTALL/ayuda_sistemas.md");
                }
                if ($contenido == "administracion") {
                    $text = file_get_contents("../INSTALL/ayuda_administracion.md");
                } else {
                    if ($contenido == "general") {
                        $text = file_get_contents("../INSTALL/ayuda_general.md");
                    } else {
                        if ($contenido == "acceso") {
                            $text = file_get_contents("../INSTALL/ayuda_acceso.md");
                        }
                        else{
                            if ($contenido == "install") {
                            $text = file_get_contents("../INSTALL/ayuda_install.md");
                            }
                        }
                    }
                }
            }
        }

        return $this->render('aplicacion/ayuda.html.twig', [
                    'contenidoHeaderPersonalizado' => true,
                    'apartado' => $apartado,
                    'texto' => $parse->text($text)
        ]);
    }

    /**
     * Muestra la información general, de copyright y licencia de la aplicación.
     * 
     * @return Response Respuesta que se envia al navegador del usuario.
     * @Route("/acercade", name="acercade")
     */
    public function acercadeAction(): Response {
        $apartado["imagenS"] = null;
        $apartado["texto"] = null;

        return $this->render('aplicacion/acercade.html.twig', [
                    'apartado' => $apartado,
                    'contenidoHeaderPersonalizado' => false,
                    'error' => ""
        ]);
    }

    /**
     * Desconecta y da por finalizada la sesión del usuario actualmente conectado. Ya no peermite 
     * el acceso a las opciones hasta que no se realice una nueva identificación.
     * 
     * @return Response Description
     * @Route("/logout", name="logout")
     */
    public function logoutAction(): Response {
        $apartado["imagenS"] = null;
        $apartado["texto"] = null;

        return $this->render('security/login.html.twig', [
                    'controller_name' => 'AplicacionController',
                    'apartado' => $apartado,
                    'error' => ""
        ]);
    }

    /**
     * Muestra el menu de la aplicación según los roles del usuario, basados en el nivel
     * y el grupo al que pertenece el usuario.
     * 
     * @return Response Respuesta que se envia al navegador del usuario que hace 
     * la peticion.
     * 
     */
    public function mostrarMenuAction() {
        $user = $this->getUser();

        // Listado de todos los nombres de ruta disponibles
        $router = $this->container->get('router');
        $listadoRutas = $router->getRouteCollection()->all();

        $omenu = new Menu($this->getDoctrine()->getManager());

        return $this->render('aplicacion/menu.html.twig', array(
                    'menu' => $omenu->getMenu($user->getNivel(), $user->getGruposIdgrupo(), $listadoRutas)
        ));
    }

    /**
     * Genera la cabecera con la información de Entorno, usuario conectado, y la fecha actual. 
     * 
     * @return Response Respuesta que se envia al navegador del usuario que hace 
     * la peticion.
     */
    public function cabeceraInformacionAction() {

        // Muestra el nombre del usuario identificado
        $user = $this->getUser();
        $nombreusuario = $user->getNombre() . " " . $user->getApellidos();

        $fecha = Funciones::fecha_actual();

        return $this->render('aplicacion/cabecera_informacion.html.twig', array(
                    'usuario' => $nombreusuario,
                    'fecha' => $fecha
        ));
    }

    /**
     * Genera el listado de usuarios y lo presenta al usuario conectado si este 
     * tuviera permisos.
     * 
     * @return Response Respuesta http que se envia al navegador del usuario.
     * @Route("/listado/usuarios", name="listado_usuarios", requirements={"slug"="usuarios"}, defaults={"slug" = "usuarios"})
     */
    public function showListadoUsuariosAction(Request $request, DataTableFactory $dataTableFactory) {

        $repository = $this->getDoctrine()->getRepository(AppListados::class);
        $listado = $repository->findBySlug("usuarios");

        if (empty($listado)) {
            return $this->redirectToRoute('inicio', array(
                        'error' => "No se ha encontrado el listado"
            ));
        }

        if(!$this->tieneAcceso($listado)) {
            return $this->volverInicio("No se tiene el nivel requerido para acceder a esta opción");
        }
        
        $cabecera = new CabeceraListado("Listado de Usuarios.", $listado->getImagens());
        $cabecera->setPaginaedicion($listado->getPaginaedicions());
        $cabecera->setCrear(true);

        $table = $dataTableFactory->create()
                ->add('apellidos', TextColumn::class, [
                    "label" => "Apellidos",
                    "searchable" => true
                ])
                ->add('nombre', TextColumn::class, [
                    "label" => "Nombre",
                    "searchable" => true
                ])
                ->add('dni', TextColumn::class, [
                    "label" => "DNI",
                    "searchable" => true
                ])
                ->add('login', TextColumn::class, [
                    "label" => "Identificador de acceso",
                    "searchable" => true
                ])
                ->add('gruposIdgrupo', TextColumn::class, [
                    "label" => "Grupo",
                    "render" => function($value, $context) {

                        $grupo = $this->getDoctrine()
                                ->getRepository(Grupos::class)
                                ->find($value);

                        return $grupo->getNombre();
                    },
                    "searchable" => true
                ])
                ->add('nivel', TextColumn::class, [
                    "label" => "Nivel",
                    "render" => function($value, $context) {

                        switch ($value) {
                            case 1:
                                return "Administrador de Grupo";
                                break;
                            case 10:
                                return "Administrador";
                                break;
                            default :
                                return "Usuario";
                        }
                    },
                    "searchable" => true
                ])
                ->add('estado', TextColumn::class, [
                    "label" => "Estado",
                    "render" => function($value, $context) {

                        switch ($value) {
                            case 1:
                                return "Activado";
                                break;
                            case 2:
                                return "BLOQUEADO";
                                break;
                            default :
                                return "Desconocido";
                        }
                    },
                    "searchable" => true
                ])
                ->add('id', TextColumn::class, [
            'render' => function ($value, $context) {
                return sprintf('<span style="white-space: nowrap;"><center><a href="/SiSToFa/administracion/usuarios/editar/%u" class="boton edit">&nbsp;</a>' .
                        '&nbsp;<a href="/SiSToFa/administracion/usuarios/clonar/%u" class="boton clone">&nbsp;</a>' .
                        '&nbsp;<a href="/SiSToFa/administracion/usuarios/desactivar/%u" class="boton lock">&nbsp;</a></center></span>'
                        , $value, $value, $value);
            },
            'label' => 'Acciones'
        ]);

        if ($this->getUser()->getNivel() == 1) {

            $grupo = $this->getUser()->getGrupo()->getIdgrupo();
            $table->createAdapter(ORMAdapter::class, [
                'entity' => Usuarios::class,
                'query' => function (QueryBuilder $builder) use($grupo) {
                    $builder
                            ->select('ss')
                            ->from(Usuarios::class, 'ss')
                            ->where("ss.gruposIdgrupo = :id")
                            ->setParameter("id", $grupo);
                }]);
        } else {
            if ($this->getUser()->getNivel() == 10) {
                $table->createAdapter(ORMAdapter::class, [
                    'entity' => Usuarios::class
                ]);
            }
        }

        $table->handleRequest($request);

        if ($table->isCallback()) {
            return $table->getResponse();
        }

        return $this->render('aplicacion/listado.html.twig', array(
                    'datatable' => $table,
                    'cabecera' => $cabecera
        ));
    }

    /**
     * Genera el listado de simulaciones que son visibles para el usuario y lo presenta al usuario conectado si este 
     * tuviera permisos.
     * 
     * @return Response Respuesta http que se envia al navegador del usuario.
     * @Route("/listado/simulaciones", name="listado_simulaciones", requirements={"slug"="simulaciones"}, defaults={"slug" = "simulaciones"})
     */
    public function showListadoSimulacionesAction(Request $request, $slug, DataTableFactory $dataTableFactory) {
        $repository = $this->getDoctrine()->getRepository(AppListados::class);
        // Obtiene el listado
        $listado = $repository->findBySlug($slug);

        if (empty($listado)) {
            return $this->redirectToRoute('inicio', array(
                        'error' => "No se ha encontrado el listado"
            ));
        }

        $cabecera = new CabeceraListado($listado->getTexto(), $listado->getImagens());
        $cabecera->setPaginaedicion($listado->getPaginaedicions());
        $cabecera->setCrear(true);

        $table = $dataTableFactory->create()
                ->add('nombre', TextColumn::class, [
                    "label" => "Simulación",
                    "searchable" => true
                ])
                ->add('nombresistema', TextColumn::class, [
                    "label" => "Sistema",
                    "searchable" => true
                ])
                ->add('descripcion', TextColumn::class, [
                    "label" => "Descripción",
                    "searchable" => true
                ])
                ->add('propietario', TextColumn::class, [
                    "label" => "Propietario",
                    "searchable" => true
                ])
                ->add('visibilidadtexto', TextColumn::class, [
                    "label" => "Visibilidad",
                    "searchable" => true
                ])
                ->add('grupo', TextColumn::class, [
                    "label" => "Grupo",
                    "searchable" => true
                ])
                ->add('idsimulacion', TextColumn::class, [
                    'render' => function ($value, $context) {
                        return sprintf('<center><span style="white-space: nowrap;"><a href="/SiSToFa/simulaciones/editar/%u" title="Edita esta simulación." class="boton edit">&nbsp;</a>&nbsp;' .
                                '<a href="/SiSToFa/simulaciones/simular/%u" title="Ejecuta esta simulación." class="boton play">&nbsp;</a>' .
                                '</span></center>', $value, $value);
                    },
                    'label' => 'Acciones'
                ]);
                    
                if($this->getNivel() == 10) {
                    $table->createAdapter(ORMAdapter::class, [
                        'entity' => VwSimulacionesSistemas::class
                    ]);
                }
                else {    
                    $grupo = $this->getGrupo();
                    
                    $table->createAdapter(ORMAdapter::class, [
                    'entity' => VwSimulacionesSistemas::class,
                    'query' => function (QueryBuilder $builder) use($grupo) {
                        $builder
                        ->select('ss')
                        ->from(VwSimulacionesSistemas::class, 'ss')
                        ->where("ss.visibilidad = 352")
                        ->orWhere("ss.grupo = :idgrupo and  ss.visibilidad = 351")
                        ->setParameter("idgrupo", $grupo);
                    }]);
                }
                
                $table->handleRequest($request);

        if ($table->isCallback()) {
            return $table->getResponse();
        }

        return $this->render('aplicacion/listado.html.twig', array(
                    'datatable' => $table,
                    'cabecera' => $cabecera
        ));
    }

    /**
     * Genera el listado de las simulaciones que son propiedad del usuario.
     * 
     * @return Response Respuesta http que se envia al navegador del usuario.
     * @Route("/listado/missimulaciones", name="listado_missimulaciones", requirements={"slug"="missimulaciones"}, defaults={"slug" = "missimulaciones"})
     */
    public function showListadoMisSimulacionesAction(Request $request, DataTableFactory $dataTableFactory, $slug) {
        $repository = $this->getDoctrine()->getRepository(AppListados::class);
        // Obtiene el listado
        $listado = $repository->findBySlug($slug);

        if (empty($listado)) {
            return $this->redirectToRoute('inicio', array(
                        'error' => "No se ha encontrado el listado"
            ));
        }

        $cabecera = new CabeceraListado($listado->getTexto(), $listado->getImagens());
        $cabecera->setPaginaedicion($listado->getPaginaedicions());
        $cabecera->setCrear(true);

        $usuario = $this->getId();

        $table = $dataTableFactory->create()
                ->add('nombre', TextColumn::class, [
                    "label" => "Simulación",
                    "searchable" => true
                ])
                ->add('nombresistema', TextColumn::class, [
                    "label" => "Sistema",
                    "searchable" => true
                ])
                ->add('descripcion', TextColumn::class, [
                    "label" => "Descripción",
                    "searchable" => true
                ])
                ->add('propietario', TextColumn::class, [
                    "label" => "Propietario",
                    "searchable" => true
                ])
                ->add('visibilidadtexto', TextColumn::class, [
                    "label" => "Visibilidad",
                    "searchable" => true
                ])
                ->add('grupo', TextColumn::class, [
                    "label" => "Grupo",
                    "searchable" => true
                ])
                ->add('idsimulacion', TextColumn::class, [
                    'render' => function ($value, $context) {
                        return sprintf('<center><span style="white-space: nowrap;"><a href="/SiSToFa/simulaciones/editar/%u" title="Edita esta simulación." class="boton edit">&nbsp;</a>&nbsp;' .
                                '<a href="/SiSToFa/simulaciones/simular/%u" title="Ejecuta esta simulación." class="boton play">&nbsp;</a>' .
                                '</span></center>', $value, $value);
                    },
                    'label' => 'Acciones'
                ])
                ->createAdapter(ORMAdapter::class, [
                    'entity' => VwSimulacionesSistemas::class,
                    'query' => function (QueryBuilder $builder) use($usuario) {
                        $builder
                        ->select('ss')
                        ->from(VwSimulacionesSistemas::class, 'ss')
                        ->where("ss.idpropietario = :id")
                        ->setParameter("id", $usuario);
                    }])
                ->handleRequest($request);


        if ($table->isCallback()) {
            return $table->getResponse();
        }

        return $this->render('aplicacion/listado.html.twig', array(
                    'datatable' => $table,
                    'cabecera' => $cabecera
        ));
    }

    /**
     * Genera el listado de las ejecuciones lanzadas por el propio usuario.
     * 
     * @return Response Respuesta http que se envia al navegador del usuario.
     * @Route("/listado/misejecuciones", name="listado_misejecuciones", requirements={"slug"="simulaciones"}, defaults={"slug" = "simulaciones"})
     */
    public function showListadoMisEjecucionesAction(Request $request, DataTableFactory $dataTableFactory) {
        $repository = $this->getDoctrine()->getRepository(AppListados::class);
        // Obtiene el listado
        $listado = $repository->findBySlug("simulaciones");

        if (empty($listado)) {
            return $this->redirectToRoute('inicio', array(
                        'error' => "No se ha encontrado el listado"
            ));
        }

        $usuario = $this->getId();

        $cabecera = new CabeceraListado("Listado de ejecuciones.", $listado->getImagens());
        $cabecera->setPaginaedicion($listado->getPaginaedicions());
        $cabecera->setCrear(true);

        $table = $dataTableFactory->create()
                ->add('nombresimulacion', TextColumn::class, [
                    "label" => "Simulacion",
                    "searchable" => true
                ])
                ->add('nombresistema', TextColumn::class, [
                    "label" => "Sistema",
                    "searchable" => true
                ])
                ->add('fechaformateada', TextColumn::class, [
                    "label" => "Fecha de Inicio",
                    "searchable" => true
                ])
                ->add('paso', TextColumn::class, [
                    "label" => "Pasos",
                    "searchable" => false
                ]);
//                $table->add('descripcion', TextColumn::class, [
//                    "label" => "Descripción",
//                    "searchable" => true
//                ]);
//                ->add('usuarionombre', TextColumn::class, [
//                    "label" => "Usuario",
//                    "searchable" => true
//                ]);
                $table->add('idejecucion', TextColumn::class, [
                    'render' => function ($value, $context) {
                        return sprintf('<span style="white-space: nowrap;"><center><a href="/SiSToFa/simulaciones/continuar/%u" title="Continua esta ejecución." class="boton play">&nbsp;</a>&nbsp;' .
                                '<a href="/SiSToFa/simulaciones/veresultados/%u" title="Muestra los resultados de esta ejecución." class="boton verresultados">&nbsp;</a>&nbsp;' .
                                '<a href="/SiSToFa/simulaciones/veresultados/%u" class="boton compare" title="Compara los resultados con otras ejecuciones.">&nbsp;</a>&nbsp;' .
                                '<a href="javascript:eliminaResultados(\'%u\');" class="boton delete" title="Elimina los resultados de esta ejecución.">&nbsp;</a> ' . '</center></span>', $value, $value, $value, $value);
                    },
                    'label' => 'Acciones'
                ])
                ->createAdapter(ORMAdapter::class, [
                    'entity' => VwEjecuciones::class
                ])
                ->createAdapter(ORMAdapter::class, [
                    'entity' => VwEjecuciones::class,
                    'query' => function (QueryBuilder $builder) use($usuario) {
                        $builder
                        ->select('ss')
                        ->from(VwEjecuciones::class, 'ss')
                        ->where("ss.idusuario = :id")
                        ->setParameter("id", $usuario);
                    }])
                ->handleRequest($request);

        if ($table->isCallback()) {
            return $table->getResponse();
        }

        return $this->render('aplicacion/listado.html.twig', array(
                    'datatable' => $table,
                    'cabecera' => $cabecera
        ));
    }

    /**
     * Genera el listado de los grupos definidos en el sistema y lo muestra si el usuario tiene
     * el rol adecuado.
     * 
     * @return Response Respuesta http que se envia al navegador del usuario.
     * @Route("/listado/grupos", name="listado_grupos", requirements={"slug"="grupos"}, defaults={"slug" = "grupos"})
     */
    public function showListadoGruposAction(Request $request, DataTableFactory $dataTableFactory) {

        $repository = $this->getDoctrine()->getRepository(AppListados::class);
        // Obtiene el listado
        $listado = $repository->findBySlug("grupos");

        if (empty($listado)) {
            return $this->redirectToRoute('inicio', array(
                        'error' => "No se ha encontrado el listado"
            ));
        }
        
         if(!$this->tieneAcceso($listado)) {
            return $this->volverInicio("No se tiene el nivel requerido para acceder a esta opción");
        }
        
        $cabecera = new CabeceraListado("Listado de grupos", $listado->getImagens());
        $cabecera->setPaginaedicion($listado->getPaginaedicions());
        $cabecera->setCrear(true);

        $table = $dataTableFactory->create()
                ->add('nombre', TextColumn::class, [
                    "label" => "Nombre",
                    "searchable" => true
                ])
                ->add('idgrupo', TextColumn::class, [
            'render' => function ($value, $context) {
                return sprintf('<center><span style="white-space: nowrap;"><a href="/SiSToFa/aplicacion/grupos/editar/%u" title="Edita el grupo." class="boton edit">&nbsp;</a>&nbsp;' .
                        '</span><span style="white-space: nowrap;"><a href="/SiSToFa/aplicacion/grupos/clonar/%u" title="Edita el grupo." class="boton clone">&nbsp;</a></center>', $value, $value);
            },
            'label' => 'Acciones'
        ]);

        if ($this->getUser()->getNivel() == 1) {

            $grupo = $this->getUser()->getGrupo()->getIdgrupo();
            $table->createAdapter(ORMAdapter::class, [
                'entity' => Grupos::class,
                'query' => function (QueryBuilder $builder) use($grupo) {
                    $builder
                            ->select('ss')
                            ->from(Grupos::class, 'ss')
                            ->where("ss.idgrupo = :id")
                            ->setParameter("id", $grupo);
                }]);
        } else {
            if ($this->getUser()->getNivel() == 10) {
                $table->createAdapter(ORMAdapter::class, [
                    'entity' => Grupos::class
                ]);
            }
        }

        $table->handleRequest($request);

        if ($table->isCallback()) {
            return $table->getResponse();
        }

        return $this->render('aplicacion/listado.html.twig', array(
                    'datatable' => $table,
                    'cabecera' => $cabecera
        ));
    }

    /**
     * Genera el listado de sistemas visibles por el usuario y lo muestra si el usuario tiene 
     * el rol adecuado.
     * 
     * @return Response Respuesta http que se envia al navegador del usuario.
     * @Route("/listado/sistemas", name="listado_sistemas", requirements={"slug"="sistemas"}, defaults={"slug" = "sistemas"})
     */
    public function showListadoSistemasAction(Request $request, DataTableFactory $dataTableFactory, $slug) {
        $repository = $this->getDoctrine()->getRepository(AppListados::class);
        // Obtiene el listado
        $listado = $repository->findBySlug($slug);
        if (empty($listado)) {
            return $this->redirectToRoute('inicio', array(
                        'error' => "No se ha encontrado el listado"
            ));
        }

        $cabecera = new CabeceraListado($listado->getTexto(), $listado->getImagens());
        $cabecera->setPaginaedicion($listado->getPaginaedicions());
        $cabecera->setCrear(true);

        $usuario = $this->getId();
        $grupo = $this->getUser()->getGrupo()->getIdgrupo();

        $table = $dataTableFactory->create()
                ->add('nombre', TextColumn::class, [
                    "label" => "Nombre",
                    "searchable" => true
                ])
                ->add('descripcion', TextColumn::class, [
                    "label" => "Descripción",
                    "searchable" => true
                ])
                ->add('propietario', TextColumn::class, [
                    "label" => "Propietario",
                    "searchable" => true
                ])
                ->add('visibilidadtexto', TextColumn::class, [
                    "label" => "Visibilidad",
                    "searchable" => true
                ])
                ->add('idsistema', TextColumn::class, [
                    'render' => function ($value, $context) {
                        return sprintf('<span style="white-space: nowrap;"><center><a href="/SiSToFa/sistemas/editar/%u" title="Realiza modificaciones al sistema." class="boton edit">&nbsp;</a>' .
                                '&nbsp;<a href="/SiSToFa/sistemas/visualizar/%u" title="Ver el diseño del sistema." class="boton ver">&nbsp;</a>' .
                                '&nbsp;<a href="/SiSToFa/sistemas/clonar/%u" class="boton clone">&nbsp;</a>' .
                                '</center></span>', $value, $value, $value);
                    },
                    'label' => 'Acciones'
                ]);
                    
                if($this->getNivel() == 10) {
                    $table->createAdapter(ORMAdapter::class, [
                        'entity' => VwSistemas::class
                    ]);
                }
                else {   
                    $table->createAdapter(ORMAdapter::class, [
                    'entity' => VwSistemas::class,
                    'query' => function (QueryBuilder $builder) use($grupo,$usuario) {
                        $builder
                        ->select('ss')
                        ->from(VwSistemas::class, 'ss')
                        ->where("((ss.visibilidad = 352) or (ss.grupo = :idgrupo and  ss.visibilidad = 351)) and ss.idpropietario != :id")
                        ->setParameter("idgrupo", $grupo)
                        ->setParameter("id", $usuario);
                    }]);
                }
                
                $table->handleRequest($request);

        if ($table->isCallback()) {
            return $table->getResponse();
        }

        return $this->render('aplicacion/listado.html.twig', array(
                    'datatable' => $table,
                    'cabecera' => $cabecera
        ));
    }

    /**
     * Genera el listado de los sistemas que son propiedad del propio usuario conectado.
     * 
     * @return Response Respuesta http que se envia al navegador del usuario.
     * @Route("/listado/missistemas", name="listado_missistemas", requirements={"slug"="missistemas"}, defaults={"slug" = "missistemas"})
     */
    public function showListadoMisSistemasAction(Request $request, DataTableFactory $dataTableFactory, $slug) {
        $repository = $this->getDoctrine()->getRepository(AppListados::class);
        // Obtiene el listado
        $listado = $repository->findBySlug($slug);
        if (empty($listado)) {
            return $this->redirectToRoute('inicio', array(
                        'error' => "No se ha encontrado el listado"
            ));
        }

        $cabecera = new CabeceraListado($listado->getTexto(), $listado->getImagens());
        $cabecera->setPaginaedicion($listado->getPaginaedicions());
        $cabecera->setCrear(true);

        $usuario = $this->getId();

        $table = $dataTableFactory->create()
                ->add('nombre', TextColumn::class, [
                    "label" => "Nombre",
                    "searchable" => true
                ])
                ->add('descripcion', TextColumn::class, [
                    "label" => "Descripción",
                    "searchable" => true
                ])
                ->add('propietario', TextColumn::class, [
                    "label" => "Propietario",
                    "searchable" => true
                ])
                ->add('visibilidadtexto', TextColumn::class, [
                    "label" => "Visibilidad",
                    "searchable" => true
                ])
                ->add('idsistema', TextColumn::class, [
                    'render' => function ($value, $context) {
                        return sprintf('<span style="white-space: nowrap;"><center><a href="/SiSToFa/sistemas/editar/%u" class="boton edit">&nbsp;</a>' .
                                '&nbsp;<a href="/SiSToFa/sistemas/visualizar/%u" class="boton ver">&nbsp;</a>' .
                                '&nbsp;<a href="/SiSToFa/sistemas/clonar/%u" class="boton clone">&nbsp;</a>' .
                                '</center></span>', $value, $value, $value);
                    },
                    'label' => 'Acciones'
                ])
                ->createAdapter(ORMAdapter::class, [
                    'entity' => VwSistemas::class,
                    'query' => function (QueryBuilder $builder) use($usuario) {
                        $builder
                        ->select('ss')
                        ->from(VwSistemas::class, 'ss')
                        ->where("ss.idpropietario = :id")
                        ->setParameter("id", $usuario);
                    }])
                ->handleRequest($request);

        if ($table->isCallback()) {
            return $table->getResponse();
        }

        return $this->render('aplicacion/listado.html.twig', array(
                    'datatable' => $table,
                    'cabecera' => $cabecera
        ));
    }

    /**
     * Genera el listado de las listados del sistema y lo muestra si el usuario es un administrador.
     * 
     * @return Response Respuesta http que se envia al navegador del usuario.
     * @Route("/listado/listados", name="listado_listados", requirements={"slug"="listados"}, defaults={"slug" = "listados"})
     */
    public function showListadoListadosAction(Request $request, DataTableFactory $dataTableFactory) {
        $repository = $this->getDoctrine()->getRepository(AppListados::class);
        // Obtiene el listado
        $listado = $repository->findBySlug("listados");
        if (empty($listado)) {
            return $this->redirectToRoute('inicio', array(
                        'error' => "No se ha encontrado el listado"
            ));
        }
        
        if(!$this->tieneAcceso($listado)) {
            return $this->volverInicio("No se tiene el nivel requerido para acceder a esta opción");
        }

        $user = $this->getUser();

        $cabecera = new CabeceraListado("Listado de listados.", $listado->getImagens());
        $cabecera->setPaginaedicion($listado->getPaginaedicions());
        $cabecera->setCrear(true);

        $table = $dataTableFactory->create()
                ->add('nombre', TextColumn::class, [
                    "label" => "Nombre",
                    "searchable" => true
                ])
                ->add('texto', TextColumn::class, [
                    "label" => "Texto",
                    "searchable" => true
                ])
                ->add('slug', TextColumn::class, [
                    "label" => "Slug",
                    "searchable" => true
                ])
                ->add('idlistado', TextColumn::class, [
                    'render' => function ($value, $context) {
                        return sprintf('<center><a href="/SiSToFa/aplicacion/listados/editar/%u" class="boton edit">&nbsp;</a>' .
                                '&nbsp;<a href="/SiSToFa/aplicacion/listados/clonar/%u" class="boton clone">&nbsp;</a>' .
                                '&nbsp;<a href="/SiSToFa/aplicacion/listados/desactivar/%u" class="boton lock">&nbsp;</a></center>'
                                , $value, $value, $value);
                    },
                    'label' => 'Acciones'
                ])
                ->createAdapter(ORMAdapter::class, [
                    'entity' => AppListados::class
                ])
                ->handleRequest($request);

        if ($table->isCallback()) {
            return $table->getResponse();
        }

        return $this->render('aplicacion/listado.html.twig', array(
                    'datatable' => $table,
                    'cabecera' => $cabecera
        ));
    }

    /**
     * Genera el listado de las categorias y tipos definidos en la aplicación y lo muestra si el usuario es un administrador.
     * 
     * @return Response Respuesta http que se envia al navegador del usuario.
     * @Route("/listado/categoriasgenericas", name="listado_categoriasgenericas", requirements={"slug"="categoriasgenericas"}, defaults={"slug" = "categoriasgenericas"})
     */
    public function showListadoCategoriasAction(Request $request, DataTableFactory $dataTableFactory) {
        $repository = $this->getDoctrine()->getRepository(AppListados::class);
        // Obtiene el listado
        $listado = $repository->findBySlug("categoriasgenericas");
        if (empty($listado)) {
            return $this->redirectToRoute('inicio', array(
                        'error' => "No se ha encontrado el listado"
            ));
        }
    
        if(!$this->tieneAcceso($listado)) {
            return $this->volverInicio("No se tiene el nivel requerido para acceder a esta opción");
        }

        $user = $this->getUser();

        $cabecera = new CabeceraListado("Listado de categorias genéricas.", $listado->getImagens());
        $cabecera->setPaginaedicion($listado->getPaginaedicions());
        $cabecera->setCrear(true);

        $table = $dataTableFactory->create()
                ->add('nombre', TextColumn::class, [
                    "label" => "Nombre",
                    "searchable" => true
                ])
                ->add('nombrecategoriasuperior', TextColumn::class, [
                    "label" => "Categoria Superior",
                    "searchable" => true
                ])
                ->add('idcategoria', TextColumn::class, [
                    'render' => function ($value, $context) {
                        return sprintf('<center><a href="/SiSToFa/aplicacion/categoriasgenericas/editar/%u" class="boton edit">&nbsp;</a>' .
                                '&nbsp;<a href="/SiSToFa/aplicacion/categoriasgenericas/clonar/%u" class="boton clone">&nbsp;</a>' .
                                '&nbsp;<a href="/SiSToFa/aplicacion/categoriasgenericas/desactivar/%u" class="boton lock">&nbsp;</a></center>'
                                , $value, $value, $value);
                    },
                    'label' => 'Acciones'
                ])
                ->createAdapter(ORMAdapter::class, [
                    'entity' => VwAppCategorias::class
                ])
                ->handleRequest($request);

        if ($table->isCallback()) {
            return $table->getResponse();
        }

        return $this->render('aplicacion/listado.html.twig', array(
                    'datatable' => $table,
                    'cabecera' => $cabecera
        ));
    }

}
