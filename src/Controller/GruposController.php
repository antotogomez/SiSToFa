<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\Controller\SimSitfaController;
use App\Entity\AppListados;
use App\Entity\Grupos;
use App\Entity\Usuarios;
use App\Entity\CabeceraListado;

/**
 * Clase controlador de las acciones de usuario relativas a la gestión de grupos
 * de usuarios.
 *
 * @author Antonio Gómez <i52goloa@uco.es>
 */
class GruposController extends SisToFaController {

    /**
     * Acción para mostrar el formulario de edición de grupos en blanco para añadir
     * los datos de otro usuario.
     *
     * @Route("/aplicacion/{slug}/crear", name="grupos_crear", requirements={"slug"="grupos"}, defaults={"slug" = "grupos"})
     * @Route("/aplicacion/{slug}/editar/{id}", name="grupos_editar", requirements={"slug"="grupos", "id"="\d+"}, defaults={"slug" = "grupos"})
     * 
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador del usuario que se va a editar, nulo si es nuevo.
     * @return Response Respuesta http que se envia al navegador del usuario
     */
    public function editarAction(Request $request, $slug, $id = null): Response {

        $disabled = false;

        // Obtiene la definición de la pantalla
        $listado = $this->getDoctrine()
                ->getRepository(AppListados::class)
                ->findBySlug($slug);

        if (!$this->tieneAcceso($listado)) {
            return $this->volverInicio("No tiene acceso a esta opción.");
        }

        $datos = new Grupos();

        //Si hay identificador se busca el usuario
        if ($id != null) {
            $datos = $this->getDoctrine()
                    ->getRepository(Grupos::class)
                    ->find($id);

            if (empty($datos)) {
                $this->volverInicio("Listado \"" . $slug . "\" no encontrado.");
            }
        }

        // ------------------------------
        // FORMULARIO DE CREACIÓN/EDICIÓN
        // ------------------------------
        $builder = $this->createFormBuilder()->setAction($this->generateUrl($slug . '_guardar',
                        array('id' => $id)
        ));

        // Se añaden los campos del formulario
        $builder->add('nombre', TextType::class, array(
            'required' => true,
            'label' => 'Nombre*',
            'data' => $datos->getNombre(),
            'disabled' => $disabled
        ));

        $builder->add('descripcion', TextareaType::class, array(
            'required' => false,
            'label' => 'Descripcion',
            'data' => $datos->getDescripcion(),
            'disabled' => $disabled
        ));

        $builder->add('estado', ChoiceType::class, array(//esto es para mostrar en la pantalla de la tabla ?
            'required' => true,
            'label' => 'Estado',
            'data' => $datos->getEstado(),
            'choices' => array(
                "Activado" => "1",
                "BLOQUEADO" => "2"
            ),
            'disabled' => $disabled
        ));

        // El botón de guardar solo se mostrará si tiene permisos de escritura
        $builder->add('Guardar', SubmitType::class);
        $builder->add('Clonar', SubmitType::class);
        $builder->add('Eliminar', SubmitType::class);

        // El formulario de creación/edición es enviado
        $form = $builder->getForm();

        // Si el formulario ha sido enviado y se ha producido un error recuerda los datos introducidos
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
        }

        // Define la cabecera del listado.
        $cabecera = new CabeceraListado($datos->getNombre(), $listado->getImagens());
        $cabecera->setPaginaedicion($listado->getPaginaedicions());
        $cabecera->setCrear(true);
        $cabecera->setRutavuelta("listado_" . $slug);

        return $this->render('administracion/grupos_editar.html.twig', array(
                    'formulario' => $form->createView(),
                    'datos' => $datos,
                    'cabecera' => $cabecera,
                    'listado' => $listado,
                    'usuarios' => $this->getDoctrine()
                            ->getRepository(Usuarios::class)
                            ->findByGrupo($id),
                    'tienePermisos' => true
        ));
    }

    /**
     * GUARDAR GRUPOS
     *
     * Guarda los datos del formulario de creación/edición de tipo de servicio.
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador del grupo que se va a guardar.
     * 
     * @Route("/aplicacion/{slug}/guardar/{id}", name="grupos_guardar", requirements={"slug"="grupos", "id"="\d+"}, defaults={"slug" = "grupos"})
     */
    public function guardarAction(Request $request, $slug, $id = null) {

        // Obtiene la definición de la pantalla
        $listado = $this->getDoctrine()
                ->getRepository(AppListados::class)
                ->findBySlug($slug);

        if (!$this->tieneAcceso($listado)) {
            return $this->volverInicio("No tiene acceso a esta opción.");
        }

        if ($request->request->get('form')) { // Se ha enviado el formulario
            $datosform = $request->request->get('form');
            var_dump($datosform);

            $datos = new Grupos();
                
            if ($id) { // Si $id está definido, actualiza el elemento
                $datos = $this->getDoctrine()->getRepository(Grupos::class)->find($id);

                if ($this->getGrupo() != $datos->getIdgrupo() && $this->getNivel() == 1) { // No tiene permisos de escritura
                    return $this->volverInicio('Ha ocurrido un error al guardar los datos:<br>no tiene permisos de escritura.');
                } 
            }

                    $url_vuelta = $slug . '_editar';

                    try {
                        if (isset($datosform["Eliminar"])) {
                            $this->emInstance->remove($datos);
                            $this->emInstance->flush();
                            $this->addFlash('correcto', 'Los datos se han eliminado correctamente.');
                            $url_vuelta = 'listado_' . $slug;
                        } else {

                            if (isset($datosform["Clonar"])) {

                                $datos = new Grupos();

                                $datos->setNombre($datosform["nombre"] . " (COPIA)");
                                $datos->setDescripcion($datosform["descripcion"]);
                                $datos->setEstado($datosform["estado"]);

                                $this->emInstance->persist($datos);
                                $this->emInstance->flush();

                                $this->addFlash('correcto', 'Los datos se han clonado correctamente.');
                            } else {

                                //Guarda los datos
                                $datos->setNombre($datosform["nombre"]);
                                $datos->setDescripcion($datosform["descripcion"]);
                                $datos->setEstado($datosform["estado"]);

                                $this->emInstance->persist($datos);
                                $this->emInstance->flush();
                                $this->addFlash('correcto', 'Los datos se han guardado correctamente.');
                            }
                        }

                        return $this->redirectToRoute($url_vuelta, array(
                                    'id' => $datos->getIdgrupo()));
                    } catch (\Exception $e) {
                        $this->addFlash('error', 'Ha ocurrido un error al guardar los cambios.');
                        return $this->redirectToRoute('listado_' . $slug );
                    }
                
            
            return $this->redirectToRoute('listado_' . $slug);
            
        } else { // No se ha enviado el formulario
            $this->addFlash(
                    'error',
                    'Ha ocurrido un error al enviar el formulario.'
            );
            return $this->redirectToRoute('listado_' . $slug);
        }
    }

}
