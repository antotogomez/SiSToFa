<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\AppListados;
use App\Entity\Simulaciones;
use App\Entity\Sistemas;
use App\Entity\Metricas;
use App\Entity\MetricasValores;
use App\Entity\Eventos;
use App\Entity\Ejecuciones;
use App\Entity\ActualizaEjecucion;
use App\Entity\VwValoresComparados;
use App\Entity\VwEjecuciones;
use App\Entity\Usuarios;
use App\Services\Funciones;
use App\Entity\CabeceraListado;
use App\Entity\AppCategoriasGenericas;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\DateTimeColumn;
use Omines\DataTablesBundle\DataTableFactory;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Controlador que gestiona la visualización, edición y consulta de los resultados
 * de una simulación.
 *
 * @author Antonio Gómez <i52goloa@uco.es>
 * 
 */
class SimulacionController extends SisToFaController {

    /**
     * Muestra el formulario de edición y creación de simulaciones.
     * 
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador del sistema que se va a editar, nulo si es nuevo.
     * @return Response Respuesta http que se envia al navegador del usuario
     * 
     * @Route("/simulaciones/editar/{id}", name="simulaciones_editar",requirements={"slug"="simulaciones", "id"="\d+"}, defaults={"slug" = "simulaciones"})
     * @Route("/simulaciones/crear", name="simulaciones_crear",requirements={"slug"="simulaciones"}, defaults={"slug" = "simulaciones"})
     */
    public function editarAction(Request $request, $slug = "sistemas", $id = null): Response {

        // Obtiene listado
        $listado = $this->getDoctrine()
                ->getRepository(AppListados::class)
                ->findBySlug($slug);

        $disabled = false;
        $tienePermisos = false;

        $datos = new Simulaciones();
        $ejecuciones = array();

        if ($id != "") {
            // Obtiene información del sistema que se va a editar.

            $datos = $this->getDoctrine()
                    ->getRepository(Simulaciones::class)
                    ->find($id);

            $ejecuciones = $this->getDoctrine()
                    ->getRepository(VwEjecuciones::class)
                    ->findEjecucionesSimulacion($id);

            if (is_null($datos)) {
                $this->volverInicio("Listado \"" . $slug . "\" no encontrado.");
            }
        } else {
            //Si es un sistema nuevo, establece el usuario actual como el propietario del sistema
            $datos->setUsuarios($this->getUser());
        }

        //Comprueba el nivel de acceso a este sistema por parte del usuario actualmente conectado
        if ($datos->getUsuarios()->getId() == $this->getId()) {
            //Es el propietario, puede modificar el sistema
            $disabled = false;
        } else {
            $visiblidad_publica = $this->getDoctrine()->getRepository(AppCategoriasGenericas::class)->findCategoriaCodigo("VISIBILIDAD_PUBLICO")->getIdcategoria();
            $visiblidad_grupo = $this->getDoctrine()->getRepository(AppCategoriasGenericas::class)->findCategoriaCodigo("VISIBILIDAD_GRUPO")->getIdcategoria();
            $grupo_propietario = $datos->getUsuarios()->getGruposIdgrupo();

            if ($datos->getVisibilidad() == $visiblidad_publica || ($datos->getVisibilidad() == $visiblidad_grupo && $this->getGrupo()->getId() == $grupo_propietario)) {
                $disabled = true;
            } else {
                return $this->redirectToRoute('listado_sistemas', array(
                            'error' => "No se ha encontrado el listado"
                ));
            }
        }

        $builder = $this->createFormBuilder()->setAction($this->generateUrl($slug . '_guardar',
                        array('id' => $id)
        ));

        // Se añaden los campos del formulario
        $builder->add('nombre', TextType::class, array(
            'required' => true,
            'label' => 'Nombre*',
            'data' => $datos->getNombre(),
            'disabled' => $disabled
        ));

        // Se añaden los campos del formulario
        $builder->add('tiempo', TextType::class, array(
            'required' => false,
            'label' => 'Pasos',
            'data' => $datos->getTiempo(),
            'disabled' => $disabled
        ));

        // Se añaden los campos del formulario
        $builder->add('propietario', TextType::class, array(
            'required' => true,
            'label' => 'Propietario*',
            'data' => $datos->getUsuarios()->getNombre() . " " . $datos->getUsuarios()->getApellidos(),
            'disabled' => true
        ));

        $sistema = null;
        if ($datos->getSistema() != "") {
            $sistema = $datos->getSistema();
        }

        $builder->add('sistema', EntityType::class, array(
            'label' => 'Sistema a simular:',
            'class' => Sistemas::class,
            'choice_label' => 'nombre',
            'mapped' => false,
            'data' => $sistema,
            'disabled' => $disabled
        ));

        // Se añaden los campos del formulario
        $builder->add('idpropietario', HiddenType::class, array(
            'required' => true,
            'data' => $datos->getUsuarios()->getId(),
            'disabled' => true
        ));

        $builder->add('descripcion', TextareaType::class, array(
            'required' => false,
            'label' => 'Descripcion:',
            'data' => $datos->getDescripcion(),
            'disabled' => $disabled
        ));

        $visibilidad = null;
        if ($datos->getVisibilidad() != "") {
            $visibilidad = $this->getDoctrine()->getRepository(AppCategoriasGenericas::class)->find($datos->getVisibilidad());
        }

        $builder->add('visibilidad', EntityType::class, array(
            'label' => 'Visibilidad de la simulacion:',
            'class' => AppCategoriasGenericas::class,
            'query_builder' => function (EntityRepository $er) {
                return $er->findCategoriasTipoQuery('VISIBILIDAD');
            },
            'choice_label' => 'nombre',
            'mapped' => false,
            'data' => $visibilidad,
            'disabled' => $disabled
        ));

        // El botón de guardar solo se mostrará si tiene permisos de escritura
        if (!$disabled){
            $builder->add('Guardar', SubmitType::class);
            $builder->add('Eliminar', SubmitType::class);
        }
        
        $builder->add('Clonar', SubmitType::class);
        
        // El formulario de creación/edición es enviado
        $form = $builder->getForm();

        // Si el formulario ha sido enviado y se ha producido un error recuerda los datos introducidos
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
        }

        $metricasfiabilidad = $this->getDoctrine()->getRepository(AppCategoriasGenericas::class)->findCategoriasTipo("METRICAS_FIABILIDAD");

        // Define la cabecera del listado.
        $cabecera = new CabeceraListado("Simulación:" . $datos->getNombre(), $listado->getImagens());
        $cabecera->setPaginaedicion($listado->getPaginaedicions());
        $cabecera->setCrear(true);
        $cabecera->setRutavuelta("listado_" . $slug);
        $cabecera->setNolistado(true);

        return $this->render('simulacion/simulacion_editar.html.twig', [
                    'formulario' => $form->createView(),
                    'datos' => $datos,
                    'ejecuciones' => $ejecuciones,
                    'cabecera' => $cabecera,
                    'listado' => $listado,
                    'datosHistorial' => $datos->getHistorial(),
                    'metricasfiabilidad' => $metricasfiabilidad,
                    'tienePermisos' => !$disabled,
                    'error' => ""
        ]);
    }

    /**
     * Muestra los resultados de la ejecución de la simulación cuyo identificados se envia
     *
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador del sistema que se va a editar, nulo si es nuevo.
     * 
     * @return Response Respuesta http que se envia al navegador del cliente.
     * 
     * @Route("/simulaciones/veresultados/{id}/{idcomparacion}", name="ver_resultados",requirements={"slug"="simulaciones", "id"="\d+","idcomparacion"="\d+"}, defaults={"slug" = "simulaciones"})
     */
    public function verResultadosAction(Request $request, $slug = "simulaciones", $id = null, $idcomparacion = null, DataTableFactory $dataTableFactory): Response {

        // Obtiene listado
        $listado = $this->getDoctrine()
                ->getRepository(AppListados::class)
                ->findBySlug($slug);

        $disabled = false;

        //Obtiene los datos de la ejecución.
        $datos = new VwEjecuciones();
        if ($id != "") {
            // Obtiene información de la ejecución que se va a visualizar

            $datos = $this->getDoctrine()
                    ->getRepository(VwEjecuciones::class)
                    ->find($id);

            if (is_null($datos)) {
                $this->volverInicio("Listado \"" . $slug . "\" no encontrado.");
            }
        } else {
            //Si es un sistema nuevo, establece el usuario actual como el propietario del sistema
            $this->volverInicio("Ejecución no encontrada.");
        }

        $simulacion = $this->getDoctrine()
                ->getRepository(Simulaciones::class)
                ->find($datos->getIdsimulacion());

        $datos->setTiempo($simulacion->getTiempo());
        $valoresmetricas = array();

        $i = 0;
        foreach ($simulacion->getMetricas() as $metrica) {

            $valoresmetricas[$i]["nombre"] = $metrica->getNombre() . " (Ejecución " . $id . ")";
            $valoresmetricas[$i]["color"] = $metrica->getColor();
            $valoresmetricas[$i]["id"] = $metrica->getIdmetrica();
            $valoresmetricas[$i]["valores"] = $this->getDoctrine()
                    ->getRepository(MetricasValores::class)
                    ->findValoresEjecucion($metrica->getIdmetrica(), $id);

            $valoresmetricas[$i]["datatable"] = $dataTableFactory->create()
                    ->setName('datatable' . strval($metrica->getIdmetrica()))
                    ->add('instante', TextColumn::class, [
                        "label" => "Instante",
                        "searchable" => false
                    ])
                    ->add('valor', TextColumn::class, [
                "label" => "Valor (Ej." . $id . ")",
                "searchable" => false
            ]);

            if ($idcomparacion != "") {

                $valoresmetricas[$i]["datatable"]->add('valor2', TextColumn::class, [
                    "label" => "Valor (Ej." . $idcomparacion . ")",
                    "searchable" => false
                ]);

                $valoresmetricas[$i]["datatable"]->createAdapter(ORMAdapter::class, [
                    'hydrate' => \Doctrine\ORM\Query::HYDRATE_ARRAY,
                    'entity' => VwValoresComparados::class,
                    'query' => function (QueryBuilder $builder) use ($id, $metrica, $idcomparacion) {

                        $builder->select('v1.idvalor AS idvalor, v1.valor AS valor, v1.instante, v2.valor AS valor2')
                                ->from(MetricasValores::class, 'v1')
                                ->leftJoin(MetricasValores::class, 'v2', 'WITH', "v1.idmetrica = v2.idmetrica AND v1.instante = v2.instante AND v2.idejecucion = :idejecucion2")
                                ->where("v1.idejecucion = :id")
                                ->andWhere("v1.idmetrica = :metrica")
                                ->setParameter("id", $id)
                                ->setParameter("metrica", $metrica->getIdmetrica())
                                ->setParameter("idejecucion2", $idcomparacion)
                                ->orderBy("v1.instante");
                    }]);
            } else {

                $valoresmetricas[$i]["datatable"]->createAdapter(ORMAdapter::class, [
                    'entity' => MetricasValores::class,
                    'query' => function (QueryBuilder $builder) use ($id, $metrica) {
                        $builder
                                ->select('v')
                                ->from(MetricasValores::class, 'v')
                                ->where("v.idejecucion = :id")
                                ->andWhere("v.idmetrica = :metrica")
                                ->setParameter("id", $id)
                                ->setParameter("metrica", $metrica->getIdmetrica())
                                ->orderBy("v.instante");
                    }]);
            };

            $valoresmetricas[$i]["datatable"]->handleRequest($request);

            if ($valoresmetricas[$i]["datatable"]->isCallback()) {
                return $valoresmetricas[$i]["datatable"]->getResponse();
            }

            $i++;
        }

        //Obtiene los datos de la ejecución comparada
        $datoscomparacion = new VwEjecuciones();
        $valoresmetricascomparacion = array();

        if ($idcomparacion != "") {
            // Obtiene información de la ejecución que se va a visualizar

            $datoscomparacion = $this->getDoctrine()
                    ->getRepository(VwEjecuciones::class)
                    ->find($idcomparacion);

            if (!is_null($datoscomparacion)) {

                $i = 0;

                foreach ($simulacion->getMetricas() as $metrica) {

                    $valoresmetricascomparacion[$i]["nombre"] = $metrica->getNombre() . " (Ejecución " . $idcomparacion . ")";

                    //Establece como color el negativo
                    //$valoresmetricascomparacion[$i]["color"] = $metrica->getColor();

                    $valoresmetricascomparacion[$i]["color"] = "#" . substr(dechex(~hexdec(substr($metrica->getColor(), 1))), -6);

                    $valoresmetricascomparacion[$i]["id"] = $metrica->getIdmetrica();
                    $valoresmetricascomparacion[$i]["valores"] = $this->getDoctrine()
                            ->getRepository(MetricasValores::class)
                            ->findValoresEjecucion($metrica->getIdmetrica(), $idcomparacion);
                    $i++;
                }
            }
        }

        // Define la cabecera del listado.
        $cabecera = new CabeceraListado("Ejecución " . $datos->getIdejecucion() . " de " . $datos->getNombresimulacion(), $listado->getImagens());
        $cabecera->setPaginaedicion($listado->getPaginaedicions());
        $cabecera->setCrear(true);
        $cabecera->setNolistado(true);
        $cabecera->setPaginaedicion("");

        $cabecera->setRutavuelta("listado_misejecuciones");

        if ($idcomparacion != "") {

            $cabecera->setTexto("Ejecución " . $datos->getIdejecucion() . " - Ejecución " . $datoscomparacion->getIdejecucion() . " de " . $datos->getNombresimulacion(), $listado->getImagens());

            return $this->render('simulacion/simulacion_resultados.html.twig', [
                        'datos' => $datos,
                        'datoscomparacion' => $datoscomparacion,
                        'cabecera' => $cabecera,
                        'listado' => $listado,
                        'tienePermisos' => 'escritura',
                        'valores' => $valoresmetricas,
                        'valorescomparacion' => $valoresmetricascomparacion,
                        'error' => "",
                        'datatable' => (count($valoresmetricas) > 0 ? $valoresmetricas[0]["datatable"] : null)
            ]);
        } else {

            return $this->render('simulacion/simulacion_resultados.html.twig', [
                        'datos' => $datos,
                        'cabecera' => $cabecera,
                        'listado' => $listado,
                        'tienePermisos' => 'escritura',
                        'valores' => $valoresmetricas,
                        'error' => "",
                        'datatable' => (count($valoresmetricas) > 0 ? $valoresmetricas[0]["datatable"] : null)
            ]);
        }
    }

    /**
     * Recibe una petición de modificación de los datos de una simulación, incluida 
     * la eliminación y clonación de simulaciones.
     * 
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador del sistema que se va a editar, nulo si es nuevo.
     * 
     * @return Response Respuesta http que se envia al navegador del usuario.
     * 
     * @Route("/simulaciones/guardar/{id}", name="simulaciones_guardar",requirements={"slug"="simulaciones", "id"="\d+"}, defaults={"slug" = "simulaciones"})
     */
    public function guardarAction(Request $request, $slug = "sistemas", $id = null) {

        if ($request->request->get('form')) { // Se ha enviado el formulario
            $datosform = $request->request->get('form');

            $datos = new simulaciones();

            if ($id) { // Si $id está definido, actualiza el elemento
                $datos = $this->getDoctrine()->getRepository(Simulaciones::class)->find($id);
            } else { //Si no, es nuevo y se establece su propietario como el usaurio actual.
                $datos->setUsuarios($this->getDoctrine()->getRepository(Usuarios::class)->find($this->getUser()->getId()));
            }

            $sistema = null;


            try {
                if (isset($datosform["Eliminar"])) {

                    if ($datos->getUsuarios()->getId() != $this->getId()) { // No tiene permisos de escritura
                        $this->addFlash('error', 'Ha ocurrido un error al eliminar los datos:<br>No tiene permisos de escritura sobre el objeto.');
                        return $this->redirectToRoute($slug . '_editar', array('id' => $id));
                    }
                    //Tiene que eliminar las metricas, los eventos y las ejecuciones realizadas
                    $this->emInstance->remove($datos);
                    $this->emInstance->flush();
                    $this->addFlash('correcto', 'Los datos se han eliminado correctamente.');

                    return $this->redirectToRoute('listado_' . $slug);
                    
                } else {

                    if (isset($datosform["Clonar"])) {

                        $nuevos = new Simulaciones();

                        $nuevos->setNombre($datos->getNombre() . " (COPIA)")
                                ->setDescripcion($datos->getDescripcion())
                                ->setTiempo($datos->getTiempo())
                                ->setVisibilidad($datos->getVisibilidad());

                        if ($datos->getSistema()) { // Si $id está definido, actualiza el elemento
                            $sistema = $this->getDoctrine()->getRepository(Sistemas::class)->find($datos->getSistema()->getIdsistema());
                        }

                        $nuevos->setSistema($sistema);
                        $nuevos->setUsuarios($this->getDoctrine()->getRepository(Usuarios::class)->find($this->getUser()->getId()));

                        $nuevos->setEventos($datos->getEventos());
                        $nuevos->setMetricas($datos->getMetricas());

                        $this->emInstance->persist($nuevos);
                        $this->emInstance->flush();

                        foreach ($datos->getEventos() as $evento) {

                            $nuevoevento = new Eventos();
                            $nuevoevento->setComponenteid($evento->getComponenteid());
                            $nuevoevento->setDescripcion($evento->getDescripcion());
                            $nuevoevento->setEntradaid($evento->getEntradaid());
                            $nuevoevento->setEstado($evento->getEstado());
                            $nuevoevento->setInstante($evento->getInstante());
                            $nuevoevento->setNombre($evento->getNombre());
                            $nuevoevento->setPorcentajefallo($evento->getPorcentajefallo());
                            $nuevoevento->setSimulaciones($nuevos);
                            $nuevoevento->setTipofallo($evento->getTipofallo());
                            $nuevoevento->setValor($evento->getValor());

                            $nuevos->getEventos()->add($nuevoevento);
                            $this->emInstance->persist($nuevoevento);
                            $this->emInstance->flush();
                        }

                        foreach ($datos->getMetricas() as $metrica) {

                            $nuevametrica = new Metricas();
                            $nuevametrica->setDescripcion($metrica->getDescripcion());
                            $nuevametrica->setIdcomponente($metrica->getIdcomponente());
                            $nuevametrica->setIdentrada($metrica->getIdentrada());
                            $nuevametrica->setIdfiabilidad($metrica->getIdfiabilidad());
                            $nuevametrica->setColor($metrica->getColor());
                            $nuevametrica->setIdsalida($metrica->getIdsalida());
                            $nuevametrica->setNombre($metrica->getNombre());
                            $nuevametrica->setSimulacion($nuevos);

                            $nuevos->getMetricas()->add($nuevametrica);
                            $this->emInstance->persist($nuevametrica);
                            $this->emInstance->flush();
                        }

                        $this->addFlash('correcto', 'Los datos se han clonado correctamente.');
                        return $this->redirectToRoute($slug . '_editar', array(
                                    'id' => $nuevos->getId()));
                    } else {

                        if ($datosform['sistema']) { // Si $id está definido, actualiza el elemento
                            $sistema = $this->getDoctrine()->getRepository(Sistemas::class)->find($datosform['sistema']);
                        } else { //Si no, es nuevo y se establece su propietario como el usaurio actual.
                        }

                        $datos->setSistema($sistema);
                        
                        if ($datos->getUsuarios()->getId() != $this->getId()) { // No tiene permisos de escritura
                            $this->addFlash('error', 'Ha ocurrido un error al guardar los datos:<br>No tiene permisos de escritura sobre el objeto.');
                            return $this->redirectToRoute($slug . '_editar', array('id' => $id));
                        }

                        if ($datosform['tiempo'] == "" || !is_numeric($datosform['tiempo'])) {
                            $datosform['tiempo'] = null;
                        }

                        $datos->setNombre($datosform['nombre'])
                                ->setDescripcion($datosform['descripcion'])
                                ->setTiempo($datosform['tiempo'])
                                ->setVisibilidad($datosform["visibilidad"]);
            
                        $this->emInstance->persist($datos);
                        $this->emInstance->flush();
                        $this->addFlash('correcto', 'Los cambios han sido guardados correctamente.');

                        return $this->redirectToRoute($slug . '_editar', array(
                                    'id' => $datos->getId()));
                    }
                }
            } catch (\Exception $e) {
                
                $this->addFlash('error', 'Ha ocurrido un error al guardar los cambios.');
                return $this->redirectToRoute('listado_' . $slug);

            }
        } else { // No se ha enviado el formulario
            $this->addFlash('error', 'Ha ocurrido un error al enviar el formulario.');
            return $this->redirectToRoute('listado_' . $slug);
        }
    }

    /**
     * Devuelve un json con la información del componente indicado.
     *
     * @param Request $request Petición que se envia desde el navegador y que debe incluir el id del componente a buscar.
     * @return json Cadena con estructura json que incluye todos los datos del componente que se ha buscado.
     * 
     * @Route("/simulaciones/metricas/info", name="metricas_info")
     */
    public function infoMetricasAction(Request $request) {

        $response = new JsonResponse();
        $id = $request->request->get('id');

        $datos = new Metricas();

        if ($id != null && $id > 0) {

            $datos = $this->getDoctrine()
                    ->getRepository(Metricas::class)
                    ->find($id);

            $response->setData($datos->toArray());
        }

        return $response;
    }

    /**
     * Devuelve un json con la información del componente indicado.
     *
     * @param Request $request Petición que se envia desde el navegador y que debe incluir el id del componente a buscar.
     * @return json Cadena con estructura json que incluye todos los datos del componente que se ha buscado.
     * 
     * @Route("/simulaciones/eventos/info", name="eventos_info")
     */
    public function infoEventosAction(Request $request) {

        $response = new JsonResponse();
        $id = $request->request->get('id');

        $datos = new Eventos();

        if ($id != null && $id > 0) {

            $datos = $this->getDoctrine()
                    ->getRepository(Eventos::class)
                    ->find($id);

            $response->setData($datos->toArray());
        }

        return $response;
    }

    /**
     * Guarda los datos del formulario de creación/edición de una salida de un componente de un sistema.
     * 
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador del sistema que se va a editar, nulo si es nuevo.
     * 
     * @return Response Respuesta http que se envia al navegador del cliente.
     * 
     * @Route("/simulaciones/guardarevento/{id}", name="simulaciones_guardarevento",requirements={"slug"="simulaciones", "id"="\d+"}, defaults={"slug" = "simulaciones"})
     */
    public function guardarEventoAction(Request $request, $slug = "sistemas", $id = null) {

        $url_vuelta = "";

        if (str_contains($request->server->get("HTTP_REFERER"), "visualizar")) {
            $url_vuelta = $slug . "_visualizar";
        } else {
            $url_vuelta = $slug . "_editar";
        }

        if ($request->request->get('form')) { // Se ha enviado el formulario
            $datosform = $request->request->get('form');

            $simulacion = $this->getDoctrine()->getRepository(Simulaciones::class)->find($datosform["evento_simulacion_id"]);

            if ($simulacion->getUsuarios()->getId() != $this->getId()) { // No tiene permisos de escritura
                $this->addFlash('error', 'Ha ocurrido un error al eliminar los datos:<br>No tiene permisos de escritura sobre el objeto.');
                return $this->redirectToRoute($slug . '_editar', array('id' => $id));
            }

            $datos = new Eventos();

            if ($datosform['evento_id']) { // Si $id está definido, actualiza el elemento
                $datos = $this->getDoctrine()->getRepository(Eventos::class)->find($datosform['evento_id']);
            }

            $datos->setNombre($datosform['evento_nombre'])
                    ->setDescripcion($datosform['evento_descripcion'])
                    ->setInstante($datosform['evento_instante']);

            if (isset($datosform['evento_componente'])) {
                if ($datosform['evento_componente'] != "") {

                    $datos->setComponenteid($datosform['evento_componente'])
                            ->setEstado($datosform['evento_estado']);

                    if (isset($datosform['evento_tipofallo'])) {

                        if ($datosform['evento_tipofallo'] == "") {
                            $datos->setTipofallo(null);
                        } else {
                            $datos->setTipofallo($datosform['evento_tipofallo']);
                        }
                    }

                    if ($datosform['evento_porcentajefallo'] == "") {
                        $datos->setPorcentajefallo(100);
                    } else {
                        $datos->setPorcentajefallo($datosform['evento_porcentajefallo']);
                    }

                    $datos->setEntradaid(null)
                            ->setValor(null);
                }
            }

            if (isset($datosform['evento_entrada'])) {
                if ($datosform['evento_entrada'] != "") {
                    $datos->setComponenteid(null)
                            ->setEstado(null)
                            ->setPorcentajefallo(null)
                            ->setTipofallo(null)
                            ->setEntradaid($datosform['evento_entrada'])
                            ->setValor($datosform['evento_valor']);
                }
            }

            //Hay que establecer las dos relaciones para que los datos se guarden correctamente.
            $datos->setSimulaciones($simulacion);
            $simulacion->getEventos()->add($datos);

            try {

                if (isset($datosform['evento_eliminar'])) {
                    $this->emInstance->remove($datos);
                    $this->emInstance->flush();
                    $this->addFlash('correcto', 'Los cambios han sido ELIMINADOS correctamente.');
                }

                if (isset($datosform['evento_guardar'])) {
                    $this->emInstance->persist($datos);
                    $this->emInstance->flush();
                    $this->addFlash('correcto', 'Los cambios han sido guardados correctamente.');
                }

                if (isset($datosform['evento_clonar'])) {

                    $clonado = new Eventos();
                    $clonado->setComponenteid($datos->getComponenteid());
                    $clonado->setDescripcion($datos->getDescripcion());
                    $clonado->setEntradaid($datos->getEntradaid());
                    $clonado->setEstado($datos->getEstado());
                    $clonado->setInstante($datos->getInstante());
                    $clonado->setNombre($datos->getNombre() . " (copia)");
                    $clonado->setPorcentajefallo($datos->getPorcentajefallo());
                    $clonado->setSimulaciones($datos->getSimulaciones());
                    $clonado->setTipofallo($datos->getTipofallo());
                    $clonado->setValor($datos->getValor());

                    $this->emInstance->persist($clonado);
                    $this->emInstance->flush();
                    $this->addFlash('correcto', 'Los cambios han sido clonado correctamente.');
                }

                return $this->redirectToRoute($url_vuelta, array(
                            'id' => $id));
            } catch (\Exception $e) {
                $this->addFlash('error', 'Ha ocurrido un error al guardar los cambios.');
                return $this->redirectToRoute('listado_' . $slug );
            }
        } else { // No se ha enviado el formulario
            $this->addFlash('error', 'Ha ocurrido un error al enviar el formulario.');
            return $this->redirectToRoute('listado_' . $slug);
        }
    }

    /**
     * Ejecuta el siguiente paso de una simulacion ya iniciada
     * 
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador del sistema que se va a editar, nulo si es nuevo.
     * 
     * @return Response Respuesta http que se envia al navegador del cliente
     * 
     * @Route("/simulaciones/siguientepaso", name="simulacion_siguientepaso",requirements={"slug"="simulaciones", "id"="\d+"}, defaults={"slug" = "simulaciones"})
     */
    public function siguientePasoAction(Request $request) {

        $idEjecucion = $request->request->get('ejecucion');
        $idSimulacion = $request->request->get('simulacion');
        //Si la ejecución es nula, inicia una nueva ejecución de la simulación.

        $response = new JsonResponse();

        //Recoge una simulacion y ejecuta el siguiente paso, para ello:
        //  aplica los eventos.
        //  Recalcula
        //  Guarda las metricas configuradas.

        if ($idEjecucion == null) {
            //Crea una nueva ejecución para la simulacion indicada.
            $ejecucion = new Ejecuciones();

            $usuario = $this->getDoctrine()
                    ->getRepository(Usuarios::class)
                    ->find($this->getUser()->getId());

            $simulacion = $this->getDoctrine()
                    ->getRepository(Simulaciones::class)
                    ->find($idSimulacion);

            $ejecucion->setUsuario($usuario)
                    ->setSimulacion($simulacion)
                    ->setFecha(new \DateTime("now"))
                    ->setDescripcion(null)
                    ->setEstado(1);

            $this->emInstance->persist($ejecucion);
            $this->emInstance->flush();
            $idEjecucion = $ejecucion->getIdejecucion();
        } else {
            //Es una ejecución que ya esta en marcha, se recupera de la sesion, sino esta 
            //  en la sessión, pudiera estar disponible de una ejecución anterior.

            $ejecucion = $this->get('session')->get('ejecucion_' . $idEjecucion);

            if (is_null($ejecucion)) {
                //La recupera de la base de datos, ya que no estaba en proceso.
                $ejecucion = $this->getDoctrine()
                        ->getRepository(Ejecuciones::class)
                        ->find($idEjecucion);
            }
            if ($ejecucion->getSituacion() != null) {
                $ejecucion = unserialize($ejecucion->getSituacion());
            }
        }

        Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "] Ejecucion" . $ejecucion->getIdejecucion() . " esta en el paso: " . $ejecucion->getStep());
        $resultados = $ejecucion->nextStep();

        $actualiza = $this->getDoctrine()
                ->getRepository(ActualizaEjecucion::class)
                ->find($idEjecucion);
        $actualiza->setStep($ejecucion->getStep());
        $actualiza->setSituacion($ejecucion->getSituacion());

        $this->emInstance->persist($actualiza);
        $this->emInstance->flush();

        //Guarda la situacion en la sesion para continuar
        $this->get('session')->set('ejecucion_' . $idEjecucion, $ejecucion);

        $respuesta = array("ejecucion" => $idEjecucion, "paso" => $ejecucion->getStep(), "simulacion" => $ejecucion->getSimulacion()->getId());
        $respuesta["simulacion"] = $ejecucion->getSimulacion()->toArray();
        $respuesta["sistema"] = $ejecucion->getSimulacion()->getSistema()->toArray();
        $respuesta["metricas"] = $resultados["metricas"];

        //Guarda las metricas en la base de datos.
        foreach ($respuesta["metricas"] as $metrica) {

            $valor = new MetricasValores();

            $valor->setIdejecucion($metrica["idejecucion"]);
            $valor->setIdmetrica($metrica["idmetrica"]);
            $valor->setInstante($metrica["instante"]);
            $valor->setValor($metrica["valor"]);

            $this->emInstance->persist($valor);
            $this->emInstance->flush();
        }

        $response->setData($respuesta);

        return $response;
    }

    /**
     * Guarda los datos del formulario de creación/edición de una salida de un componente de un sistema.
     *
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador del sistema que se va a editar, nulo si es nuevo.
     * 
     * @return Response Respuesta http que se envia al navegador del cliente
     * 
     * @Route("/simulaciones/guardarmetrica/{id}", name="simulaciones_guardarmetrica",requirements={"slug"="simulaciones", "id"="\d+"}, defaults={"slug" = "simulaciones"})
     */
    public function guardarMetricaAction(Request $request, $slug = "sistemas", $id = null) {

        $url_vuelta = "";

        if (str_contains($request->server->get("HTTP_REFERER"), "visualizar")) {
            $url_vuelta = $slug . "_visualizar";
        } else {
            $url_vuelta = $slug . "_editar";
        }

        var_dump($request->request);

        if ($request->request->get('form')) { // Se ha enviado el formulario
            $datosform = $request->request->get('form');

            $simulacion = $this->getDoctrine()->getRepository(Simulaciones::class)->find($datosform["metrica_simulacion_id"]);
            if ($simulacion->getUsuarios()->getId() != $this->getId()) { // No tiene permisos de escritura
                $this->addFlash('error', 'Ha ocurrido un error al eliminar los datos:<br>No tiene permisos de escritura sobre el objeto.');
                return $this->redirectToRoute($slug . '_editar', array('id' => $id));
            }

            $datos = new Metricas();

            if ($datosform['metrica_id']) { // Si $id está definido, actualiza el elemento
                $datos = $this->getDoctrine()->getRepository(Metricas::class)->find($datosform['metrica_id']);
            }

            $datos->setNombre($datosform['metrica_nombre'])
                    ->setColor($datosform['metrica_color'])
                    ->setDescripcion($datosform['metrica_descripcion']);

            if (isset($datosform['metrica_componente'])) {
                if ($datosform['metrica_componente'] != "") {

                    $datos->setidComponente($datosform['metrica_componente'])
                            ->setidentrada(null)
                            ->setidsalida(null)
                            ->setidfiabilidad(null);
                }
            }

            if (isset($datosform['metrica_salida'])) {
                if ($datosform['metrica_salida'] != "") {
                    $datos->setidComponente(null)
                            ->setIdentrada(null)
                            ->setIdsalida($datosform['metrica_salida'])
                            ->setIdfiabilidad(null);
                }
            }

            if (isset($datosform['metrica_entrada'])) {
                if ($datosform['metrica_entrada'] != "") {
                    $datos->setidComponente(null)
                            ->setIdentrada($datosform['metrica_entrada'])
                            ->setIdsalida(null)
                            ->setIdfiabilidad(null);
                }
            }

            if (isset($datosform['metrica_fiabilidad'])) {
                if ($datosform['metrica_fiabilidad'] != "") {
                    Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "]" . "Inserta fiabilidad calculada:" . $datosform['metrica_fiabilidad']);
                    $datos->setIdComponente(null)
                            ->setIdentrada(null)
                            ->setIdsalida(null);
                    
                            $datos->setIdfiabilidad($datosform['metrica_fiabilidad']);
                     
                }
            }

            //Hay que establecer las dos relaciones para que los datos se guarden correctamente.
            $datos->setSimulacion($simulacion);
            $simulacion->getMetricas()->add($datos);
 
            try {

                if (isset($datosform['metrica_eliminar'])) {
                    $this->emInstance->remove($datos);
                    $this->emInstance->flush();
                    $this->addFlash('correcto', 'Los cambios han sido ELIMINADOS correctamente.');
                }

                if (isset($datosform['metrica_guardar'])) {
                    $this->emInstance->persist($datos);
                    Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "]" . "Despues de Insertar fiabilidad calculada:" . $datos->getIdfiabilidad());       
                    $this->emInstance->flush();
                    $this->addFlash('correcto', 'Los cambios han sido guardados correctamente.');
                }

                return $this->redirectToRoute($url_vuelta, array(
                            'id' => $id));
            } catch (\Exception $e) {
                $this->addFlash('error', 'Ha ocurrido un error al guardar los cambios.');

                 return $this->redirectToRoute('listado_' . $slug );
            }
        } else { // No se ha enviado el formulario
            $this->addFlash('error', 'Ha ocurrido un error al enviar el formulario.');
            return $this->redirectToRoute('listado_' . $slug);
        }
    }

    /**
     * Elimina todos los resultados de una ejecución.
     * 
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador de la ejecución que se va a eliminar
     * 
     * @return Response Respuesta HTTP que se envia al navegador del cliente
     * 
     * @Route("/simulaciones/eliminarejecucion/{id}", name="ejecuciones_eliminar",requirements={"slug"="simulaciones", "id"="\d+"}, defaults={"slug" = "simulaciones"})
     */
    public function eliminarEjecucionAction(Request $request, $slug = "sistemas", $id = null): Response {

        $queryBuilder = $this->emInstance->createQueryBuilder()
                ->delete("App:MetricasValores", 'v')
                ->where('v.idejecucion = :id')
                ->setParameter(':id', $id);

        $queryBuilder->getQuery()
                ->execute();

        $queryBuilder = $this->emInstance->createQueryBuilder()
                ->delete("App:Ejecuciones", 'e')
                ->where('e.idejecucion = :id')
                ->setParameter(':id', $id);

        $queryBuilder->getQuery()
                ->execute();

        $this->addFlash('correcto', 'La ejecución ' . $id . ' se ha eliminado correctamente.');

        return $this->redirectToRoute('listado_misejecuciones');
    }

    /**
     * INICIAR SIMULACION
     * 
     * Muestra la pantalla para iniciar una simulación
     * 
     * @Route("/simulaciones/simular/{id}", name="simulaciones_simular",requirements={"slug"="simulaciones", "id"="\d+"}, defaults={"slug" = "simulaciones"})
     * 
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador de la simulacion que se va a iniciar
     * 
     * @return Response
     * 
     * @author Antonio Gómez <i52goloa@uco.es>
     * 
     */
    public function simularAction(Request $request, $slug = "sistemas", $id = null): Response {

        //$f = $this->get("funciones");
        //$f->logInfo("Se va a ejecutar una simulacion");
        // Obtiene listado
        $listado = $this->getDoctrine()
                ->getRepository(AppListados::class)
                ->findBySlug($slug);

        //Comprueba si el usuario tiene permisos.
        //MITODO "PERMISOS???";
        // Comprueba el acceso como administrador
        $disabled = false;
        // Comprueba los permisos

        $datos = new Simulaciones();

        if ($id != "") {
            // Obtiene información de la simulación que se va a iniciar.

            $datos = $this->getDoctrine()
                    ->getRepository(Simulaciones::class)
                    ->find($id);

            if (is_null($datos)) {
                $this->volverInicio("Simulacion no encontrada.");
            }
        } else {
            $this->volverInicio("Simulacion no indicada.");
        }

        // Define la cabecera del listado.
        $cabecera = new CabeceraListado("Simulación: \"" . $datos->getNombre() . "\"", $listado->getImagens(), true);
        $cabecera->setPaginaedicion($listado->getPaginaedicions());
        $cabecera->setCrear(true);
        $cabecera->setBotones(array(array("boton" => "Editar", "enlace" => "/SiSToFa/simulaciones/editar/" . $id)));
        $cabecera->setRutavuelta("listado_" . $slug);

        try {

            $datos->getSistema()->calculaFiabilidad();
            $datos->getSistema()->calcular();
        } catch (\Exception $ex) {
            $error = "Se ha producido un error realizando los calculos del sistema." . $ex->getMessage();
        }

        return $this->render('sistema/sistemas_grafico.html.twig', [
                    'cabecera' => $cabecera,
                    'listado' => $listado,
                    'sistema' => $datos->getSistema(),
                    'simulacion' => $datos,
                    'ejecucion' => null,
                    'tienePermisos' => 'escritura',
                    'error' => ""
        ]);
    }

    /**
     * Ejecuta el siguiente paso de una simulacion
     * 
     * @Route("/simulaciones/continuar/{id}", name="simulacion_continuar",requirements={"slug"="simulaciones", "id"="\d+"}, defaults={"slug" = "simulaciones"})
     *
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador de la ejecucion que se va a continuar
     * 
     * @return Response
     * 
     * @author Antonio Gómez <i52goloa@uco.es>
     */
    public function continuarAction(Request $request, $slug = "simulaciones", $id = null): Response {

        // Obtiene listado
        $listado = $this->getDoctrine()
                ->getRepository(AppListados::class)
                ->findBySlug($slug);

        //Comprueba si el usuario tiene permisos.
        //MITODO "PERMISOS???";
        // Comprueba el acceso como administrador
        $disabled = false;
        // Comprueba los permisos

        $ejecucion = new Ejecuciones();

        if ($id != "") {
            // Obtiene información de la ejecucion que se va a iniciar.

            $ejecucion = $this->getDoctrine()
                    ->getRepository(Ejecuciones::class)
                    ->find($id);

            if (is_null($ejecucion)) {
                $this->volverInicio("Ejecucion no encontrada.");
            }
        } else {
            $this->volverInicio("Ejecucion no indicada.");
        }

        // Define la cabecera del listado.
        $cabecera = new CabeceraListado("Ejecucion: \"" . $ejecucion->getIdejecucion() . "\"", $listado->getImagens(), true);
        $cabecera->setPaginaedicion($listado->getPaginaedicions());
        $cabecera->setCrear(true);
        $cabecera->setBotones(array(array("boton" => "Editar", "enlace" => "/SiSToFa/simulaciones/editar/" . $ejecucion->getIdsimulacion())));
        $cabecera->setRutavuelta("listado_" . $slug);

        try {

            $ejecucion->getSimulacion()->getSistema()->calculaFiabilidad();
            $ejecucion->getSimulacion()->getSistema()->calcular();
        } catch (\Exception $ex) {
            $error = "Se ha producido un error realizando los calculos del sistema." . $ex->getMessage();
        }

        return $this->render('sistema/sistemas_grafico.html.twig', [
                    'cabecera' => $cabecera,
                    'listado' => $listado,
                    'sistema' => $ejecucion->getSimulacion()->getSistema(),
                    'simulacion' => $ejecucion->getSimulacion(),
                    'ejecucion' => $ejecucion,
                    'tienePermisos' => 'escritura',
                    'error' => ""
        ]);
    }

    /**
     * Devuelve un json con la información de la entrada indicada.
     *
     * @param Request $request Petición que se envia desde el navegador y que debe incluir el id de la entrada a buscar.
     * @return json Cadena con estructura json que incluye todos los datos de la entrada del componente que se ha buscado.
     * 
     * @Route("/simulaciones/metricas/download", name="metrica_download",requirements={"slug"="simulaciones"}, defaults={"slug" = "simulaciones"})
     *
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador de la ejecucion que se va a continuar
     * 
     * @return Response
     * 
     * @author Antonio Gómez <i52goloa@uco.es>
     */
    public function descargarMetricaAction(Request $request): Response {

        $id = $request->get('id');
        $idsimulacion = $request->get('simulacion');
        $fichero = $request->get('fichero');

        $valores = $this->getDoctrine()
                ->getRepository(MetricasValores::class)
                ->findValoresEjecucion($id, $idsimulacion);

        $encoders = [new CsvEncoder()];
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $csvContent = $serializer->serialize($valores, 'csv');

        $response = new Response($csvContent);
        $response->headers->set('Content-Encoding', 'UTF-8');
        $response->headers->set('Content-Type', 'text/csv; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'attachment; filename=' . $fichero . '.csv');
        return $response;
    }

    /**
     * Devuelve un json con la lista de ejecuciones de uns simulacion.
     *
     * @param Request $request Petición que se envia desde el navegador y que debe incluir el id de la entrada a buscar.
     * @return json Cadena con estructura json que incluye todos los datos de la entrada del componente que se ha buscado.
     * 
     * @Route("/simulaciones/listaejecuciones", name="ejecuciones_comparables",requirements={"slug"="simulaciones"}, defaults={"slug" = "simulaciones"})
     * 
     * @author Antonio Gómez <i52goloa@uco.es>
     */
    public function listaEjecucionesComparables(Request $request): Response {

        $response = new JsonResponse();
        $idsimulacion = $request->get('simulacion');

        $ejecuciones = $this->getDoctrine()
                ->getRepository(VwEjecuciones::class)
                ->findEjecucionesSimulacion($idsimulacion);
        $valores = array();

        foreach ($ejecuciones as $ejecucion) {
            $valores[] = $ejecucion->toArray();
        }

        $response->setData($valores);
        return $response;
    }

}
