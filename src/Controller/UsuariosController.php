<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use App\Entity\Usuarios;
use App\Entity\Grupos;
use App\Entity\VwSistemas;
use App\Entity\AppListados;
use App\Entity\CabeceraListado;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Clase controlador de las acciones de usuario relativas a la gestión de usuarios y permisos
 * dentro de la aplicación.
 *
 * @author Antonio Gómez <i52goloa@uco.es>
 */
class UsuariosController extends SisToFaController {

    /**
     * MIS DATOS (EDITAR USUARIO ACTUAL)
     *
     * @Route("/administracion/misdatos", name="misdatos", requirements={"slug"="misdatos"}, defaults={"slug" = "misdatos"})
     */
    public function misDatosAction(Request $request, $slug = "misdatos") {

        return $this->editarAction($request, "usuarios", $this->getUser()
                                ->getId(), $soloconsulta = 1);
    }

    /**
     * EDITAR USUARIO
     *
     * @Route("/administracion/{slug}/crear", name="usuarios_crear", requirements={"slug"="usuarios"}, defaults={"slug" = "usuarios"})
     * @Route("/administracion/{slug}/editar/{id}", name="usuarios_editar", requirements={"slug"="usuarios", "id"="\d+"}, defaults={"slug" = "usuarios"})
     * 
     * Acción para mostrar el formulario de edición de usuario en blanco para añadir
     * los datos de otro usuario.
     * 
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador del usuario que se va a editar, nulo si es nuevo.
     * @param boolean $soloconsulta Si es 1 solo permite ver los datos sin editarlos, esta orientado a que el propio usuario vea su información.
     * 
     */
    public function editarAction(Request $request, $slug = "usuarios", $id = null, $soloconsulta = 0) {

        // Obtiene la definición de la pantalla
        $listado = $this->getDoctrine()
                ->getRepository(AppListados::class)
                ->findBySlug($slug);
        
        if (!$this->tieneAcceso($listado) && $soloconsulta != 1) {
            return $this->volverInicio("No tiene acceso a esta opción.");
        }
        
        //Si es el propio usuario consultando sus datos
        if ($soloconsulta == 1) {
            $disabled = true;
        } else {
            $disabled = false;
        }
        
        if ($this->getUser()->getNivel()==1 ) {
            $disabledgrupo = true;
        } else {
            $disabledgrupo = false;
        }
        
        
        //Si es administrador de Grupo, comprueba si el usuario esta en su grupo.
        $datos = new Usuarios();

        //Si hay identificador se busca el usuario
        if ($id != null) {
            $datos = $this->getDoctrine()
                    ->getRepository(Usuarios::class)
                    ->find($id);

            if (empty($datos)) {
                return $this->volverInicio("Usuario no encontrado.");
            }
                    
            if($this->getUser()->getNivel()==1 
                    && $datos->getGrupo()->getIdgrupo() != $this->getUser()->getGrupo()->getIdgrupo()) {
                  return $this->volverInicio("El usuario no se puede editar porque no está en el grupo administrado.");
                
            }
        }

        // ------------------------------
        // FORMULARIO DE CREACIÓN/EDICIÓN
        // ------------------------------
        $builder = $this->createFormBuilder()->setAction($this->generateUrl($slug . '_guardar', array(
                    'id' => $id
        )));

        // Se añaden los campos del formulario
        $builder->add('nombre', TextType::class, array(
            'required' => true,
            'label' => 'Nombre*',
            'data' => $datos->getNombre(),
            'disabled' => $disabled
        ));

        $builder->add('apellidos', TextType::class, array(
            'required' => false,
            'label' => 'Apellidos',
            'data' => $datos->getApellidos(),
            'disabled' => $disabled
        ));

        $builder->add('dni', TextType::class, array(
            'required' => false,
            'label' => 'DNI',
            'data' => $datos->getDni(),
            'disabled' => $disabled
        ));

        $builder->add('login', TextType::class, array(
            'required' => false,
            'label' => 'Login',
            'data' => $datos->getLogin(),
            'disabled' => $disabled
        ));

        $builder->add('email', TextType::class, array(
            'required' => false,
            'label' => 'E-Mail',
            'data' => $datos->getEmail(),
            'disabled' => $disabled
        ));

        $builder->add('estado', ChoiceType::class, array(
            'required' => true,
            'label' => 'Estado del Usuario',
            'data' => $datos->getEstado(),
            'choices' => array(
                "Activado" => "1",
                "BLOQUEADO" => "2"
            ),
            'disabled' => $disabled
        ));

        if($this->getUser()->getNivel() == 1) {
             $builder->add('nivel', ChoiceType::class, array(
            'required' => true,
            'label' => 'Rol del Usuario',
            'data' => $datos->getNivel(),
            'choices' => array(
                "Administrador de grupo" => "1",
                "Usuario" => "0"
            ),
            'disabled' => $disabled
            ));
        }
        else {
            
        $builder->add('nivel', ChoiceType::class, array(
            'required' => true,
            'label' => 'Rol del Usuario',
            'data' => $datos->getNivel(),
            'choices' => array(
                "Administrador" => "10",
                "Administrador de grupo" => "1",
                "Usuario" => "0"
            ),
            'disabled' => $disabled
            ));
        }
        

        //Obtiene los grupos disponibles del repositorio de grupos.
        $grupos = $this->getDoctrine()
                ->getRepository(Grupos::class)
                ->findAll();

        $grupo = null;
        if ($datos->getGruposIdgrupo() != "") {
            $grupo = $this->getDoctrine()->getRepository(Grupos::class)->find($datos->getGruposIdgrupo());
        }

        //Si es un usuario administrador de grupos, solo puede trabajar con su propio grupo.
        if($this->getUser()->getNivel() == 1) {
             $grupo = $this->getDoctrine()->getRepository(Grupos::class)->find($this->getUser()->getGrupo()->getIdgrupo());
         }
         
        $builder->add('grupo', EntityType::class, array(
            'label' => 'Grupo:',
            'class' => Grupos::class,
            'choice_label' => 'nombre',
            'mapped' => false,
            'data' => $grupo,
            'disabled' => $disabled || $disabledgrupo
        ));

        // El botón de guardar solo se mostrará si tiene permisos de escritura
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && $soloconsulta == 0) {
            $builder->add('guardar', SubmitType::class);
            $builder->add('clonar', SubmitType::class);
            $builder->add('eliminar', SubmitType::class, array('label' => "Eliminar"));
        }

        // El formulario de creación/edición es enviado
        $form = $builder->getForm();
        
        // Si el formulario ha sido enviado y se ha producido un error recuerda los datos introducidos
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
        }

        // Define la cabecera del listado.
        $cabecera = new CabeceraListado($datos->getNombreCompleto(), $listado->getImagens());
        $cabecera->setPaginaedicion($listado->getPaginaedicions());
        if ($soloconsulta == 1) {
            $cabecera->setNolistado(true);
            $cabecera->setTexto("Mis Datos: " . $datos->getNombreCompleto());
            $cabecera->setCrear(false);
        } else {
            $cabecera->setCrear(true);
        }

        $builderPassword = null;
        //Crea un nuevo formulario para el password
        if($soloconsulta==0)
        {
            $builderPassword = $this->createFormBuilder()->setAction($this->generateUrl('usuario_cambiapassword', array('id' => $id)));            
        }
        else{
            $builderPassword = $this->createFormBuilder()->setAction($this->generateUrl('misdatos_cambiapassword', array('id' => $id)));            
        }
        // Se añaden los campos del formulario
        $builderPassword->add('nuevapassword', PasswordType::class, array(
            'required' => true,
            'label' => 'Nueva contraseña*',
            'data' => ""
        ));

        $builderPassword->add('confirmapassword', PasswordType::class, array(
            'required' => true,
            'label' => 'Confirmar la contraseña*',
            'data' => ""
        ));
        
        $builderPassword->add('cambiar', SubmitType::class);
        $formPassword = $builderPassword->getForm();

        $cabecera->setRutavuelta("listado_" . $slug);

        $sistemas = $this->getDoctrine()
                ->getRepository(VwSistemas::class)
                ->findByPropietario($id);

        return $this->render('administracion/usuarios_editar.html.twig', array(
                    'formulario' => $form->createView(),
                    'formularioPassword' => $formPassword->createView(),
                    'datos' => $datos,
                    'cabecera' => $cabecera,
                    'listado' => $listado,
                    'sistemas' => $sistemas,
                    'tienePermisos' => true,
                    'datosHistorial' => $datos->getHistorial(),
                    'soloconsulta' => $soloconsulta
        ));
    }

    /**
     * Guarda los datos del formulario de creación/edición de usuario.
     *
     * @Route("/administracion/{slug}/guardar/{id}", name="usuarios_guardar", requirements={"slug"="usuarios", "id"="\d+"}, defaults={"slug" = "usuarios"})
     * @Route("/administracion/{slug}/guardar/{id}", name="mis_datos_guardar", requirements={"slug"="mis_datos", "id"="\d+"}, defaults={"slug" = "mis_datos"})
     * 
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador del usuario que se va a editar, nulo si es nuevo.
     * @return Response Respuesta http que se envia al navegador del usuario.
     * 
     */
    public function guardarAction(Request $request, $slug = "usuarios", $id = null) {

        // Obtiene listado
        $listado = $this->getDoctrine()
                ->getRepository(AppListados::class)
                ->findBySlug($slug);
        
        if (!$this->tieneAcceso($listado)) {
            return $this->volverInicio("No tiene acceso a esta opción.");
        }
       
            if ($request->request->get('form')) { // Se ha enviado el formulario
                $datosform = $request->request->get('form');

                $datos = new Usuarios();

                if ($id) { // Si $id está definido, actualiza el elemento
                    $datos = $this->getDoctrine()->getRepository(Usuarios::class)->find($id);
                    
            if($this->getNivel() == 10 || ($this->getNivel() == 1 && $this->getGrupo() == $datos->getGrupo()->getIdgrupo()) ) {
                
            }
            else {
                    $this->addFlash('error', 'Se ha intentado editar un usuario sin permiso.');
                  return $this->redirectToRoute('listado_usuarios');
            }
            
                }

                $datos->setApellidos($datosform['apellidos'])
                        ->setDni($datosform['dni'])
                        ->setNombre($datosform['nombre'])
                        ->setEmail($datosform['email'])
                        ->setEstado($datosform['estado'])
                        ->setLogin($datosform['login'])
                        ->setNivel($datosform['nivel']);
                
                if($this->getNivel() == 1) {
                       $datos->setGruposIdgrupo($this->getGrupo());
                }
                else {
                    $datos->setGruposIdgrupo($datosform['grupo']);
                }

                $datos->setGrupo($this->getDoctrine()->getRepository(Grupos::class)->find($datos->getGruposIdgrupo()));
                      
                try {

                    //Depende del botón indicado ejecuta una u otra opción.
                    if (isset($datosform["eliminar"])) {

                        $this->emInstance->remove($datos);
                        $this->emInstance->flush();
                        $this->addFlash('correcto', 'Los datos se han eliminado correctamente.');
                        return $this->redirectToRoute('listado_' . $slug);
                        
                    } else {

                        if (isset($datosform["clonar"])) {

                            $nuevo = new Usuarios();

                            $nuevo->setNombre($datosform['nombre'] . " (COPIA)")
                                ->setApellidos($datos->getApellidos())
                                ->setDni("")
                                ->setEmail("")
                                ->setEstado($datosform['estado'])
                                ->setGrupo($this->getDoctrine()->getRepository(Grupos::class)->find($datosform['idgrupo']()))
                                ->setGruposIdgrupo($datosform['idgrupo']())
                                ->setLogin($datosform['login']. "_copia")
                                ->setNivel($datos->getNivel())
                                ->setNombre($datos->getNombre())
                                ->setPassword($datos->getPassword());

                            $this->emInstance->persist($nuevo);
                            $this->emInstance->flush();
                            
                            $this->addFlash('correcto', 'Los datos se han clonado correctamente.');
                            return $this->redirectToRoute($slug . '_editar', array(
                                        'id' => $nuevo->getId()));
                            
                        } else {

                            $this->emInstance->persist($datos);
                            $this->emInstance->flush();

                            //$em->flush();

                            $this->addFlash('correcto', 'Los cambios han sido guardados correctamente.');

                            return $this->redirectToRoute($slug . '_editar', array(
                                        'id' => $datos->getId()));
                        }
                    }
                } catch (\Exception $e) {
                    $this->addFlash('error', 'Ha ocurrido un error al guardar los cambios:<br><i>' . $datosGuardar->VerError() . '</i>');
                    return $this->redirectToRoute('listado_' . $slug );
                }
            } else { // No se ha enviado el formulario
                $this->addFlash('error', 'Ha ocurrido un error al enviar el formulario.');
                return $this->redirectToRoute('listado_' . $slug);
            }
        
    }
    
    /**
     * Cambia la constraseña desde la opción misdatos, solo la puede cambiar para el usuario conectado.
     *
     * @Route("/administracion/{slug}/cambiapassword", name="misdatos_cambiapassword", requirements={"slug"="misdatos"}, defaults={"slug" = "misdatos"})
     */
    public function misDatosCambiaPassword(Request $request, $slug = "mis_datos") {

        //Compruena que los passwords introducidos son los mismos.
        if ($request->request->get('form')) { // Se ha enviado el formulario
            $datosform = $request->request->get('form');

            if ($datosform["nuevapassword"] != $datosform["confirmapassword"]) {

                $this->addFlash('error', 'Ha ocurrido un error reestableciendo la contraseña del usuario.<br> Las contraseñas no coinciden.');
                
            } else {
                $datos = new Usuarios();
                $datos = $this->getDoctrine()->getRepository(Usuarios::class)->find($this->getUser()->getId());

                $datos->setPassword(md5($datosform["nuevapassword"]));

                try {

                    $this->emInstance->persist($datos);
                    $this->emInstance->flush();
                    $this->addFlash('correcto', 'Los datos se han guardado correctamente.');
                                        
                } catch (\Exception $e) {
                              $this->addFlash('error', 'Ha ocurrido un error al guardar los cambios.');

                }
            }
        }
        else { // No se ha enviado el formulario
                $this->addFlash('error', 'Ha ocurrido un error al enviar el formulario.');
        }
        
         return $this->redirectToRoute($slug,array(), 302);
         
    }

    /**
     * Cambia la constraseña desde la opción misdatos, solo la puede cambiar para el usuario conectado.
     *
     * @Route("/administracion/{slug}/cambiapassword/{id}", name="usuario_cambiapassword", requirements={"slug"="usuarios", "id"="\d+"}, defaults={"slug" = "usuarios"})
     */
    public function usuariosCambiaPassword(Request $request, $slug = "usuarios",$id=null) {

        //Compruena que los passwords introducidos son los mismos.
        if ($request->request->get('form')) { // Se ha enviado el formulario
            $datosform = $request->request->get('form');

            if ($datosform["nuevapassword"] != $datosform["confirmapassword"]) {

                $this->addFlash('error', 'Ha ocurrido un error reestableciendo la contraseña del usuario.<br> Las contraseñas no coinciden.');
                
            } else {
                $datos = new Usuarios();
                $datos = $this->getDoctrine()->getRepository(Usuarios::class)->find($id);

                $datos->setPassword(md5($datosform["nuevapassword"]));

                try {

                    $this->emInstance->persist($datos);
                    $this->emInstance->flush();
                    $this->addFlash('correcto', 'Los datos se han guardado correctamente.');
                                        
                } catch (\Exception $e) {
                              $this->addFlash('error', 'Ha ocurrido un error al guardar los cambios.');

                }
            }
        }
        else { // No se ha enviado el formulario
                $this->addFlash('error', 'Ha ocurrido un error al enviar el formulario.');
        }
        
            return $this->redirectToRoute($slug . '_editar', array(
                                'id' => $id
                                    ), 302);
         
    }
    
}
