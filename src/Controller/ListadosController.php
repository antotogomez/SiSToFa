<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use App\Services\Funciones;
use App\Entity\CabeceraListado;
use App\Entity\AppListados;
use App\Entity\AppCategoriasGenericas;
use App\Repository\AppListadosRepository;
use App\Controller\SimSitfaController;

/**
 * Clase controlador de las acciones de usuario relativas a la gestión de listados
 * de la aplicación.
 *
 * @author Antonio Gómez <i52goloa@uco.es>
 */
class ListadosController extends SisToFaController {

    /**
     * Acción que muestra el formulario de edición de los listados para editar uno ya existente o 
     * crear uno nuevo.
     *
     * @param Request  $request Petición que envia el usuario
     * @param sttring  $slug texto que indentifica la url con la que se esta trabajando
     * @param int $id Identificador del objeto a visualizar o editar.
     * @return Response Respuesta http enviada al navegador del usuario
     * 
     * @Route("/aplicacion/{slug}/crear", name="listados_crear", requirements={"slug"="listados"}, defaults={"slug" = "listados"})
     * @Route("/aplicacion/{slug}/editar/{id}", name="listados_editar", requirements={"slug"="listados", "id"="\d+"}, defaults={"slug" = "listados"})
     */
    public function editarAction(Request $request, $slug, $id = null): Response {
        
        // Obtiene el listado
        $listado = $this->getDoctrine()
                ->getRepository(AppListados::class)
                ->findBySlug($slug);
        
        // Comprueba los permisos
        if(!$this->tieneAcceso($listado)) {
            $this->volverInicio("No tiene suficientes privilegios para realizar esta acción");
        }
        $disabled=false;
        if (empty($listado)) {
            $this->volverInicio("No se ha encontrado el listado.");
        }

        // Obtiene  listado
        $datos = $this->getDoctrine()
                ->getRepository(AppListados::class)
                ->find($id);

        if (empty($datos)) {
            $this->volverInicio("Listado \"" . $slug . "\" no encontrado.");
        }

        // Si no existe un elemento con el identificador indicado devuelve error
        if ($id && !$datos) {
            $this->addFlash(
                    'error',
                    'No se ha encontrado ningún elemento con el identificador indicado.'
            );
            return $this->redirectToRoute($definicion['volver_path']);
        }

        // Obtiene las categorías genéricas para el campo TIPO
        $tipos = $this->getDoctrine()
                ->getRepository(AppCategoriasGenericas::class)
                ->findAll();
        // ----------------------
        // FORMULARIO DEL LISTADO
        // ----------------------
        $builder = $this->createFormBuilder()->setAction($this->generateUrl('listados_guardar',
                        array('id' => $id)
        ));

        // Se añaden los campos del formulario de edición de listado
        $builder->add('nombre', TextType::class, array(
            'required' => true,
            'label' => 'Nombre*',
            'data' => $datos->getNombre(),
            'disabled' => $disabled
        ));

        $builder->add('slug', TextType::class, array(
            'required' => true,
            'label' => 'Nombre para la URL*',
            'data' => $datos->getSlug(),
            'disabled' => $disabled
        ));

        $builder->add('texto', TextType::class, array(
            'required' => true,
            'label' => 'Texto*',
            'data' => $datos->getTexto(),
            'disabled' => $disabled
        ));

        $builder->add('imagenS', TextType::class, array(
            'required' => false,
            'label' => 'Imagen para Symfony',
            'data' => $datos->getImagenS(),
            'disabled' => $disabled
        ));

        $builder->add('paginaedicionS', TextType::class, array(
            'required' => false,
            'label' => 'Página de edición en Symfony',
            'data' => $datos->getPaginaedicionS(),
            'disabled' => $disabled
        ));

        $builder->add('campodeordenacion', TextType::class, array(
            'required' => false,
            'label' => 'Campo de ordenación',
            'data' => $datos->getCampodeordenacion(),
            'disabled' => $disabled
        ));

        $builder->add('orden', TextType::class, array(
            'required' => false,
            'data' => $datos->getOrden(),
            'disabled' => $disabled
        ));

        $builder->add('activado', CheckboxType::class, array(
            'required' => false,
            'data' => ($datos->getActivado() == "1" ? true : false),
            'disabled' => $disabled
        ));

        $builder->add('listadodatos', CheckboxType::class, array(
            'required' => false,
            'label' => '¿Es un listado de datos?',
            'data' => ($datos->getListadodatos() == "1" ? true : false),
            'disabled' => $disabled
        ));

        $builder->add('nuevo_bloque', CheckboxType::class, array(
            'required' => false,
            'label' => '¿Añadir separador?',
            'data' => ($datos->getNuevoBloque() == "1" ? true : false),
            'disabled' => $disabled
        ));

        $builder->add('nivel', ChoiceType::class, array(
            'data' => $datos->getNivel(),
            'choices' => array(
                'Usuario' => '0',
                'Administrador de Grupo' => '1',
                'Administrador' => '10'
            ),
            'disabled' => $disabled
        ));

        $tipo = null;
        if ($datos->getTipo() != "") {
            $tipo = $this->getDoctrine()->getRepository(AppCategoriasGenericas::class)->find($datos->getTipo());
        }

        $builder->add('tipo', EntityType::class, array(
            'label' => 'Apartado del menú:',
            'class' => AppCategoriasGenericas::class,
            'query_builder' => function (EntityRepository $er) {
                return $er->findApartadosMenuTodos();
            },
            'choice_label' => 'nombre',
            'mapped' => false,
            'data' => $tipo,
            'disabled' => $disabled
        ));


            $builder->add('guardar', SubmitType::class, array('label' => 'Guardar'));
            $builder->add('clonar', SubmitType::class);
            $builder->add('eliminar', SubmitType::class);

        // El formulario de edición de listados es enviado
        $form = $builder->getForm();

        // Si el formulario ha sido enviado y se ha producido un error recuerda los datos introducidos
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
        }

        //Define la cabecera del listado.
        $cabecera = new CabeceraListado("Listado de " . $datos->getNombre(), $listado->getImagens());
        $cabecera->setPaginaedicion($listado->getPaginaedicions());
        $cabecera->setCrear(true);
        $cabecera->setRutavuelta("listado_listados");

        return $this->render(
                        'administracion/listados_editar.html.twig',
                        array('formulario' => $form->createView(),
                            'permisos' => null,
                            'datos' => $datos,
                            'cabecera' => $cabecera,
                            'listado' => $listado,
                            'tienePermisos' => true
                        )
        );
    }

    /**
     * Guarda los datos del formulario de creación/edición de listados.
     * 
     * @param Request  $request Petición que envia el usuario
     * @param sttring  $slug texto que indentifica la url con la que se esta trabajando
     * @param int $id Identificador del objeto a visualizar o editar.
     * @return Response Respuesta http enviada al navegador del usuario
     * 
     * @Route("/aplicacion/{slug}/guardar/{id}", name="listados_guardar", requirements={"slug"="listados", "id"="\d+"}, defaults={"slug" = "listados"})
     */
    public function guardarAction(Request $request, $slug, $id = null) : Response {
       // Obtiene el listado
        $listado = $this->getDoctrine()
                ->getRepository(AppListados::class)
                ->findBySlug($slug);
        
        // Comprueba los permisos
        if(!$this->tieneAcceso($listado)) {
            $this->volverInicio("No tiene suficientes privilegios para realizar esta acción");
        }
        
        $disabled=false;

        

            if ($request->request->get('form')) { // Se ha enviado el formulario
                $datosform = $request->request->get('form');
                
                $datos = new AppListados();

                    if ($id) { // Si $id está definido, actualiza el elemento
                        $datos = $this->getDoctrine()->getRepository(AppListados::class)->find($id);
                    }
                    
                    $url_vuelta= $slug . '_editar';
                    
                    try {
                        if (isset($datosform["eliminar"])) {
                            $this->emInstance->remove($datos);
                            $this->emInstance->flush();
                            $this->addFlash('correcto', 'Los datos se han eliminado correctamente.');
                            $url_vuelta= 'listado_' . $slug;
                        } else {
                            
                            
                            if (isset($datosform["clonar"])) {
                                
                                $datos = new AppListados();

                                $datos->setNombre($datosform["nombre"] . " (COPIA)");
                                $datos->setSlug($datosform["slug"] . " (COPIA)");
                                $datos->setTexto($datosform["texto"]);
                                $datos->setPaginaedicions($datosform["paginaedicionS"]);
                                $datos->setImagens($datosform["imagenS"]);
                                $datos->setCampodeordenacion($datosform["campodeordenacion"]);
                                $datos->setTipo($datosform["tipo"]);
                                $datos->setNivel($datosform["nivel"]);
                                $datos->setOrden($datosform["orden"]);
                                $datos->setActivado(0);
                                $datos->setListadodatos((isset($datosform["listadodatos"]) ? '1' : '0'));
                                $datos->setNuevoBloque((isset($datosform["nuevo_bloque"]) ? '1' : '0'));
                    
                                $this->emInstance->persist($datos);
                                $this->emInstance->flush();
                                
                                $this->addFlash('correcto', 'Los datos se han clonado correctamente.');
                            } else {
                                
                                //Guarda los datos
                                $datos->setNombre($datosform["nombre"]);
                                $datos->setSlug($datosform["slug"]);
                                $datos->setTexto($datosform["texto"]);
                                $datos->setPaginaedicions($datosform["paginaedicionS"]);
                                $datos->setImagens($datosform["imagenS"]);
                                $datos->setCampodeordenacion($datosform["campodeordenacion"]);
                                $datos->setTipo($datosform["tipo"]);
                                $datos->setNivel($datosform["nivel"]);
                                $datos->setOrden($datosform["orden"]);
                                $datos->setActivado((isset($datosform["activado"]) ? '1' : '0'));
                                $datos->setListadodatos((isset($datosform["listadodatos"]) ? '1' : '0'));
                                $datos->setNuevoBloque((isset($datosform["nuevo_bloque"]) ? '1' : '0'));
                    
                                $this->emInstance->persist($datos);
                                $this->emInstance->flush();
                            }
                        }

                        return $this->redirectToRoute($url_vuelta, array(
                                    'id' => $datos->getIdlistado()));
                        
                    } catch (\Exception $e) {
                        $this->addFlash('error', 'Ha ocurrido un error al guardar los cambios.');

                         return $this->redirectToRoute('listado_' . $slug );
                    }
                
            } else { // No se ha enviado el formulario
                $this->addFlash(
                        'error',
                        'Ha ocurrido un error al enviar el formulario.'
                );
                return $this->redirectToRoute('listado_' . $slug);
            }
        }
    }



?>