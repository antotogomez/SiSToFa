<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\AppCategoriasGenericas;
use App\Entity\AppListados;
use App\Entity\CabeceraListado;
use Doctrine\ORM\EntityRepository;

/**
 * Clase controladores de las acciones de usuario relativas a la gestión de categorias y 
 * tipos genericos de la aplicación.
 *
 * @author Antonio Gómez <i52goloa@uco.es>
 */
class CategoriasGenericasController extends SisToFaController {

    /**
     * Acción para mostrar el formulario de edición de una categoria generica o el 
     * formulario en blanco para añadir los datos de otra categoria.
     *
     * @Route("/aplicacion/{slug}/crear", name="categoriasgenericas_crear", requirements={"slug"="categoriasgenericas"}, defaults={"slug" = "categoriasgenericas"})
     * @Route("/aplicacion/{slug}/editar/{id}", name="categoriasgenericas_editar", requirements={"slug"="categoriasgenericas", "id"="\d+"}, defaults={"slug" = "categoriasgenericas"})
     * 
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador de la categoria que se va a editar, nulo si es nuevo.
     * @return Response Respuesta que se envia al navegador del usuario 
     * 
     */
    public function editarAction(Request $request, string $slug = "categoriasgenericas",  int $id = null, int $soloconsulta = 0): Response {

        // Obtiene la definición de la pantalla
        $listado = $this->getDoctrine()
                ->getRepository(AppListados::class)
                ->findBySlug($slug);

        if(!$this->tieneAcceso($listado)){
            return $this->volverInicio("No tiene acceso a esta opción.");
        }
        
        $disabled = false;
                
        $datos = new AppCategoriasGenericas();

        //Si hay identificador se busca el usuario
        if ($id != null) {
            $datos = $this->getDoctrine()
                    ->getRepository(AppCategoriasGenericas::class)
                    ->find($id);

            if (empty($datos)) {
                $this->volverInicio("Categoria \"" . $slug . "\" no encontrada.");
            }
        }

        //Crea el formulario de creación y edición.
        $builder = $this->createFormBuilder()->setAction($this->generateUrl($slug . '_guardar', array(
                    'id' => $id
        )));

        // Se añaden los campos del formulario
        $builder->add('nombre', TextType::class, array(
            'required' => true,
            'label' => 'Nombre*',
            'data' => $datos->getNombre(),
            'disabled' => $disabled
        ));

        $categoriaSuperior = null;
        if ($datos->getTipo() != "") {
            $categoriaSuperior = $this->getDoctrine()->getRepository(AppCategoriasGenericas::class)->find($datos->getTipo());
        }

        $builder->add('tipo', EntityType::class, array(
            'required' => false,
            'label' => 'Categoria Superior',
            'class' => AppCategoriasGenericas::class,
            'query_builder' => function (EntityRepository $er) {
                return $er->findCategoriasSuperiores();
            },
            'choice_label' => 'nombre',
            'mapped' => false,
            'data' => $categoriaSuperior,
            'disabled' => $disabled
        ));

        $builder->add('valor', TextType::class, array(
            'required' => false,
            'label' => 'Valor',
            'data' => $datos->getValor(),
            'disabled' => $disabled
        ));

        $builder->add('codigo', TextType::class, array(
            'required' => false,
            'label' => 'Código',
            'data' => $datos->getCodigo(),
            'disabled' => $disabled
        ));

        $builder->add('descripcion', TextareaType::class, array(
            'required' => false,
            'label' => 'Descripcion',
            'data' => $datos->getDescripcion(),
            'disabled' => $disabled
        ));

        $builder->add('orden', TextType::class, array(
            'required' => false,
            'label' => 'Orden',
            'data' => $datos->getOrden(),
            'disabled' => $disabled
        ));


        $builder->add('activado', ChoiceType::class, array(
            'required' => true,
            'label' => 'Estado de la categoria',
            'data' => $datos->getActivado(),
            'choices' => array(
                "Activado" => "1",
                "BLOQUEADO" => "2"
            ),
            'disabled' => $disabled
        ));

        $builder->add('nivel', ChoiceType::class, array(
            'required' => true,
            'label' => 'Nivel',
            'data' => $datos->getNivel(),
            'choices' => array(
                "Administrador" => "10",
                "Administrador de grupo" => "1",
                "Usuario" => "0"
            ),
            'disabled' => $disabled
        ));



        // El botón de guardar solo se mostrará si tiene permisos de escritura
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $builder->add('Guardar', SubmitType::class);
            $builder->add('Clonar', SubmitType::class);
            $builder->add('Eliminar', SubmitType::class);
        }

        // El formulario de creación/edición es enviado
        $form = $builder->getForm();

        // Si el formulario ha sido enviado y se ha producido un error recuerda los datos introducidos
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
        }

        // Define la cabecera del listado.
        $cabecera = new CabeceraListado($datos->getNombre(), $listado->getImagens());
        
        $cabecera->setPaginaedicion($listado->getPaginaedicions());
        $cabecera->setCrear(true);
        $cabecera->setRutavuelta("listado_" . $slug);

        return $this->render('administracion/categoriasgenericas_editar.html.twig', array(
                    'formulario' => $form->createView(),
                    'datos' => $datos,
                    'cabecera' => $cabecera,
                    'listado' => $listado,
                    'tienePermisos' => true
        ));
    }

    /**
     * Guarda, eliminar o clona los datos del formulario de creación/edición de categorias y tipos.
     *
     * @Route("/administracion/{slug}/guardar/{id}", name="categoriasgenericas_guardar", requirements={"slug"="categoriasgenericas", "id"="\d+"}, defaults={"slug" = "categoriasgenericas"})
     * 
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador del usuario que se va a editar, nulo si es nuevo.
     * @return Response Respuesta que se envia al navegador del usuario
     */
    public function guardarAction(Request $request, $slug = "categoriasgenericas", $id = null) {

        // Obtiene la definición de la pantalla
        $listado = $this->getDoctrine()
                ->getRepository(AppListados::class)
                ->findBySlug($slug);

        if(!$this->tieneAcceso($listado)){
            return $this->volverInicio("No tiene acceso a esta opción.");
        }

            if ($request->request->get('form')) { // Se ha enviado el formulario
                $datosform = $request->request->get('form');

                $datos = new AppCategoriasGenericas();

                if ($id) { // Si $id está definido, actualiza el elemento
                    $datos = $this->getDoctrine()->getRepository(AppCategoriasGenericas::class)->find($id);
                    //$datos = $this->getRepository(Usuarios::class)->find($id);
                }

                $datos->setActivado($datosform["activado"])
                        ->setCodigo($datosform["codigo"])
                        ->setDescripcion($datosform["descripcion"])
                        ->setNivel($datosform["nivel"])
                        ->setNombre($datosform["nombre"])
                        ->setOrden($datosform["orden"])
                        ->setValor($datosform["valor"])
                        ->setTipo(($datosform["tipo"] == "" ? null : $datosform["tipo"]));

                try {
                    if (array_key_exists("Eliminar", $datosform)) {
                        $this->emInstance->remove($datos);
                    } else {
                        if (array_key_exists("Clonar", $datosform)) {
                            
                            $clon = new AppCategoriasGenericas();
                            
                            //Clona la categoria pero siempre desactivada
                            $clon->setActivado(0)
                                    ->setCodigo($datosform["codigo"] . " ( COPIA )")
                                    ->setDescripcion($datosform["descripcion"])
                                    ->setNivel($datosform["nivel"])
                                    ->setNombre($datosform["nombre"] . " ( COPIA )")
                                    ->setOrden($datosform["orden"])
                                    ->setValor($datosform["valor"])
                                    ->setTipo(($datosform["tipo"] == "" ? null : $datosform["tipo"]));
                            
                                    $this->emInstance->persist($clon);
                                    $this->emInstance->flush();
                                    return $this->redirectToRoute($slug . '_editar', array(
                                    'id' => $clon->getIdcategoria()));
                                    
                        } else {
                            $this->emInstance->persist($datos);
                        }
                    }

                    $this->emInstance->flush();
                    $this->addFlash('correcto', 'Los datos se han modificado correctamente.');

                    if (array_key_exists("Eliminar", $datosform)) {
                        return $this->redirectToRoute("listado_" . $slug, array(), 307);
                    } else {
                        return $this->redirectToRoute($slug . '_editar', array(
                                    'id' => $datos->getIdcategoria()));
                    }
                } catch (\Exception $e) {
                    
                    $this->addFlash('error', 'Ha ocurrido un error al modificar los datos.');

                    return $this->redirectToRoute('listado_' . $slug );
                }
            } else { // No se ha enviado el formulario
                $this->addFlash('error', 'Ha ocurrido un error al enviar el formulario.');
                return $this->redirectToRoute('listado_' . $slug);
            }
        
    }

}
