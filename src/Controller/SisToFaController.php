<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Services\Funciones;
use Doctrine\ORM\EntityManagerInterface;
use App\Services\cDatos;
use App\Entity\cMenu;
use App\Entity\Sistemas;
use App\Entity\AppListados;

/**
 * Controlador Base del aplicación. Incluye los metodos para obtener la sesión y 
 * consultar si el usuario conectado tiene permisos para acceder a determinadar
 * opciones.
 *
 * @author Antonio Gómez <i52goloa@uco.es>
 */
class SisToFaController extends AbstractController{
  
    /**
     * @var Session $session Sesión actual del usuario en el sistema.
     */
    protected $session;
    
    /**
     * 
     * @var EntityManagerInterface $emInstance Objeto con acceso al Gestor de entidades de doctrine
     */
    protected $emInstance;
   
    /**
     * Construcctor de la clase Controllador de la cual heredan el resto de controladores.
     * 
     * @param EntityManagerInterface $em Obtiene por inyección el Gestor de entidades de doctrine
     */
    function __construct(EntityManagerInterface $em) {
        $this->emInstance = $em;
        
    }

    /**
     * Obtiene el Id del usuario actualmente conectado al sistema.
     * 
     * @return Int
     */
    public function getId() {
        
        $user = $this->getUser();
        
        return $user->getId();
    }
    
    public function getNivel() {
        
        $user = $this->getUser();
        
        return $user->getNivel();
    }
    
    public function getGrupo() {
        
        $user = $this->getUser();
        
        return $user->getGrupo();
    }
    
    public function getIdGrupo() {
        
        $user = $this->getUser();
        
        return $user->getGrupo()->getIdgrupo();
    }
    
    public function getRepository(string $class)
    { 
       $this->getDoctrine()->getRepository($class);
    }
        
    public function getSession()
    {
        if ( !isset($this->session) )
        {
            $this->session  =  new Session();
        }
        else
        {
            return $this->session;
        }
    }

    
    /**
     * Vuelve al inicio con un mensaje de error
     * 
     * @param String $error  mensaje de error.
     * 
     * @return Redirect
     * 
     */
    public function volverInicio(String $error) {
        
           $this->addFlash('error', $error);
           return $this->redirectToRoute('inicio');
    }
    
    /**
     * Guarda una información en la sesión del usuario
     * 
     * @param String $error  mensaje de error.
     * 
     * @return Redirect
     * 
     */
    public function guardarEnSesion(String $dato, String $valor)
    {
        $this->getSession();
        $this->_session->set($dato, $valor);

        $response = new JsonResponse();
        $response->setData(true);
        return $response;
    }
    
    /**
     * Guarda en sesión un dato con el valor indicado
     *
     * @Route("/interno/guardar_en_sesion", name="guardar_en_sesion")
     */
    public function guardarEnSesionAction(Request $request)
    {
        $dato = $request;
        
        $this->getSession();
        $this->_session->set($dato, $valor);

        $response = new JsonResponse();
        $response->setData(true);
        return $response;
    }
    
    /**
     * Comprueba si el usuario actualmente conectado tiene acceso a la opcion de
     * menu a la que se pretende.
     * Solo se comprueba en las opciones del menu que tengan un nivel superior a 0.
     * 
     * @param AppListados $listado Opcion del menu a la que se pretende acceder
     * 
     */
    public function tieneAcceso(AppListados $listado):bool {
        
        $usuario = $this->getUser();

        if($usuario->getNivel()< $listado->getNivel()) {
            return false;
        }
        else {
            return true;
        }
    }
    
    /**
     * Atributo Disabled para los campos del formulario.
     * 
     * @param string $permisos texto con el permiso del usuario
     * 
     * @return boolean Devuelve true si no tiene permisos de escritura
    */
    public function formDisabled ($permisos)
    {
        if ($permisos == 'lectura') {
            return true;
        } else {
            return false;
        }
    }
    
}