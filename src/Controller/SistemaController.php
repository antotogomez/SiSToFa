<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use App\Controller\SimSitfaController;
use App\Entity\CabeceraListado;
use App\Entity\AppListados;
use App\Entity\Sistemas;
use App\Entity\Usuarios;
use App\Entity\Componentes;
use App\Entity\Entradas;
use App\Entity\AppCategoriasGenericas;
use App\Entity\Salidas;
use Exception;

use App\Services\Funciones;

/**
 * Controlador para la creación y edición de sistemas.
 *
 * @author Antonio Gómez <i52goloa@uco.es>
 * 
 */
class SistemaController extends SisToFaController {

    /**
     * Muestra el formulario de creación/edición de los datos de sistemas. Si se indica
     * el identificador de un sistema, se obtienen de la base de datos la información y
     * se muestra en el formulario.
     *
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador del sistema que se va a editar, nulo si es nuevo.
     * 
     * @return Response Respuesta HTTP que se envia al navegador del cliente.
     * 
     * @Route("/sistemas/editar/{id}", name="sistemas_editar",requirements={"slug"="sistemas", "id"="\d+"}, defaults={"slug" = "sistemas"})
     * @Route("/sistemas/crear", name="sistemas_crear",requirements={"slug"="sistemas"}, defaults={"slug" = "sistemas"})
     */
    public function editarAction(Request $request, $slug = "sistemas", $id = null): Response {

        // Obtiene listado
        $listado = $this->getDoctrine()
                ->getRepository(AppListados::class)
                ->findBySlug($slug);

        $disabled = false;

        $datos = new Sistemas();
        
        if ($id != "") {
            // Obtiene información del sistema que se va a editar.

            $datos = $this->getDoctrine()
                    ->getRepository(Sistemas::class)
                    ->find($id);

            if (is_null($datos)) {
                $this->volverInicio("Listado \"" . $slug . "\" no encontrado.");
            }
        } else {
            //Si es un sistema nuevo, establece el usuario actual como el propietario del sistema
            $datos->setUsuarios($this->getUser());
        }

        //Comprueba el nivel de acceso a este sistema por parte del usuario actualmente conectado
        if($datos->getUsuarios()->getId() == $this->getId()) {
            //Es el propietario, puede modificar el sistema
            $disabled = false;
        }
        else {
            $visiblidad_publica = $this->getDoctrine()->getRepository(AppCategoriasGenericas::class)->findCategoriaCodigo("VISIBILIDAD_PUBLICO")->getIdcategoria();
            $visiblidad_grupo = $this->getDoctrine()->getRepository(AppCategoriasGenericas::class)->findCategoriaCodigo("VISIBILIDAD_GRUPO")->getIdcategoria();
            $grupo_propietario = $datos->getUsuarios()->getGruposIdgrupo();
            
            if($datos->getVisibilidad() == $visiblidad_publica || ($datos->getVisibilidad() == $visiblidad_grupo && $this->getIdGrupo() == $grupo_propietario) ) {
                $disabled=true;
            }
            else {
                  return $this->redirectToRoute('listado_sistemas', array(
                        'error' => "No se ha encontrado el listado"
            ));
            }
        }
        
        $builder = $this->createFormBuilder()->setAction($this->generateUrl($slug . '_guardar',
                        array('id' => $id)
        ));

        // Se añaden los campos del formulario
        $builder->add('nombre', TextType::class, array(
            'required' => true,
            'label' => 'Nombre*',
            'data' => $datos->getNombre(),
            'disabled' => $disabled
        ));

        // Se añaden los campos del formulario
        $builder->add('propietario', TextType::class, array(
            'required' => true,
            'label' => 'Propietario*',
            'data' => $datos->getUsuarios()->getNombre() . " " . $datos->getUsuarios()->getApellidos(),
            'disabled' => true
        ));

        // Se añaden los campos del formulario
        $builder->add('idpropietario', HiddenType::class, array(
            'required' => true,
            'data' => $datos->getUsuarios()->getId(),
            'disabled' => true
        ));

        $builder->add('descripcion', TextareaType::class, array(
            'required' => false,
            'label' => 'Descripcion:',
            'data' => $datos->getDescripcion(),
            'disabled' => $disabled
        ));

        $visibilidad = null;
        if ($datos->getVisibilidad() != "") {
            $visibilidad = $this->getDoctrine()->getRepository(AppCategoriasGenericas::class)->find($datos->getVisibilidad());
        }

        $builder->add('visibilidad', EntityType::class, array(
            'label' => 'Visibilidad del Sistema:',
            'class' => AppCategoriasGenericas::class,
            'query_builder' => function (EntityRepository $er) {
                return $er->findCategoriasTipoQuery('VISIBILIDAD');
            },
            'choice_label' => 'nombre',
            'mapped' => false,
            'data' => $visibilidad,
            'disabled' => $disabled
        ));

            if(!$disabled) {
                $builder->add('Guardar', SubmitType::class);
                $builder->add('Eliminar', SubmitType::class);
            }
            
            $builder->add('Clonar', SubmitType::class);
                  

        // El formulario de creación/edición es enviado
        $form = $builder->getForm();

        // Si el formulario ha sido enviado y se ha producido un error recuerda los datos introducidos
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
        }

        // Define la cabecera del listado.
        $cabecera = new CabeceraListado("Sistema: " . $datos->getNombre(), $listado->getImagens());
        $cabecera->setPaginaedicion($listado->getPaginaedicions());
        $cabecera->setNolistado(true);
        $cabecera->setCrear(true);
        $cabecera->setRutavuelta("listado_" . $slug);
       
        return $this->render('sistema/sistemas_editar.html.twig', [
                    'formulario' => $form->createView(),
                    'datos' => $datos,
                    'cabecera' => $cabecera,
                    'listado' => $listado,
                    'componentes' => $datos->getComponentes(),
                    'datosHistorial' => $datos->getHistorial(),
                    'tienePermisos' => !$disabled,
                    'error' => ""
        ]);
    }

    /**
     * Muestra el sistema en modo grafico, para ello se debe indicar el id del este sistema
     * para poder obtener la información necesaria de la base de datos.
     *
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador del sistema que se va a editar, nulo si es nuevo.
     * 
     * @return Response Respuesta HTTP que se envia la navegador del cliente
     * 
     * @Route("/sistemas/visualizar/{id}", name="sistemas_visualizar",requirements={"slug"="sistemas", "id"="\d+"}, defaults={"slug" = "sistemas"})
     */
    public function visualizarAction(Request $request, $slug = "sistemas", $id = null): Response {

       
        // Obtiene listado
        $listado = $this->getDoctrine()
                ->getRepository(AppListados::class)
                ->findBySlug($slug);
        
        $disabled = false;
        $tienePermisos = false;

        $sistema = new Sistemas();

        if ($id != "") {
            // Obtiene información del sistema que se va a editar.

            $sistema = $this->getDoctrine()
                    ->getRepository(Sistemas::class)
                    ->find($id);

            Funciones::logDebug("[" . basename(__FILE__) . ":" . __LINE__ . "]" . $sistema->getNombre(). "  _________________________________________________________");
                          
            if (is_null($sistema)) {
                $this->volverInicio("Listado \"" . $slug . "\" no encontrado.");
            }
        } else {
            //Si es un sistema nuevo, establece el usuario actual como el propietario del sistema
            $sistema->setUsuarios($this->getUser());
        }

         //Comprueba los permisos
        if ($sistema->getUsuarios()->getId() != $this->getId()) { // No tiene permisos de escritura
           $tienePermisos=true;
        }
                
        // Define la cabecera del listado.
        $cabecera = new CabeceraListado("Ver sistemas:" . $sistema->getNombre(), $listado->getImagens());
        $cabecera->setPaginaedicion($listado->getPaginaedicions());
        $cabecera->setCrear(true);
        $cabecera->setNolistado(true);
        $cabecera->setBotones(array(array("boton" => "Editar", "enlace" => "/SiSToFa/sistemas/editar/" . $id)));
        $cabecera->setRutavuelta("listado_" . $slug);

        $tiposerror = $this->getDoctrine()
                ->getRepository(AppCategoriasGenericas::class)
                ->findCategoriasTipo('ERRORES_COMPONENTES');

        try {

            $sistema->calcular(true);
            $sistema->calculaFiabilidad();

            //Lo guarda en memoria con los datos recien calculador y obtenidos de la base
            // de datos.
            $this->get('session')->set('sistema_' . $id, $sistema);
        } catch (\Exception $ex) {
            $this->addFlash('error', 'Ha ocurrido un error al calcular los resultados:<br><i>' . $ex->getMessage() . '</i>');
        }

        return $this->render('sistema/sistemas_grafico.html.twig', [
                    'cabecera' => $cabecera,
                    'listado' => $listado,
                    'sistema' => $sistema,
                    'simulacion' => null,
                    'tienePermisos' => !$tienePermisos,
                    'error' => "",
                    'tiposerror' => $tiposerror
        ]);
    }

    /**
     * Muestra el sistema en modo grafico en la pantalla para simularlo.
     * 
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador del sistema que se va a editar, nulo si es nuevo.
     * 
     * @return Response respuesta HTTP que se envia al navegador del cliente
     * 
     * @Route("/sistemas/simular/{id}", name="sistemas_simular",requirements={"slug"="sistemas", "id"="\d+"}, defaults={"slug" = "sistemas"})
     */
    public function simularAction(Request $request, $slug = "sistemas", $id = null): Response {
        
        $tienePermisos = false;
        
        // Obtiene listado
        $listado = $this->getDoctrine()
                ->getRepository(AppListados::class)
                ->findBySlug($slug);

        $datos = new Sistemas();

        if ($id != "") {
            // Obtiene información del sistema que se va a editar.

            $datos = $this->getDoctrine()
                    ->getRepository(Sistemas::class)
                    ->find($id);

            if (is_null($datos)) {
                $this->volverInicio("Listado \"" . $slug . "\" no encontrado.");
            }
        } else {
            //Si es un sistema nuevo, establece el usuario actual como el propietario del sistema
            $datos->setUsuarios($this->getUser());
        }

        // Define la cabecera del listado.
        $cabecera = new CabeceraListado("Ejecucion estática: \"" . $datos->getNombre() . "\"", $listado->getImagens(), true);
        $cabecera->setPaginaedicion($listado->getPaginaedicions());
        $cabecera->setCrear(true);
        $cabecera->setBotones(array(array("boton" => "Editar", "enlace" => "/SiSToFa/sistemas/editar/" . $id)));
        $cabecera->setRutavuelta("listado_" . $slug);

        $datos->calcular(true);
        $datos->calculaFiabilidad();
        

        //Comprueba los permisos
        if ($datos->getUsuarios()->getId() != $this->getId()) { // No tiene permisos de escritura
           $tienePermisos=true;
        }
        return $this->render('sistema/sistemas_grafico.html.twig', [
                    'cabecera' => $cabecera,
                    'listado' => $listado,
                    'sistema' => $datos,
                    'simulacion' => null,
                    'tienePermisos' => !$tienePermisos,
                    'error' => ""
        ]);
    }

    /**
     * Guarda los datos del formulario de creación/edición de un sistemas.
     *
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador del sistema que se va a editar, nulo si es nuevo.
     * 
     * @return Response Respuesta http que se envia al navegador del cliente
     * 
     * @Route("/sistemas/guardar/{id}", name="sistemas_guardar",requirements={"slug"="sistemas", "id"="\d+"}, defaults={"slug" = "sistemas"})
     *
     */
    public function guardarAction(Request $request, $slug = "sistemas", $id = null) {

        // Comprueba los permisos, solo puede escribir si es el propietario del sistema.
            if ($request->request->get('form')) { // Se ha enviado el formulario
                
                $datosform = $request->request->get('form');

                $datos = new Sistemas();

                if ($id) { // Si $id está definido, actualiza el elemento
                    $datos = $this->getDoctrine()->getRepository(Sistemas::class)->find($id);
                } else { //Si no, es nuevo y se establece su propietario como el usaurio actual.
                    $datos->setUsuarios($this->getDoctrine()->getRepository(Usuarios::class)->find($this->getUser()->getId()));
                }
               
                try {
                    if (isset($datosform["Eliminar"])) {
                        //Comprueba los permisos
                        if ($datos->getUsuarios()->getId() != $this->getId()) { // No tiene permisos de escritura
                            $this->addFlash('error', 'Ha ocurrido un error al guardar los datos:<br>No tiene permisos de escritura sobre el objeto.');
                            return $this->redirectToRoute($slug . '_editar', array('id' => $id));
                        }
                        //Tiene que eliminar los componentes con sus entradas y salidas
                        foreach ($datos->getComponentes() as $componente) {
                            foreach ($componente->getSalidas() as $salida) {
                                $this->emInstance->remove($salida);
                            }
                            foreach ($componente->getEntradas() as $entrada) {
                                $this->emInstance->remove($entrada);
                            }
                            $this->emInstance->remove($componente);
                        }
                        $this->emInstance->remove($datos);
                        $this->emInstance->flush();
                        $this->addFlash('correcto', 'Los datos se han eliminado correctamente.');
                        return $this->redirectToRoute('listado_' . $slug);
                    } else {

                        if (isset($datosform["Clonar"])) {

                            $nuevos = new Sistemas();

                            $nuevos->setNombre($datos->getNombre() . " (COPIA)")
                                    ->setDescripcion($datos->getDescripcion())
                                    ->setVisibilidad($datos->getVisibilidad());

                            
                            $nuevos->setComponentes(array());
                            
                            $nuevos->setUsuarios($this->getDoctrine()->getRepository(Usuarios::class)->find($this->getUser()->getId()));

                            $this->emInstance->persist($nuevos);
                            $this->emInstance->flush();

                            foreach ($datos->getComponentes() as $componente) {

                                $nuevocomponente = new Componentes();
                                $nuevocomponente->setColor($componente->getColor());
                                $nuevocomponente->setColortexto($componente->getColortexto());
                                $nuevocomponente->setConfiguracion($componente->getConfiguracion());
                                $nuevocomponente->setDescripcion($componente->getDescripcion());
                                $nuevocomponente->setEntradas(array());
                                $nuevocomponente->setSalidas(array());
                                $nuevocomponente->setFiabilidad($componente->getFiabilidad());
                                $nuevocomponente->setNombre($componente->getNombre());
                                $nuevocomponente->setOperacionfallo($componente->getOperacionfallo());
                                $nuevocomponente->setSistema($nuevos);

                                $nuevos->getComponentes()->add($nuevocomponente);
                                $this->emInstance->persist($nuevocomponente);
                                $this->emInstance->flush();

                                foreach ($componente->getSalidas() as $salida) {
                                    $nuevasalida = new Salidas();
                                    $nuevasalida->setColor($salida->getColor());
                                    $nuevasalida->setColortexto($salida->getColortexto());
                                    $nuevasalida->setComponente($nuevocomponente);
                                    $nuevasalida->setNombre($salida->getNombre());
                                    $nuevasalida->setDescripcion($salida->getDescripcion());
                                    $nuevasalida->setOperacion($salida->getOperacion());
                                    $nuevasalida->setOperacionsalida($salida->getOperacionsalida());

                                    $nuevocomponente->getSalidas()->add($nuevasalida);
                                    $this->emInstance->persist($nuevasalida);
                                    $this->emInstance->flush();
                                }

                                foreach ($componente->getEntradas() as $entrada) {
                                    $nuevaentrada = new Entradas();
                                    $nuevaentrada->setColor($entrada->getColor());
                                    $nuevaentrada->setColortexto($entrada->getColortexto());
                                    $nuevaentrada->setComponente($nuevocomponente);
                                    $nuevaentrada->setDescripcion($entrada->getDescripcion());
                                    $nuevaentrada->setNombre($entrada->getNombre());
                                    $nuevaentrada->setValorInicial($entrada->getValorInicial());

                                    $nuevocomponente->getEntradas()->add($nuevaentrada);
                                    $this->emInstance->persist($nuevaentrada);
                                }
                            }

                            //Ya estan todos los componentes con sus entradas y salidas, ahora hay que conectarlas.
                            // Se da otra vuelta.
                            foreach ($datos->getComponentes() as $componente) {
                                foreach ($componente->getEntradas() as $entrada) {
                                    $salida = $entrada->getSalida();
                                    if ($salida != null) {
                                        echo $entrada->getComponente()->getNombre() . "::" . $entrada->getNombre();
                                        echo "->" . $salida->getComponente()->getNombre() . "::" . $salida->getNombre() . "<br>";

                                        $nuevasalida = $nuevos->getComponenteNombre($salida->getComponente()->getNombre())->getSalidaNombre($salida->getNombre());
                                        $nuevaentrada = $nuevos->getComponenteNombre($entrada->getComponente()->getNombre())->getEntradaNombre($entrada->getNombre());

                                        $nuevaentrada->setSalida($nuevasalida);
                                        //var_dump($nuevaentrada);
                                        $this->emInstance->persist($nuevaentrada);
                                    }
                                }
                            }

                            $this->emInstance->flush();
                            $this->addFlash('correcto', 'Los datos se han clonado correctamente.');
                            return $this->redirectToRoute($slug . '_editar', array(
                                        'id' => $nuevos->getIdsistema()));
                        } else {
                            //Comprueba los permisos
                            if ($datos->getUsuarios()->getId() != $this->getId()) { // No tiene permisos de escritura
                                $this->addFlash('error', 'Ha ocurrido un error al guardar los datos:<br>No tiene permisos de escritura sobre el objeto.');
                                return $this->redirectToRoute($slug . '_editar', array('id' => $id));
                            }
                             $datos->setNombre($datosform['nombre'])
                                ->setDescripcion($datosform['descripcion'])
                                ->setVisibilidad($datosform["visibilidad"]);
                            $this->emInstance->persist($datos);
                            $this->emInstance->flush();

                            $this->addFlash('correcto', 'Los cambios han sido guardados correctamente.');

                            return $this->redirectToRoute($slug . '_editar', array(
                                        'id' => $datos->getIdsistema()));
                        }
                    }
                } catch (\Exception $e) {
                    $this->addFlash('error', 'Ha ocurrido un error al guardar los cambios.');
                    return $this->redirectToRoute('listado_' . $slug );
                }
            } else { 
                // No se ha enviado el formulario
                $this->addFlash('error', 'Ha ocurrido un error al enviar el formulario.');
                return $this->redirectToRoute('listado_' . $slug);
            }
     
    }

    /**
     * Guarda los datos del formulario de creación/edición de un componente de un sistema.
     *
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador del sistema que se va a editar, nulo si es nuevo.
     * 
     * @return Response Respuesta http que se envia al navegador del usuario
     * 
     * @Route("/sistemas/guardarcomponente/{id}", name="sistemas_guardarcomponente",requirements={"slug"="sistemas", "id"="\d+"}, defaults={"slug" = "sistemas"})
     */
    public function guardarComponenteAction(Request $request, $slug = "sistemas", $id = null) {

        $sistema = $this->getDoctrine()->getRepository(Sistemas::class)->find($id);
        //Comprueba los permisos
        if ($sistema->getUsuarios()->getId() != $this->getId()) { // No tiene permisos de escritura
            $this->addFlash('error', 'Ha ocurrido un error al guardar los datos:<br>No tiene permisos de escritura sobre el objeto.');
            return $this->redirectToRoute($slug . '_editar', array('id' => $id));
        }
        else {

            $url_vuelta = "";

            if (str_contains($request->server->get("HTTP_REFERER"), "visualizar")) {
                $url_vuelta = $slug . "_visualizar";
            } else {
                $url_vuelta = $slug . "_editar";
            }

            if ($request->request->get('form')) { // Se ha enviado el formulario
                $datosform = $request->request->get('form');

                $datos = new Componentes();

                if ($datosform['componente_id']) { // Si $id está definido, actualiza el elemento
                    $datos = $this->getDoctrine()->getRepository(Componentes::class)->find($datosform['componente_id']);
                } else { //Si no, es nuevo y se establece su propietario como el usaurio actual.
                }

                try {
                    if (isset($datosform['componente_eliminar'])) {
                        //Tiene que eliminas las entradas y salidas.
                        foreach ($datos->getSalidas() as $salida) {
                                $this->emInstance->remove($salida);
                        }
                        
                        foreach ($datos->getEntradas() as $entrada) {
                            $this->emInstance->remove($entrada);
                        }
                            
                        $this->emInstance->remove($datos);
                        $this->emInstance->flush();
                        $this->addFlash('correcto', 'Los cambios han sido ELIMINADOS correctamente.');
                    }

                    if (isset($datosform['componente_clonar'])) {

                        $sistema = $this->getDoctrine()->getRepository(Sistemas::class)->find($id);

                        if ($sistema->existeNombreComponente($datosform['componente_nombre'] . " (COPIA)", intval($datosform['componente_id']))) {
                            throw new Exception('Ya existe un componente con ese nombre.');
                        }
                            $nuevo = new Componentes();
                            
                            $nuevo->setNombre($datosform['componente_nombre'] . " (COPIA)")
                                ->setDescripcion($datosform['componente_descripcion'])
                                ->setConfiguracion($datosform["componente_configuracion"])
                                ->setFiabilidad($datosform["componente_fiabilidad"])
                                ->setColor($datosform["componente_color"])
                                ->setColortexto($datosform["componente_colortexto"])
                                ->setSistema($sistema);
                        
                            if($datosform["componente_ndekminimo"]!= "") {
                                $datos->setNdekminimo($datosform["componente_ndekminimo"]);
                            }
                            else {
                                $datos->setNdekminimo(null);
                            }

                            $this->emInstance->persist($nuevo);
                            $this->emInstance->flush();
                            
                            //Ahora le añade las entrada y salidas.
                            foreach($datos->getEntradas() as $entrada){
                                $nueva = new Entradas();
                                $nueva->setColor($entrada->getColor());
                                $nueva->setColortexto($entrada->getColortexto());
                                $nueva->setComponente($nuevo);
                                $nueva->setDescripcion($entrada->getDescripcion());
                                $nueva->setNombre($entrada->getNombre());
                                $nueva->setSalida($entrada->getSalida());
                                $nueva->setValorInicial($entrada->getValorInicial());
                                $this->emInstance->persist($nueva);
                                $this->emInstance->flush();                           
                            }
                            
                            //Ahora le añade las entrada y salidas.
                            foreach($datos->getSalidas() as $salidas){
                                $nueva = new Salidas();
                                $nueva->setColor($salidas->getColor());
                                $nueva->setColortexto($salidas->getColortexto());
                                $nueva->setComponente($nuevo);
                                $nueva->setDescripcion($salidas->getDescripcion());
                                $nueva->setNombre($salidas->getNombre());
                                $nueva->setOperacion($salidas->getOperacion());
                                $nueva->setOperacionsalida($salidas->getOperacionsalida());
                                
                                $this->emInstance->persist($nueva);
                                $this->emInstance->flush();                           
                            }
                            
                        
                        $this->addFlash('correcto', 'Los cambios han sido guardados correctamente.');
                    }
                    
                    if (isset($datosform['componente_guardar'])) {

                        $sistema = $this->getDoctrine()->getRepository(Sistemas::class)->find($id);

                        if ($sistema->existeNombreComponente($datosform['componente_nombre'], intval($datosform['componente_id']))) {
                            throw new Exception('Ya existe un componente con ese nombre.');
                        }

                        $datos->setNombre($datosform['componente_nombre'])
                                ->setDescripcion($datosform['componente_descripcion'])
                                ->setConfiguracion($datosform["componente_configuracion"])
                                ->setFiabilidad($datosform["componente_fiabilidad"])
                                ->setColor($datosform["componente_color"])
                                ->setColortexto($datosform["componente_colortexto"])
                                ->setSistema($sistema);
                        
                        if($datosform["componente_ndekminimo"]!= "") {
                            $datos->setNdekminimo($datosform["componente_ndekminimo"]);
                        }
                        else {
                            $datos->setNdekminimo(null);
                        }

                        $this->emInstance->persist($datos);
                        $this->emInstance->flush();
                        $this->addFlash('correcto', 'Los cambios han sido guardados correctamente.');
                    }

                    return $this->redirectToRoute($url_vuelta, array(
                                'id' => $id));
                } catch (\Exception $e) {
                    $this->addFlash('error', 'Ha ocurrido un error al guardar los cambios.');

                   return $this->redirectToRoute('listado_' . $slug );
                }
            } else { // No se ha enviado el formulario
                $this->addFlash('error', 'Ha ocurrido un error al enviar el formulario.');
                return $this->redirectToRoute('listado_' . $slug);
            }
        }
    }

    /**
     * Guarda los datos del formulario de creación/edición de una entrada de un componente de un sistema.
     *
     * @Route("/sistemas/guardarentrada/{id}", name="sistemas_guardarentrada",requirements={"slug"="sistemas", "id"="\d+"}, defaults={"slug" = "sistemas"})
     *
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador del sistema que se va a editar, nulo si es nuevo.
     * 
     * @return Response Respuesta http que se envia al navegador del cliente
     */
    public function guardarEntradaAction(Request $request, $slug = "sistemas", $id = null) {

        $url_vuelta = "";

        if (str_contains($request->server->get("HTTP_REFERER"), "visualizar")) {
            $url_vuelta = $slug . "_visualizar";
        } else {
            $url_vuelta = $slug . "_editar";
        }

        $sistema = $this->getDoctrine()->getRepository(Sistemas::class)->find($id);
        //Comprueba los permisos
        if ($sistema->getUsuarios()->getId() != $this->getId()) { // No tiene permisos de escritura
            $this->addFlash('error', 'Ha ocurrido un error al guardar los datos:<br>No tiene permisos de escritura sobre el objeto.');
            return $this->redirectToRoute($slug . '_editar', array('id' => $id));
        }
        else {

            if ($request->request->get('form')) { // Se ha enviado el formulario
                $datosform = $request->request->get('form');

                //Analiza la operaciona realizar.
                $componente = $this->getDoctrine()->getRepository(Componentes::class)->find($datosform["entrada_componente_id"]);
                $datos = new Entradas();

                if ($datosform['entrada_id']) { // Si $id está definido, actualiza el elemento
                    $datos = $this->getDoctrine()->getRepository(Entradas::class)->find($datosform['entrada_id']);
                }

                $datos->setNombre($datosform['entrada_nombre'])
                        ->setValorInicial($datosform['entrada_valorinicial'])
                        ->setColor($datosform["entrada_color"])
                        ->setColortexto($datosform["entrada_colortexto"])
                        ->setDescripcion($datosform["entrada_descripcion"]);

                //Comprueba si se ha establecido el valos de la salida a la que esta conectada
                if ($datosform["entrada_conexionsalida"] != "null") {
                    $salida = $this->getDoctrine()->getRepository(Salidas::class)->find($datosform["entrada_conexionsalida"]);

                    $salida->getEntradas()->add($datos);
                    $datos->setSalida($salida);
                } else {
                    $datos->setSalida(null);
                }

                //Hay que establecer las dos relaciones para que los datos se guarden correctamente.
                $datos->setComponente($componente);
                $componente->getEntradas()->add($datos);

                try {

                    if (isset($datosform['entrada_eliminar'])) {
                        $this->emInstance->remove($datos);
                        $this->emInstance->flush();
                        $this->addFlash('correcto', 'Los cambios han sido ELIMINADOS correctamente.');
                    }

                    if (isset($datosform['entrada_guardar'])) {
                        $this->emInstance->persist($datos);
                        $this->emInstance->flush();
                        $this->addFlash('correcto', 'Los cambios han sido guardados correctamente.');
                    }

                    return $this->redirectToRoute($url_vuelta, array(
                                'id' => $id));
                } catch (\Exception $e) {
                    $this->addFlash('error', 'Ha ocurrido un error al guardar los cambios.');

                   return $this->redirectToRoute('listado_' . $slug );
                }
            } else { // No se ha enviado el formulario
                $this->addFlash('error', 'Ha ocurrido un error al enviar el formulario.');
                return $this->redirectToRoute('listado_' . $slug);
            }
        }
    }

    /**
     * Guarda los datos del formulario de creación/edición de una salida de un componente de un sistema.
     *
     * @param Request $request Petición que llega desde el cliente
     * @param string $slug Fragmento de la url que identifica el objeto con el que se esta trabajando.
     * @param int $id Identificador del sistema que se va a editar, nulo si es nuevo.
     * 
     * @return Response Respuesta http que se envia al navegador del cliente
     * 
     * @Route("/sistemas/guardarsalida/{id}", name="sistemas_guardarsalida",requirements={"slug"="sistemas", "id"="\d+"}, defaults={"slug" = "sistemas"})
     */
    public function guardarSalidaAction(Request $request, $slug = "sistemas", $id = null) {
       
        $url_vuelta = "";
        $sistema = $this->getDoctrine()->getRepository(Sistemas::class)->find($id);
        //Comprueba los permisos
        if ($sistema->getUsuarios()->getId() != $this->getId()) { // No tiene permisos de escritura
            $this->addFlash('error', 'Ha ocurrido un error al guardar los datos:<br>No tiene permisos de escritura sobre el objeto.');
            return $this->redirectToRoute($slug . '_editar', array('id' => $id));
        }
                
        if (str_contains($request->server->get("HTTP_REFERER"), "visualizar")) {
            $url_vuelta = $slug . "_visualizar";
        } else {
            $url_vuelta = $slug . "_editar";
        }

            if ($request->request->get('form')) { // Se ha enviado el formulario
                $datosform = $request->request->get('form');

                $componente = $this->getDoctrine()->getRepository(Componentes::class)->find($datosform["salida_componente_id"]);
                $datos = new Salidas();

                if ($datosform['salida_id']) { // Si $id está definido, actualiza el elemento
                    $datos = $this->getDoctrine()->getRepository(Salidas::class)->find($datosform['salida_id']);
                }

                $datos->setNombre($datosform['salida_nombre'])
                        ->setColor($datosform["salida_color"])
                        ->setColortexto($datosform["salida_colortexto"])
                        ->setDescripcion($datosform["salida_descripcion"]);

                if (isset($datosform["salida_operacion"])) {
                    if ($datosform["salida_operacion"] != "null") {
                        $datos->setOperacion($datosform['salida_operacion']);
                        $operacion = $this->getDoctrine()->getRepository(AppCategoriasGenericas::class)->find($datosform["salida_operacion"]);
                        $datos->setOperacionsalida($operacion);
                    }
                }

                //Hay que establecer las dos relaciones para que los datos se guarden correctamente.
                $datos->setComponente($componente);
                $componente->getSalidas()->add($datos);

                try {

                    if (isset($datosform['salida_eliminar'])) {
                        $this->emInstance->remove($datos);
                        $this->emInstance->flush();
                        $this->addFlash('correcto', 'Los cambios han sido ELIMINADOS correctamente.');
                    }

                    if (isset($datosform['salida_guardar'])) {
                        $this->emInstance->persist($datos);
                        $this->emInstance->flush();
                        $this->addFlash('correcto', 'Los cambios han sido guardados correctamente.');
                    }

                    return $this->redirectToRoute($url_vuelta, array(
                                'id' => $id));
                } catch (\Exception $e) {
                    $this->addFlash('error', 'Ha ocurrido un error al guardar los cambios.');
                    return $this->redirectToRoute('listado_' . $slug );
                }
            } else { // No se ha enviado el formulario
                $this->addFlash('error', 'Ha ocurrido un error al enviar el formulario.');
                return $this->redirectToRoute('listado_' . $slug);
            }
        
    }

    /**
     * Devuelve un json con la información del componente indicado.
     *
     * @param Request $request Petición que se envia desde el navegador y que debe incluir el id del componente a buscar.
     * @return json Cadena con estructura json que incluye todos los datos del componente que se ha buscado.
     * 
     * @Route("/sistemas/componentes/info", name="componentes_info")
     */
    public function infoComponenteAction(Request $request) {

        $response = new JsonResponse();
        $id = $request->request->get('id');

        $datos = new Componentes();

        if ($id != null && $id > 0) {

            $datos = $this->getDoctrine()
                    ->getRepository(Componentes::class)
                    ->find($id);

            $response->setData($datos->toArray());
        }

        return $response;
    }

    /**
     * Devuelve un json con la información de la entrada indicada.
     *
     * @param Request $request Petición que se envia desde el navegador y que debe incluir el id de la entrada a buscar.
     * @return json Cadena con estructura json que incluye todos los datos de la entrada del componente que se ha buscado.
     * 
     * @Route("/sistemas/entradas/info", name="entradas_info")
     */
    public function infoEntradasAction(Request $request) {

        $response = new JsonResponse();
        $id = $request->request->get('id');

        $datos = new Entradas();

        if ($id != null && $id > 0) {

            $datos = $this->getDoctrine()
                    ->getRepository(Entradas::class)
                    ->find($id);

            $response->setData($datos->toArray());
        }

        return $response;
    }

    /**
     * Devuelve un json con la información de la salida indicada.
     *
     * @param Request $request Petición que se envia desde el navegador y que debe incluir el id de la salida a buscar.
     * @return json Cadena con estructura json que incluye todos los datos de la salida del componente que se ha buscado.
     * 
     * @Route("/sistemas/salidas/info", name="salidas_info")
     */
    public function infoSalidasAction(Request $request) {

        $response = new JsonResponse();
        $id = $request->request->get('id');

        $datos = new Salidas();

        if ($id != null && $id > 0) {

            $datos = $this->getDoctrine()
                    ->getRepository(Salidas::class)
                    ->find($id);

            $response->setData($datos->toArray());
        }

        return $response;
    }

    /**
     * Devuelve un json con la información de todas las salidas disponibles para conectar a una entrada
     *
     * @param Request $request Petición que se envia desde el navegador y que debe incluir el id de la entrada a buscar.
     * @return json Cadena con estructura json que incluye todos los datos de las salidas de un componente que se ha buscado.
     * 
     * @Route("/sistemas/salidas/lista", name="salidas_lista")
     */
    public function listaSalidasAction(Request $request) {

        $response = new JsonResponse();
        $id = $request->request->get('idsistema');

        $v = [];

        if ($id != null && $id > 0) {

            $datos = $this->getDoctrine()
                    ->getRepository(Sistemas::class)
                    ->find($id);

            foreach ($datos->getComponentes() as $componente) {

                foreach ($componente->getSalidas() as $salida) {

                    $v[] = array("idsalida" => $salida->getId(), "salida" => $salida->getNombre(), "componente" => $componente->getNombre());
                }
            }

            $response->setData($v);
        }

        return $response;
    }

    /**
     * Devuelve un json con la información de todas las salidas disponibles para conectar a una entrada
     *
     * @param Request $request Petición que se envia desde el navegador y que debe incluir el id de la entrada a buscar.
     * @return json Cadena con estructura json que incluye todos los datos de las salidas de un componente que se ha buscado.
     * 
     * @Route("/sistemas/entradassalidas/lista", name="sistemas_EntradasSalidas")
     * 
     */
    public function listaEntradasSalidasAction(Request $request) {

        $response = new JsonResponse();
        $id = $request->request->get('idsistema');

        $v = [];
        if ($id != null && $id > 0) {

            $datos = $this->getDoctrine()
                    ->getRepository(Sistemas::class)
                    ->find($id);

            foreach ($datos->getComponentes() as $componente) {

                foreach ($componente->getSalidas() as $salida) {

                    $v[] = array("idsalida" => $salida->getId(), "salida" => $salida->getNombre(), "componente" => $componente->getNombre());
                }
            }

            $response->setData($v);
        }

        return $response;
    }

    /**
     * Devuelve un json con la información del sistema despues de modificar un valor.
     *
     * @param Request $request Petición que se envia desde el navegador y que debe incluir el id de la entrada y el nuevo valor.
     * @param Int $id Identificador del sistema sobre el que se esta trabajando
     * @return json Cadena con estructura json que incluye todos los datos del sistema simulado
     * 
     * @Route("/sistemas/cambiarvalorentrada/{id}", requirements={ "id"="\d+"},name="sistemas_cambiarvalorentrada")
     */
    public function cambiarValorEntradaAction(Request $request, $id = null) {

        $response = new JsonResponse();

        $identrada = $request->request->get('identrada');
        $valor = $request->request->get('valor');

        if ($id != null && $id > 0) {

            //Es una ejecución que ya esta en marcha, se recupera de la sesion, sino esta 
            //  en la sessión, pudiera estar disponible de una ejecución anterior.
            $sistema = $this->get('session')->get('sistema_' . $id);

            if (is_null($sistema)) {
                //La recupera de la base de datos, ya que no estaba en proceso.
                $sistema = $this->getDoctrine()
                        ->getRepository(Sistemas::class)
                        ->find($id);
            }

            $sistema->setValorEntrada($identrada, $valor);
            $sistema->calcular(true);
            $sistema->calculaFiabilidad();
            
            //Guarda el sistema con los cambios realizados.
            $this->get('session')->set('sistema_' . $id, $sistema);

            $response->setData($sistema->toArray());
        }

        return $response;
    }

    /**
     * Devuelve un json con la información del sistema despues de modificar el estado de un componente.
     *
     * @param Request $request Petición que se envia desde el navegador y que debe incluir el id de la entrada y el nuevo valor.
     * @param Int $id Identificador del sistema sobre el que se esta trabajando
     * @return json Cadena con estructura json que incluye todos los datos del sistema simulado
     * 
     * @Route("/sistemas/cambiarestadocomponente/{id}", requirements={ "id"="\d+"},name="sistemas_cambiarestadocomponente")
     */
    public function cambiarEstadoComponenteAction(Request $request, $id = null) {

        $response = new JsonResponse();

        $idcomponente = $request->request->get('idcomponente');
        $estado = $request->request->get('estado');
        $tipofallo = $request->request->get('tipofallo');

        if ($id != null && $id > 0) {

            //Es una ejecución que ya esta en marcha, se recupera de la sesion, sino esta 
            //  en la sessión, pudiera estar disponible de una ejecución anterior.
            $sistema = $this->get('session')->get('sistema_' . $id);

            if (is_null($sistema)) {
                //La recupera de la base de datos, ya que no estaba en proceso.
                $sistema = $this->getDoctrine()
                        ->getRepository(Sistemas::class)
                        ->find($id);
            }

            if ($estado == 1) {
                $operacion = $this->getDoctrine()
                        ->getRepository(AppCategoriasGenericas::class)
                        ->find($tipofallo);

                $sistema->setEstadoComponente($idcomponente, true, $operacion, 100, $operacion);
            } else {
                $sistema->setEstadoComponente($idcomponente, false, null, null, null);
            }
            
            $sistema->calcular(true);
            $sistema->calculaFiabilidad();

            //Guarda el sistema con los cambios realizados.
            $this->get('session')->set('sistema_' . $id, $sistema);

            $response->setData($sistema->toArray());
        }

        return $response;
    }

    /**
     * Devuelve un json con la información de todas las operaciones disponibles para realizar por la salida de un componente
     *
     * @param Request $request Petición que se envia desde el navegador y que debe incluir el id de la entrada a buscar.
     * @return json Cadena con estructura json que incluye todos los datos de las operaciones disponibles.
     * 
     * @Route("/sistemas/operaciones/lista", name="operaciones_lista")
     */
    public function listaOperaciones(Request $request) {

        $response = new JsonResponse();

        $v = [];

        $operaraciones = $this->getDoctrine()
                ->getRepository(AppCategoriasGenericas::class)
                ->findCategoriasTipo('OPERACIONES_COMPONENTES');

        foreach ($operaraciones as $dato) {
            $v[] = array("idoperacion" => $dato->getIdCategoria(), "operacion" => $dato->getNombre());
        }

        $response->setData($v);

        return $response;
    }

    /**
     * Devuelve un json con la información de todos los tipos de error disponibles para aplicar a la salida de un componente
     *
     * @param Request $request Petición que se envia desde el navegador y que debe incluir el id de la entrada a buscar. 
     * @return json Cadena con estructura json que incluye todos los datos de los errores disponibles.
     * 
     * @Route("/sistemas/errores/lista", name="errores_lista")
     * 
     */
    public function listaErrores(Request $request) {

        $response = new JsonResponse();

        $v = [];
        $errores = $this->getDoctrine()
                ->getRepository(AppCategoriasGenericas::class)
                ->findCategoriasTipo('ERRORES_COMPONENTES');

        foreach ($errores as $dato) {
            $v[] = array("iderror" => $dato->getIdCategoria(), "error" => $dato->getNombre());
        }

        $response->setData($v);
        return $response;
    }

}
