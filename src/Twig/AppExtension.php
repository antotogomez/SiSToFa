<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{

    public function getFunctions()
    {
        return array(
            new TwigFunction('archivoExiste', array($this, 'archivoExiste')),
            new TwigFunction('mostrarTecnicoGrupo', array($this, 'mostrarTecnicoGrupo')),
            new TwigFunction('corregirSlug', array($this, 'corregirSlug'))
        );
    }

    public function getFilters()
    {
        return array(
            new TwigFilter('resumir', array($this, 'resumir'))
        );
    }

    /**
     * Comprueba si existe un archivo
     */
    public function archivoExiste($ruta, $archivo = null)
    {
        /* [PENDIENTE] En la versión remota no funciona. De momento muestra siempre el icono indicado en la DB */
        // if ($archivo != null && file_exists($_SERVER['DOCUMENT_ROOT'].$ruta.'/'.$archivo)) {
        //     return true;
        // } else {
        //     return false;
        // }
        return true;
    }

    /**
     * Filtro de Twig que acorta un texto sin cortar palabras.
     * Ejemplo de uso: {{ descripcion|resumir()60 }}
     *
     * @return string Devuelve el texto con la extensión indicada.
     */
    public function resumir($texto, $caracteres)
    {
        $resumen = $texto;

        if (strlen($resumen) > $caracteres) {
            if (preg_match('/^.{1,'.$caracteres.'}\b/s', $texto, $match)) {
                $resumen = $match[0].' (...)';
            }
        }

        return $resumen;
    }

    /**
     * Si el usuario tiene nivel 1, muestra "Departamento de Servicios Informáticos" en lugar 
     * del nombre del técnico/grupo
     *
     * @param string $nombre Nombre del técnico o del grupo
     * @param int $nivel Nivel del usuario autenticado
     * @param string $grupo Nombre del grupo al que pertenece el técnico autenticado
     * @return string Nombre del técnico/grupo o Departamento de Servicios Informáticos
     */
    public function mostrarTecnicoGrupo($nombre, $nivel, $grupo = null)
    {
        if ($grupo != null && $nivel == 1) $nombre = 'Dpto. de Servicios Informáticos';

        return $nombre;
    }

    /*
    * Modifica un string para que cumpla las normas del slug
    */
    public function corregirSlug($slug) {
        // replace non letter or digits by -
        $slug = preg_replace('~[^\pL\d]+~u', '_', $slug);

        // transliterate
        $slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);

        // remove unwanted characters
        $slug = preg_replace('~[^-\w]+~', '', $slug);

        // trim
        $slug = trim($slug, '-');

        // remove duplicate -
        $slug = preg_replace('~-+~', '_', $slug);

        // lowercase
        $slug = strtolower($slug);

        return $slug;
    }
}