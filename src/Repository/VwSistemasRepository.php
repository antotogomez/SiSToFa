<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Repository;

use App\Entity\VwSistemas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VwSistemas|null find($id, $lockMode = null, $lockVersion = null)
 * @method VwSistemas|null findOneBy(array $criteria, array $orderBy = null)
 * @method VwSistemas[]    findAll()
 * @method VwSistemas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 */
class VwSistemasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VwSistemas::class);
    }
    
    /**
     * @return VwSistemas[]
     */
    public function findByPropietario($idPropietario) {
                
        //echo $idPropietario;
        return $this->createQueryBuilder('c')
                        ->andWhere('c.idpropietario = :propietario')
                        ->setParameter('propietario', $idPropietario)
                        ->orderBy('c.nombre', 'ASC')
                        ->getQuery()
                        ->getResult();
        
    }
    
}
