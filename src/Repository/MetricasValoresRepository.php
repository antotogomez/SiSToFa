<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Repository;

use App\Entity\MetricasValores;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MetricasValores|null find($id, $lockMode = null, $lockVersion = null)
 * @method MetricasValores|null findOneBy(array $criteria, array $orderBy = null)
 * @method MetricasValores[]    findAll()
 * @method MetricasValores[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method void deleteValoresEjecucion($id)
 */
class MetricasValoresRepository extends ServiceEntityRepository 
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MetricasValores::class);
    }
 
    public function findValoresEjecucion($metrica,$ejecucion)
    {

        return $this->createQueryBuilder('v')
            ->andWhere('v.idmetrica = :metrica')
                 ->andWhere('v.idejecucion = :ejecucion')
            ->setParameter('metrica', $metrica)
                ->setParameter('ejecucion', $ejecucion)
                ->orderBy("v.instante")
            ->getQuery()
                ->getResult()
        ;
    }
    
}
