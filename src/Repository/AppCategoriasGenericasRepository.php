<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Repository;

use App\Entity\AppCategoriasGenericas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AppCategoriasGenericas|null find($id, $lockMode = null, $lockVersion = null)
 * @method AppCategoriasGenericas|null findOneBy(array $criteria, array $orderBy = null)
 * @method AppCategoriasGenericas[]    findAll()
 * @method AppCategoriasGenericas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AppCategoriasGenericasRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, AppCategoriasGenericas::class);
    }

    /**
     * @return AppCategoriasGenericas
     */
    public function findApartadosMenuNivel($nivel) {
        return $this->createQueryBuilder('c')
                        ->andWhere('c.tipo = 18')
                        ->andWhere('c.nivel <= :nivel')
                        ->andWhere('c.activado = 1')
                        ->setParameter('nivel', $nivel)
                        ->orderBy('c.orden', 'ASC')
                        ->getQuery()
                        ->getResult();
        
    }

    /**
     * @return AppCategoriasGenericas
     */
    public function findApartadosMenuTodos() {
        return $this->createQueryBuilder('c')
                        ->andWhere('c.tipo = 18')
                        ->orderBy('c.nombre', 'ASC')
        ;
    }
    /**
     * @return AppCategoriasGenericas
     */
    public function findCategoriasSuperiores() {
        return $this->createQueryBuilder('c')
                        ->andWhere('c.tipo is null')
                        ->orderBy('c.nombre', 'ASC')
        ;
    }

    /**
     * @return AppCategoriasGenericas
     */
    public function findCategoriaCodigo($codigo) {
        return $this->createQueryBuilder('c')
                        ->andWhere('c.codigo = :codigo')
                        ->setParameter('codigo', $codigo)
                        ->setMaxResults(1)
                        ->getQuery()
                        ->getOneOrNullResult();
    }

    /**
     * @return AppCategoriasGenericas
     */
    public function findCategoriasTipo($tipo) {
        
        $result = $this->createQueryBuilder('c')
                        ->andWhere('c.codigo = :tipo')
                        ->setParameter('tipo', $tipo)
                        ->getQuery()
                        ->getOneOrNullResult();
        
        if(!is_null($result)){
            return $this->createQueryBuilder('c')
                        ->andWhere('c.tipo = :tipo')
                        ->setParameter('tipo', $result->getIdCategoria())
                        ->orderBy('c.nombre', 'ASC')
                        ->getQuery()
                        ->getResult();
        }
        else {
            return null;
        }
                        
    }
    
    /**
     * @return AppCategoriasGenericas
     */
    public function findCategoriasTipoQuery($tipo) {
        
        $result = $this->createQueryBuilder('c')
                        ->andWhere('c.codigo = :tipo')
                        ->setParameter('tipo', $tipo)
                        ->getQuery()
                        ->getOneOrNullResult();
        
        if(!is_null($result)){
            return $this->createQueryBuilder('c')
                        ->andWhere('c.tipo = :tipo')
                        ->setParameter('tipo', $result->getIdCategoria())
                        ->orderBy('c.nombre', 'ASC');
                        //->getQuery()
                        //->getResult();
        }
        else {
            return null;
        }
                        
    }

}
