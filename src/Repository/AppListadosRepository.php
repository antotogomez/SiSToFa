<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Repository;

use App\Entity\AppListados;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AppListados|null find($id, $lockMode = null, $lockVersion = null)
 * @method AppListados|null findOneBy(array $criteria, array $orderBy = null)
 * @method AppListados[]    findAll()
 * @method AppListados[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AppListadosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AppListados::class);
    }

    /**
     * @return AppListados
     */
    public function findBySlug($slug)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.slug = :val')
            ->setParameter('val', $slug)
            ->orderBy('a.idlistado', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @return AppListados
     */
    public function findOneByIdlistado($id)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.idlistado = :val')
            ->setParameter('val', $id)
            ->orderBy('a.idlistado', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    
    /**
     * @return AppCategoriasGenericas
     */
    public function findOpcionesMenu($menu, $nivel) {
        return $this->createQueryBuilder('l')
                        ->andWhere('l.activado = 1')
                        ->andWhere('l.tipo = :menu')
                        ->andWhere('l.nivel <= :nivel')
                        ->setParameter('nivel', $nivel)
                        ->setParameter('menu', $menu)
                        ->orderBy('l.orden', 'ASC')
                        ->getQuery()
                        ->getResult();
        
    }
    
    /*
    public function findOneBySomeField($value): ?Sistemas
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
