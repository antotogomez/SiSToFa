<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Repository;

use App\Entity\VwEjecuciones;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VwEjecuciones|null find($id, $lockMode = null, $lockVersion = null)
 * @method VwEjecuciones|null findOneBy(array $criteria, array $orderBy = null)
 * @method VwEjecuciones[]    findAll()
 * @method VwEjecuciones[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 */
class VwEjecucionesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VwEjecuciones::class);
    }
    
    /**
     * Devuelve las ejecuciones realizadas en una simulación.
     * 
     * @param Long $idsimulacion
     * @return array
     */
    public function findEjecucionesSimulacion($idsimulacion)
    {
        return $this->createQueryBuilder('v')
                 ->andWhere('v.idsimulacion = :idsimulacion')
            ->setParameter('idsimulacion', $idsimulacion)
                ->orderBy("v.fecha")
            ->getQuery()
                ->getResult();
    }
    
}
