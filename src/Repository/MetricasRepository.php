<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Repository;

use App\Entity\Metricas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Metricas|null find($id, $lockMode = null, $lockVersion = null)
 * @method Metricas|null findOneBy(array $criteria, array $orderBy = null)
 * @method Metricas[]    findAll()
 * @method Metricas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MetricasRepository extends ServiceEntityRepository 
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Metricas::class);
    }
 
}
