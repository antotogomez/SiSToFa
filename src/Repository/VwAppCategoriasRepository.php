<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Repository;

use App\Entity\VwAppCategorias;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VwAppCategorias|null find($id, $lockMode = null, $lockVersion = null)
 * @method VwAppCategorias|null findOneBy(array $criteria, array $orderBy = null)
 * @method VwAppCategorias[]    findAll()
 * @method VwAppCategorias[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VwAppCategoriasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VwAppCategorias::class);
    }
   
}
