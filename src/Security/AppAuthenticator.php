<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Guard\PasswordAuthenticatedInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Security;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use App\Entity\Usuarios;

/**
 * Clase que implementa el interfaz de autenticación de usuarios mediante usuario y contraseña
 * 
 * @author Antonio Gómez <i52goloa@uco.es>
 */
class AppAuthenticator extends AbstractGuardAuthenticator implements PasswordAuthenticatedInterface {

    private $security;
    private $em;
    private $router;

    /**
     * Called on every request. Return whatever credentials you want,
     * or null to stop authentication.
     */
    public function __construct(Security $security, EntityManagerInterface $em, RouterInterface $router) {
        $this->security = $security;
        $this->em = $em;
        $this->router = $router;
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning `false` will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request) {
        //Método que determina si hay que autenticar o no
        //var_dump($login);exit(0);//DEBUG
        return ( ($request->getPathInfo() == '/login') && $request->isMethod('POST') );
    }

    /**
     * Called on every request. Return whatever credentials you want,
     * or null to stop authentication.
     */
    public function getCredentials(Request $request) {

        if (!$username = $request->request->get("username")) {
            // no token? Return null and no other methods will be called
            return null;
        }

        //Autenticación basada en base de datos. Recoge el usuario y la contraseña
        // del formulario y loc comprueba controa la base de datos.
        return array(
            'username' => $request->request->get('username'),
            'password' => $request->request->get('password'),
            'certificado' => $request->request->get('certificado'),
        );
    }

    /**
     * 
     * @param type $credentials
     * @param UserProviderInterface $userProvider
     * @return type
     * @throws CustomUserMessageAuthenticationException
     */
    public function getUser($credentials, UserProviderInterface $userProvider) {

        if (null === $credentials) {
            // The token header was empty, authentication fails with HTTP Status
            // Code 401 "Unauthorized"
            return null;
        }

        $apiKey = $credentials['username'];

        try {
            return $userProvider->loadUserByUsername($apiKey);
        } catch (UsernameNotFoundException $e) {
            throw new CustomUserMessageAuthenticationException('Usuario "' . $credentials['username'] . '" no encontrado');
        }

    }

    /**
     * @param type $credentials
     * @param UserInterface $user
     * @return boolean
     */
    public function checkCredentials($credentials, UserInterface $user) {
        $usuario = $this->em->getRepository('App:Usuarios')
                ->findOneBy(array('login' => $credentials['username'],
            'password' => md5($credentials['password']),
            'estado' => 1));

        if ($usuario) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 
     * @param Request $request
     * @param TokenInterface $token
     * @param type $providerKey
     * @return RedirectResponse
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey) {
        // on success, let the request continue
        $url = $this->router->generate('inicio');
        return new RedirectResponse($url);
    }

    /**
     * 
     * @param Request $request
     * @param AuthenticationException $exception
     * @return RedirectResponse
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception) {

        $e = new AuthenticationException("Error en la identificación del usuario.");
        
        $request->getSession()->set(Security::AUTHENTICATION_ERROR, $e);
        $url = $this->router->generate('login');
        return new RedirectResponse($url);

    }

    /**
     * Called when authentication is needed, but it's not sent
     */
    public function start(Request $request, AuthenticationException $authException = null) {

        $url = $this->router->generate('login');
        return new RedirectResponse($url);
    }

    public function supportsRememberMe() {
        return false;
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function getPassword($credentials): ?string {
        return $credentials['password'];
    }

}
