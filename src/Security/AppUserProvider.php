<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Security;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use App\Repository\UsuariosRepository;
use App\Entity\Usuarios;
use App\Services\Funciones;
use App\Services\cDatos;

/**
 * Clase que implementa la interfaz UserProviderInterface y que se encarga de 
 * obtener los datos del usuario previamente autenticado.
 * 
 * @author Antonio Gómez <i52goloa@uco.es>
 */
class AppUserProvider implements UserProviderInterface {

    private $em;

    /**
     * @InjectParams
     */
    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }

    public function refreshUser(UserInterface $user) {
        if (!$user instanceof Usuarios) {
            throw new UnsupportedUserException(
                    sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * Verifica que el objeto recido es de la clase adecuada.
     * 
     * @param Class $class
     * @return Class
     */
    public function supportsClass($class) {
        return Usuarios::class === $class;
    }

    /**
     * Obtiene los datos del usuario del repository establecido buscandolo por el 
     * login o identificador del usuario.
     * 
     * @param string $username login o nombre de usuario por el que se busca en el repositorio. Tiene que ser unico.
     * @return Usuarios|null
     * @throws UsernameNotFoundException
     */
    public function loadUserByUsername($username): ?Usuarios {

        try {

            $user = $this->em->getRepository(Usuarios::class)
                    ->findOneByLogin($username);
        } catch (NoResultException $e) {
            $message = sprintf('No se ha encontrado un usuario con el login indicado: "%s".', $username);

            throw new UsernameNotFoundException($message, 0, $e);
        }

        return $user;
    }

}
