<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Operaciones;

/**
 * Description of OperacionMedia
 *
 * @author Antonio
 */
class OperacionMedia implements IOperacionComponente {

    //put your code here
    public function operacion($entradas) {

        $suma = 0;
        $elementos = 0;
        foreach ($entradas as $entrada) {

            //var_dump($entrada->getValor());
            $suma = $suma + $entrada->getValor();
            $elementos++;
        }
        if ($elementos > 0) {
            //echo "----------";
            //echo ( $suma/$elementos);
            return $suma / $elementos;
        } else {
            return null;
        }
    }

}
