<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Operaciones;

/**
 * Description of OperacionMedia
 *

 */

/**
 * 
 * Genera un error ChunkFlip en el texto, establece el valor de los
 * últimos 4 bits consecutivos en un carácter aleatorio de
 * la cadena a 1.
 * 
 * @author Antonio Gómez <i52goloa@uco.es>
 * 
 */
class ErrorStuckAt1 implements IErrorComponente {

    public function error($valor): ?float {

        return null;
    }

}
