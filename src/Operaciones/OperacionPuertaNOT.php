<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Operaciones;

/**
 * Description of OperacionMedia
 *
 * @author Antonio
 */
class OperacionPuertaNOT implements IOperacionComponente {

    //put your code here
     public function operacion($entradas) {

        $resultado = null;
        
        foreach ($entradas as $entrada) {
           if (is_null($resultado)){
               $resultado = $entrada->getValor();
           }
           else {
               $resultado = ~ $resultado;
           }
        }
        
        return $resultado;
    }
}
