<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Operaciones;

/**
 * Description of IErrorComponente
 *
 * @author Antonio
 */
interface IErrorComponente {

    /**
     * Funcion que aplica un error a un valor numerico recibido
     * 
     * @param float $valor
     * 
     * @return float Resultado de la operacion tras aplicar un error.
     */
    public function error($valor);
}

/**

  // Genera un error ChunkFlip en el texto, establece el valor de los
  // últimos 4 bits consecutivos en un carácter aleatorio de
  // la cadena a 1.
  ERRORES.prototype.StuckAt1 =function()
  {
  var mask = 0x0F; // Establece los 4 ultimos caracteres 0000 1111
  //Aplica el operador binario OR
  this.byte_array[this.posicion] |= mask;
  //Devuelve la nueva cadena de caracteres
  return this.Recompone();
  };

 */
?>