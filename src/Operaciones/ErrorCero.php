<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Operaciones;

/**
 * Description of OperacionMedia
 *
 * @author Antonio
 */

/**
 * Genera un error Bit-flip en el texto, intercambia el valor de
 * un bit (cambiar un 0 por un 1 o viceversa), en el primer bit
 * de un carácter aleatorio de la cadena.
 */
class ErrorCero implements IErrorComponente {

    public function error($valor): ?float {

        //var mask = 0x01;
        //Aplica el operador binario XOR al utlimo bit
        //this.byte_array[this.posicion] ^= mask;
        return 0;
    }

}
