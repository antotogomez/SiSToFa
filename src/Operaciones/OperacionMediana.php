<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Operaciones;

/**
 * Description of OperacionMedia
 *
 * @author Antonio
 */
class OperacionMediana implements IOperacionComponente {

    function mediana($arr) {
        sort($arr, SORT_NUMERIC);
        $l = count($arr);
        return $l % 2 == 0 
                ? array_sum(array_slice($arr, ($l / 2) - 1, 2)) / 2 
                : array_slice($arr, $l / 2, 1)[0];
    }

    //put your code here
    public function operacion($entradas) {

        $suma = 0;
        $elementos = array();

        foreach ($entradas as $entrada) {

            $elementos [] = $entrada->getValor();
        }

        return $this->mediana($elementos);
    }

}
