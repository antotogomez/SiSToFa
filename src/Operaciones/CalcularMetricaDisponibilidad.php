<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Operaciones;

/**
 * Clase para realizar el calculo de la disponibilidad del sistema en ejecución.
 *
 * @author i52goloa
 */
class CalcularMetricaDisponibilidad implements ICalcularMetricaFiabilidad {

    /**
     * Realiza el calculo
     * 
     * @param object $ejecucion
     * @return float
     */
    public function calcula($ejecucion) {

        try {
            //(Tiempototal - tiempoaveria / numero de fallos)
            if (count($ejecucion->getErrores()) == 0) {
                return null;
            } else {
                return (($ejecucion->getStep() * 5) - (count($ejecucion->getErrores()) * 5)) / count($ejecucion->getErrores());
            }
        } catch (\Exception $e) {
            return null;
        }
    }

}
