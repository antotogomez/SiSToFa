<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Operaciones;

/**
 * Description of IOperacionComponente
 *
 * @author Antonio
 */
interface IOperacionComponente {

    /**
     * Funcion que realiza la operacion indicada con  las entradas que se pasan como 
     * parametros.
     * 
     * @param Collection<Entradas> $entradas
     * @return float Resultado de la operación que implementa cada 
     */
    public function operacion($entradas);
}

?>