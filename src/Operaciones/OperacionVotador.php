<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Operaciones;

/**
 * Clase que implementa la interfaz IOperacionComponente.
 * Calcula el valor mas repetido en la lista de entradas, y lo devuelve como valor
 * 
 * @author Antonio Gómez <i52goloa@uco.es>
 */
class OperacionVotador implements IOperacionComponente {

    public function operacion($entradas) {

        $valores = array();
        foreach ($entradas as $entrada) {
            $valores[$entrada->getValor()]=0;
        }
        
        foreach ($entradas as $entrada) {
            $valores[$entrada->getValor()]++;
        }
        
        $repetido=null;
        
        foreach ($entradas as $entrada) {
            if(is_null($repetido )) {
                $repetido = $entrada->getValor();
            }
            else {
                if($valores[$entrada->getValor()] > $valores[$repetido]){
                    $repetido = $entrada->getValor();
                }
            }
            
        }
        
        return $repetido;
      
    }

}
