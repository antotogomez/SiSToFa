<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

namespace App\Operaciones;

/**
 * Description of OperacionMedia
 *
 * @author Antonio
 */
class OperacionMayor implements IOperacionComponente {

    //put your code here
    public function operacion($entradas) {

        $mayor = 0;
        $elementos = 0;

        foreach ($entradas as $entrada) {
            if ($elementos == 0) {
                $mayor = $entrada->getValor();
            } else {
                if ($entrada->getValor() > $mayor) {
                    $mayor = $entrada->getValor();
                }
            }
            $elementos++;
        }
        if ($elementos > 0) {
            return $mayor;
        } else {
            return null;
        }
    }
}
