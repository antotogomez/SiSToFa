[ [Volver al inicio de la ayuda](inicio) ] [ [Siguiente: Administración de la aplicación](administracion) ]

# Consideraciones generales.

## Listados

Los listados de la aplicación presentan la información disponible para el usuario según sus permisos de visualización de objetos. Todos los listados se presentan con unas funcionalidades y aspecto similar.

El acceso a los menus disponibles para cada usuario estan en el menu de la aplicación.
<center><img src="/SiSToFa/ayuda/ayuda_listadogrupos.png" width="70%"><br/>Imagen: Ventana Listado de Grupos.</center>

Para cada listado, la información que se muestra es la siguiente:

* Nombre del listado.
* Buscar: Cuadro de texto que permite buscar en todas las columnas de listado. Introduciendo el texto de busqueda, filtra los elementos mostrando unicamente aquellos que tengan ese texto en alguna de sus columnas.
* Operaciones disponibles a nivel de listado. En todos los listados esta disponible la opción "Nuevo" para crear un nuevo elemento del listado.
* Columnas: Información de cada elemento visualizado. Pulsando sobre el nombre de cada columna se puede ordenar el listado por los valores de esa columna, de forma ascendente si se pulsa sobre ella una vez, y de forma descendente si se vuelve a pulsar sobre la columna.
* Columna de acciones: Acciones disponibles sobre cada elemento del listado. Se describen las acciones en la siguiente sección.
* Navegador de paginas del listado.

### Buscar

Encima de la tabla de datos, alineado a la derecha, esta el cuadro de busqueda. Este cuadro permite filtrar las filas del lisado mostrando unicamente aquellas que contengan la cadena del cuadro de texto en cualquiera de los campos de cada fila.

Para filtrar unicamente es necesario comenzar a introducir en este cuadro de texto los caracteres por los que se quiera filtrar, y de forma automática

Según el perfil y el grupo al que pertenezca el usuario, los datos pueden aparecer ya filtrados y unicamente aparezcan en el listado aquellos registros sobre los que el usuario tenga visibilidad.

### Operaciones disponibles.
A la derecha del listado, la ultima columna de cada dato muestra las acciones disponibles en cada elemento de la fila. Las operaciones disponibles en los listados son las siguientes:

|Icono |Acción  | Descripción|
|:---: | :---: | :---|
|<img src="/SiSToFa/img/edit-2-line-white.png" style="background-color: #940010; width: 40px;">|Editar|Abre la ventana de edición del elemento de seleccionado.|
|<img src="/SiSToFa/img/eye-line-white.png" style="background-color: #940010; width: 40px;">|Visualizar|Abre la ventana de visualización gráfica del elemento de seleccionado.|
|<img src="/SiSToFa/img/file-copy-2-line-white.png" style="background-color: #940010; width: 40px;">|Clonar|Clonar el elemento seleccioando creando una nueva copia del mismo.|
|<img src="/SiSToFa/img/play-fill-white.png" style="background-color: #940010; width: 40px;">|Iniciar ejecución|Inicia la ejecución de la simulación seleccionada.|
|<img src="/SiSToFa/img/line-chart-fill-white.png" style="background-color: #940010; width: 40px;">|Ver resultados|Inicia la ejecución de la simulación seleccionada.|
|<img src="/SiSToFa/img/file-search-line-white.png" style="background-color: #940010; width: 40px;">|Comprarar resultados|Inicia la ejecución de la simulación seleccionada.|
|<img src="/SiSToFa/img/delete-bin-line-white.png" style="background-color: #940010; width: 40px;">|Eliminar resultados|Inicia la ejecución de la simulación seleccionada.|
|<img src="/SiSToFa/img/locker-white.png" style="background-color: #940010; width: 40px;">|Bloquear/Desbloquear|Bloquea o desbloquea un usuario de la base de datos, estableciendo su posibilidad de acceder o no a la aplicación.|


### Paginación.

El pie del listado muestra la información de paginación del listado. Se indica el número de registros que se estan mostrando y el total. En la parte derecha del pie del listado
se muestra el número total de páginas disponibles y los botones de navegación para ir a las páginas primera, anterior, siguiente y última.

##Paginas de edición de datos.

Cuando se indica la acción Editar en un listado, se muestra la pantalla de edición del objeto seleccionado. La pantalla de edición de un objeto consultar y modificar los datos del objeto. Tambien desde esta pantalla se puede eliminar y clonar el objeto.

Las paginas de edición ademas de permitir modificar los datos de un objeto ya existente, permiten crear objetos nuevos del mismo tipo. Un ejemplo de pantalla de edición, es el siguiente:

<center><img src="/SiSToFa/ayuda/ayuda_gruposeditar.png" width="70%"><br/>Imagen: Ventana de Edición de Grupos.</center>

La cabecera del contenido de la página indica el tipo de objeto y el nombre del objeto que se esta editando, o crear si se esta creando un nuevo.

En la misma cabecera del contenido aparecen dos botones alineados a la derecha:

* Nuevo: Limpia el formulario y permite introducir los datos de un objeto nuevo.
* Volver: Regresa a la listado de objetos para buscar otro.

A continuación se muestran un marco con los datos principales del objeto. Con un "*" y con negrita estan indicados los campos que son requeridos para ese tipo de objeto.

En el marco de datos prinicpales del objeto, alineados abajo a la derecha estan los botones con las acciones disponibles para esos datos. Habitualmente los acciones que se pueden realizar sobre esos datos son:

* Guardar: Almancena los datos en pantalla en la base de datos para asegurar su persistencia.
* Clonar: Crea una copia de los datos que se muestran y sus dependencias para crear un nuevo objeto. Este nuevo objeto tendrá como nombre el mismo que el original seguido del texto " (COPIA)".
* Eliminar: Elimina los datos que se muestran así como todos sus datos dependientes. Antes de eliminar los datos se pide confirmación al usuario. Si los datos tiene relaciones con otros objetos este no se podrá eliminar.

Bajo el marco de los datos principales del objeto, se muestran otros marcos con los objetos dependientes. Para cada tipo de objeto dependiente se muestra un marco diferente. Esta marco contiene una lista de objetos dependientes que puede ser seleccionados para editar o consultar.

### Ventana modal de ediciónde objetos dependientes.

La consulta o edición de los objetos dependientes se hace en una ventana modal (Son cuadros que aparecen sobre la página, bloqueando todas las funciones para concentrar el foco en una acción particular, en esta caso, modificar los datos del objeto dependiente. )
Este ventana se abre al seleccionar un objeto dependediente de la lista. Para cerrar la ventana de detalles del objeto dependiente, pulsar sobre el botón cerrar situado abajo a la derecha, o sobre la X situada arriba a la izquierda de la ventana modal.

<center><img src="/SiSToFa/ayuda/ayuda_sistemascomponente.png" width="70%"><br/>Imagen: Ejemplo de Ventana modal para la edición de un objeto dependiente.</center>

Las ventanas modales de edición de objetos cumplen aplican las mismas consideraciones que las ventanas no modales de edición de datos. Tienen un marco principal con los datos del objeto y otros marcos secundarios con listas de objetos dependientes.

## Mensajes.

Cuando se realiza una operación con los datos, esta puede realizarse correctamente o puede producir un error. Cuando se realiza correctamente se muestra un mensaje en color verde sobre fondo verde claro indicando que todo se ha realizado de forma correcta.

<center><img src="/SiSToFa/ayuda/ayuda_mensajecorrecto.png" width="70%"><br/>Imagen: Ejemplo de Mensaje de opearación correcta.</center>

Por el contrario, cuando la operación no se realiza correctamente, se muestra al usuario un mensaje de error indicandolo. Este mensaje se muestra en color rojo.

<center><img src="/SiSToFa/ayuda/ayuda_mensajeerror.png" width="70%"><br/>Imagen: Ejemplo de Mensaje de opearación fallida.</center>
<hr/>
[ [Volver al inicio de la ayuda](inicio) ] [ [Siguiente: Administración de la aplicación](administracion) ]