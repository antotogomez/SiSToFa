[ [Volver al inicio de la ayuda](inicio) ] [ [Siguiente: Sistemas](sistemas) ]

# Administración de la aplicación.

El menu "Aplicación" contiene las opciones que dan acceso a la gestión del acceso a la aplicación, asi como a configurar otras opciones de la aplicación.

# Mis datos

La opción del menú Aplicación &rarr; "Mis datos", muestra la información personal del usuario. Desde esta ventana se puede cambiar la contraseña de acceso al sistema. El resto de los datos son de solo lectura y solo pueden ser cambiados por un administrador o un administrador de grupo del grupo al que pertenezca el usuario. 

A esta pantalla tambien se puede acceder pulsando sobre el nombre del usuario en la cabecera de la apliación o pulsando el enlace "Mis Datos" en la pantalla de bienvenida de la aplicación.

<center><img src="/SiSToFa/ayuda/ayuda_misdatos.png" width="70%"><br/>Imagen: Ventana con los datos del usuario concetado.</center>

## Cambio de contraseña.

Para cambiar la contraseña de acceso al sistema, se debe indicar la contraseña nueva y confirmarla, seguidamente se pulsa sobre el botón cambiar, y si el cambio se ha realizado correctamente se muestra un mensaje indicandolo.

De igual forma si se ha producido un error al cambiar la contraseña, tambien se indica en un mensaje de error.

#Grupos de Trabajo.

Los grupos de usuarios permiten, ademas de organizar estos en unidades para su clasificación, establecer ambitos de seguridad para el acceso a sistemas y simulaciones.

Se puede establecer la visibilidad de estos objetos a nivel de grupo, para que unicamente los miembros de esos grupos puedan consutar esta información.

La creación de nuevos grupos puede ser realizada unicamente por un administrador de la aplicación, mientras que la gestión del mismo puede ser realizada tambien por los administradores de grupo. Esta gestión implica la creación o eliminación de usarios del grupo.

## Listado de grupos

En el menu Aplicación &rarr; Grupos, se puede consultar el listado de grupos disponibles. Esta opción solo esta disponible para Administradores de la aplicacion y Administradores de Grupo. Un usuario con rol administrador de la aplicación vería todos los grupos datos de alta, y un usuario con rol administrador de grupo solo puede ver el grupo al que pertenece. 

En el listado se muestra unicamente el nombre del grupo.

Las acciones que se pueden aplicar al grupo son Editar y Clonar. Editar abre la ventana de edición del grupo seleccionado y Clonar crea una copia del elemento seleccionado con el mismo nombre seguido del texto (COPIA).

<center><img src="/SiSToFa/ayuda/ayuda_listadogrupos.png" width="70%"><br/>Imagen: Ventana del listado de grupos.</center>

## Crear o editar un grupo.

Para crear un nuevo grupo, seleccionar el botón "Nuevo" desde el listadode grupos. Se abrira una nueva ventana con los datos de un grupo sin completar.
Para editar un grupo ya existente, seleccionarlo del listado de grupos y pulsar el botón "Editar".

Para crear un grupo hay que establecer los siguientes valores:

* Nombre: Nombre corto por el que se conocerá e identificará el grupo.
* Descripción: Breve texto que añade información adicional sobre el grupo.
* Estado: Situación del grupo. Si esta en estado activo permite añadir nuevos usuarios, sino, no lo permite. Un grupo con usuario no puede cambiar a estado BLOQUEADO.

<center><img src="/SiSToFa/ayuda/ayuda_gruposeditar.png" width="70%"><br/>Imagen: Ventana de edición de grupos.</center>

Las operaciones que se pueden realizar en la pantalla de edición de Grupos son las siguientes, todas ellas mediante el uso de botones.

* Nuevo: Limpia los valores de la pantalla de edición para permitir completar los nuevos valores de un nuevo grupo.
* Volver: Regresa al listado de grupos.
* Guardar: Guarda las modificaciones realizadas sobre el grupo.
* Clonar: Realiza una copia del grupo que se esta actualmente editando.
* Eliminar: Elimina el grupo que se esta actualmente editando.

En la misma pantalla de edición de grupos se muestran los usuarios que pertenecen a este grupo. De cada uuario se muestran los datos basicos como nombre, NIF, estado y rol. Seleccionando el usuario
se puede abrir la ventana de edición de usuarios.

# Usuarios.

Los usuarios representan a las personas que hacen uso de la aplicación. Pueden tener roles asignados para disponer de mayor nivel de administración en la aplicación y pueden ser propietario de otros objetos, como sistemas y simulaciones, que pueden compartir con otros usuarios según el nivel de visibilidad establecido sobre esos objetos.

La creación de nuevos usuarios puede ser realizada tanto por un administrador de la aplicación, como por un administrador de grupo. Los administradores de aplicación pueden asignar ese nuevo usuario a cualquier grupo existente en la aplicación mientras que los administradores de grupo solo pueden crear nuevos usuarios dentro de su propio grupo.

## Listado de usuarios

En el menu Aplicación &rarr; Usuarios, se puede consultar el listado de usuarios visibles. Esta opción solo esta disponible para Administradores de la aplicacion y Administradores de Grupo.

En el listado se muestra ls siguiente información de los usuarios:

* Apellidos.
* Nombre.
* DNI.
* Identificador de acceso, o login.
* Grupo al que pertenece el usuario.
* Nivel del usuario.
* Estado de activación en el que se encuentra el usuario.

Las acciones que se pueden aplicar al grupo son Editar, Clonar y Activar/bloquear. Editar abre la ventana de edición del grupo seleccionado y Clonar crea una copia del elemento seleccionado con el mismo nombre seguido del texto (COPIA).
La acción Activar/Bloquear cambia el estado del usuario, pasandolo de Activo a Bloqueado o de Bloqueado a activo.

<center><img src="/SiSToFa/ayuda/ayuda_usuarioslistado.png" width="70%"><br/>Imagen: Ventana del listado de usuarios.</center>

## Crear o editar un usuarios.

Para crear un nuevo usuario, seleccionar el botón "Nuevo" desde el listadode de usuarios. Se abrira una nueva ventana con los datos de un nuevo usuario sin completar.
Para editar un usuario ya existente, seleccionarlo del listado de usuarios y pulsar el botón de la acción "Editar".

Para crear un nuevo usuario hay que establecer los siguientes valores:

* Nombre: Nombre del pila de la persona.
* Apellidos: Apellidos de la persona.
* DNI: Documento Nacional de Identidad o cualquier otro número de identificación oficial de la persona.
* Login: Identificacdor de acceso del usuario.
* E-Mail: Dirección de correo electronico de la persona. 
* Estado: Situación del usurio. Activado permite el acceso a la aplicación. Bloqueado no lo permite.
* Grupo: Grupo de usuarios al que pertenece el usuario.
* Rol: Conjunto de privilegios que tiene el usuario dentro de la aplicación. Se puede establecer alguno de los siguientes roles:
    * Usuario. Unicamente permite acceder a las opciones de diseño de sistemas, simulaciones y ejecución de simulaciones.
    * Administrador de grupo. Ademas de los privilegios del usuario, puede gestionar los usuarios de su propio grupo.
    * Administrador de la aplicación. * Administrador de grupo. Ademas de los privilegios del usuario, puede gestionar todos los grupos, usuarios y las opciones de la aplicación como Listados y Categorias.
    
<center><img src="/SiSToFa/ayuda/ayuda_usuarioseditar.png" width="70%"><br/>Imagen: Ventana de edición de grupos.</center>

Las operaciones que se pueden realizar en la pantalla de edición de Usuarios son las siguientes, todas ellas mediante el uso de botones.

* Nuevo: Limpia los valores de la pantalla de edición para permitir completar los nuevos valores de un nuevo grupo.
* Volver: Regresa al listado de grupos.
* Guardar: Guarda las modificaciones realizadas sobre el grupo.
* Clonar: Realiza una copia del grupo que se esta actualmente editando.
* Eliminar: Elimina el grupo que se esta actualmente editando.
* Cambiar la contraseña. Permite el cambio de contraseñas de los usuarios administrados.

En la misma pantalla de edición de usuarios se muestran la información de los objetos propiedad del usuario que esta siendo editado. Estos objetos son:

* Sistemas propiedad del usuario.
* Simulaciones propiedad del usuario.
* Historial de modificaciones del usuario.

## Cambio de contraseña.

Para cambiar la contraseña de acceso al sistema de un usuario administrado, se debe indicar la contraseña nueva y confirmarla, seguidamente se pulsa sobre el botón cambiar, y si el cambio se ha realizado correctamente se muestra un mensaje indicandolo.

De igual forma si se ha producido un error al cambiar la contraseña, tambien se indica en un mensaje de error.

# Categorias genéricas.

Las Categorias genéricas de la aplicación son los tipos  y categorias de todo tipo que se emplean en opciones varias de la aplicación. Inicialmente, la aplicación tiene definidas siguientes categorias genericas superiores que pueden se ampliadas por los usuarios administradores de la aplicación:

* Tipo de Listado. Define los tipos de listados en los que se agrupan los listados y establecen las opciones de menú existentes.
* Visibilidad. Grado de visibilidad de los objetos sobre los que aplique.
* Métrica de Fiabilidad. Diferentes tipos de calculo de metricas de la fiabilidad.
* Errores del componente. Metodos de calculo del error devuelto por componente en error.

La creación de nuevas categorías o la modificación de estas puede ser realizada unicamente por un administrador de la aplicación.

##Listado de categorias genéricas.

En el menu Aplicación &rarr; Categorias Genéricas, se puede consultar el listado de categorias disponibles. Esta opción solo esta disponible para Administradores de la aplicación.

En el listado se muestran los siguientes datos de la categoría.

* Nombre de la categoría.
* Nombre de la categoría superior a la que pertenece.

Las acciones que se pueden aplicar a la categoría son Editar, Clonar y Activar/bloquear. Editar abre la ventana de edición de la categoría seleccionado y Clonar crea una copia del elemento seleccionado con el mismo nombre seguido del texto (COPIA).
La acción Activar/Bloquear cambia el estado de la categoría, pasandolo de Activo a Bloqueado o de Bloqueado a activo.

<center><img src="/SiSToFa/ayuda/ayuda_categoriaslistado.png" width="70%"><br/>Imagen: Ventana del listado de Categorias.</center>

##Crear o editar una categoría.

Para crear una nueva categoría, seleccionar el botón "Nuevo" desde el listado de Categorías. Se abrira una nueva ventana con los datos de una categoría sin completar.
Para editar una categoría ya existente, seleccionarla del listado de Categorías y pulsar el botón "Editar".

Para crear una nueva categoría hay que establecer los siguientes valores:

* Nombre: Nombre por el que se conoce la categoría genérica.
* Valor: Cadena de caracteres que contiene un valor que será usado como la categoría. En algunos casos se indica la ruta de una clase para ejecutar funcionalidades configurables.
* Categoría Superior: Categoría a la que pertenece esta categoria. Las categorias superiores tiene este campo vacio.
* Nivel: Rol de usuario a partir del cual esta categoría esta disponible.
* Descripción: Información adicional de la categoría.
* Orden: Número de posición en la que se encuentra esta categoría respecto a otras del mismo tipo.
* Código: cadena de caracteres que indentifica de forma unica esta categoría. Se emplea para acceder desde el código a la categoría sin depender de su identificador autonumérico que puede cambiar.
* Estado: Establece si la categoria esta activada o no. Si no esta activada no puede ser seleccionada donde corresponda.

<center><img src="/SiSToFa/ayuda/ayuda_categoriaseditar.png" width="70%"><br/>Imagen: Ventana de edición de categorias.</center>

Las operaciones que se pueden realizar en la pantalla de edición de Grupos son las siguientes, todas ellas mediante el uso de botones.

* Nuevo: Limpia los valores de la pantalla de edición para permitir completar los nuevos valores de una nueva categoria.
* Volver: Regresa al listado de categorias.
* Guardar: Guarda las modificaciones realizadas sobre la categorias.
* Clonar: Realiza una copia de la categoria que se esta actualmente editando.
* Eliminar: Elimina la categoría que se esta actualmente editando.

# Listados.

Los listados configuran las opciones que se muestran en el menú. El gestionar las opciones del menú desde la propia aplicación permite añadir nuevas opciones y configurar de forma dinámica y adaptable al usuario.

La creación de nuevos listados puede ser realizada unicamente por un administrador de la aplicación.

## Listado de listados.

En el menu Aplicación &rarr; Listados, se pueden consultar el listado de listados disponibles. Esta opción solo esta habilitada para Administradores de la aplicacion.

En el listado se muestra el nombre del listado, el texto descriptivo y el slug, que es la url a la que se accede desde el navegador.

Las acciones que se pueden aplicar a el Listado son Editar, Clonar y Activar/bloquear. Editar abre la ventana de edición del Listado seleccionado y Clonar crea una copia del elemento seleccionado con el mismo nombre seguido del texto (COPIA).
La acción Activar/Bloquear cambia el estado del Listado, pasandolo de Activo a Bloqueado o de Bloqueado a activo.

<center><img src="/SiSToFa/ayuda/ayuda_listadoslistado.png" width="70%"><br/>Imagen: Ventana del listado de listados.</center>

## Crear o editar un listado.

Para crear un nuevo listado, seleccionar el botón "Nuevo" desde el listado de listados. Se abrira una nueva ventana con los datos de un listado sin completar.
Para editar un listado ya existente, seleccionarlo del listado de listados y pulsar el botón "Editar".

Para crear un listado hay que establecer los siguientes valores:

* Nombre: Nombre corto por el que se conocerá e identificará el listado.
* Texto: Texto largo que se mostrará en la cabecera del listado.
* Nombre para la url: identificador de la url que usa el controlador Symfony para mostrar este listado.
* Imagen: Icono que se muesta en la opción de menú y en la cabecera del listado.
* Estado: Situación del listado. Si esta en estado activo se muestra la opción en el menú, si no, no se muesta.
* Orden: Numero de posición en la que se muestra la opción en el menú correspondiente.
* Añadir separador: Indica si se muestra una linea separadora encima de la opción en el menú.
* Nivel: Rol de usuario a partir del cual se muestra esta opción del menú.
* Apartado: Menú en el que se mostrará esta opción del menú. Los apartados son categorías genéricas del tipo Tipo de Listado.

<center><img src="/SiSToFa/ayuda/ayuda_listadoseditar.png" width="70%"><br/>Imagen: Ventana de edición de listados.</center>

Las operaciones que se pueden realizar en la pantalla de edición de Grupos son las siguientes, todas ellas mediante el uso de botones.

* Nuevo: Limpia los valores de la pantalla de edición para permitir completar los nuevos valores de un nuevo listado.
* Volver: Regresa al listado de listados.
* Guardar: Guarda las modificaciones realizadas sobre el listado.
* Clonar: Realiza una copia del listado que se esta actualmente editando.
* Eliminar: Elimina el listado que se esta actualmente editando.

<hr/>
[ [Volver al inicio de la ayuda](inicio) ] [ [Siguiente: Sistemas](sistemas) ]

