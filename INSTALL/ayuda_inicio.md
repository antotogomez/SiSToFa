
<center><img src="/SiSToFa/ayuda/ayuda_inicio.png"><br/></center>

# Introducción

Este es el manual de usuario de la aplicación SiSToFa: Simulador de Sistemas Tolerantes a Fallos. Aplicación desarrollada en el ámbito de la realización de un Trabajo de Fin de Grado de la Ingeniería de Informática en la Escuela Politécnica Superior de la Universidad de Córdoba.

La aplicación esta diseñada para que cualquier usuario con algunos conocimientos informáticos pueda utilizar la aplicación de forma satisfactoria, ya que emplea convenciones y diseños de la interfaz
habituales en otras aplicaciones, tanto presentadas en web como instalables en ordenadores personales.

Este documento es un manual de usuario, y por tanto no incluye instrucciones de como instalar o poner en marcha la aplicación, para ellos acuda el fichero README.md que acompaña esta aplicación.

# Tabla de contenidos

Los contenidos del manual de usuario esta estructurados en las diferentes secciones que conforman la aplicación, siendo estos:

1. [Instalación y configuracion](install)
2. [Acceso al Sistema](acceso)
3. [Consideraciones generales](general)
4. [Administración de la aplicación](administracion)
5. [Sistemas](sistemas)
6. [Simulaciones](simulaciones)


