[ [Volver al inicio de la ayuda](inicio) ] [ [Siguiente: Acceso a la aplicación](acceso) ]

# Instalación de la aplicación.
## Requisitos mínimos.
La aplicación está desarrollada como una aplicación web y por tanto para su funcionamiento se requiere de un sistema servidor y otro cliente.

### Servidor.
El sistema servidor publicará la aplicación y la pondrá a disposición de los usuarios a través de una url de acceso. Este servidor deberá disponer de dos servicios de publicación, uno de páginas web con soporte php y otro de base de datos.

Las versiones mínimas de estos servicios en las que se ha probado la aplicación han sido, tanto en versión su versión.

* Apache 2.4.0
    * php7_module
    * alias_module
    * ssl_module
    * rewrite_module
* PHP 7.3.0 
    * Extensión pdo_mysql habilitada
* MariaDB 10.5.0

Este entorno ha sido comprobado tanto en Windows 10 x86 64bits como en Linux.
### Cliente.
Para el acceso a la aplicación se necesitan de un navegador web compatible con las siguientes tecnologías y versiones:

* HTML 5.0
* JavaScript 3ra edición del estándar ECMA-262.
* Soporte de cookies.
      
Se ha comprobado el correcto funcionamiento de la aplicación en los siguientes navegadores compatibles:

* Google Chrome™ (Versión 101.0.4951.67 ( Compilación oficial) (64 bits))
* Mozilla® Firefox® (última versión estable)
* Microsoft® Edge (Versión 101.0.1210.47 (Compilación oficial) (64 bits))

* Microsoft® Internet Explorer® NO es compatible.
      
## Instalación.

Para instalar la aplicación, descargar el código desde gitlab u obtenerlo en formato ZIP desde una instalación de la aplicación. Este descarga se realiza en el directorio donde se vaya a instalar la aplicación.

Desde gitlab:

    C:\wamp64\www\SiSToFa>git -clone https://gitlab.com/antotogomezlopez/sistofa.git

Desde una ubicación que lo publique como zip, descargarlo y descomprimirlo:

    C:\wamp64\www\SiSToFa>wget https://aplicaciones.uco.es/SiSToFa/instalacion/sistofa.zip
    C:\wamp64\www\SiSToFa>unzip sistofa.zip

De esta forma se dispone del código fuente de la aplicación en el directorio de instalación que se determine, al cual este documento se referirá a partir de ahora como  DIRECTORIO_DE_SiSToFa.

Una vez descargado, configurar en un servidor web con php habilitado el directorio de la aplicación. Para Apache HTTP sería así:

    Alias /SiSToFa "DIRECTORIO_DE_SiSToFa/web/"
    
    <Directory "DIRECTORIO_DE_SiSToFa/web/">
        Options Indexes FollowSymLinks MultiViews
  	AllowOverride all
	<ifDefine APACHE24>
            Require local
	</ifDefine>
	<ifDefine !APACHE24>
                Order Deny,Allow
                Allow from all
        </ifDefine>
    </Directory>

Se recomienda publicar la aplicación en un servidor con https activado. De esta la forma la comunicación entre el cliente entre y el servidor de hace de forma segura, ya que la información viaja cifrada entre ambos extremos.

## Crear la base de datos de la aplicación. 
Junto con código fuente de la aplicación se acompaña un fichero SQL con la información para la creación de una base de datos con la información necesaria para el inicio de la aplicación. Para la creación de la base de datos es necesario cargar el fichero en la instalación de MariaDB o MySQL en el servidor de base de datos destino. Para ello ejecutar el siguiente comando, o realizar la carga con la herramienta de base de datos deseada.

C:\wamp64\www\SiSToFa>mysql -u root -p -h HOST_MYSQL -P 3306 -i install/sistofa.sql


## Configuración inicial.
Una vez están los ficheros en su ubicación, Apache esta configurado para publicar los ficheros públicos de la aplicación y la base de datos creada en su ubicación correspondiente, es necesario configurar varios valores en la aplicación para su correcto funcionamiento.

La configuración básica de la aplicación únicamente requiere establecer los datos de conexión a la base de datos. Estos valores están establecidos en el fichero .env localizado en el directorio raiz de la aplicación. Modificar esos valores y completar con los datos correctos de configuración de acceso a la base de datos.


    DATABASE_HOST= HOST_MYSQL
    DATABASE_PORT=3306
    DATABASE_NAME=sistofa
    DATABASE_USER=sistofa
    DATABASE_PASSWORD=PASSWORD_ESTABLECIDO
    FICHERO_TRAZA=/var/log/sistofa.log
    ACCESO_CERTIFICADO=SI

También se configura en este fichero la ruta del fichero de traza y si esta habilitado el acceso mediante certificado digital o no.

<hr/>
[ [Volver al inicio de la ayuda](inicio) ] [ [Siguiente: Acceso a la aplicación](acceso) ] 
