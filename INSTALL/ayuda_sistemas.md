[ [Volver al inicio de la ayuda](inicio) ] [ [Siguiente: Simulaciones](simulaciones) ]

#Sistemas

Los sistemas son un conjunto de componentes relacionados entre mediantes sus entradas y salidas que sirven como unidad para estudiar su fiabilidad.

Cada componente tiene su propia fiablidad, que según esten interconectados y configurados implicarán en una u otra fiabilidad del sistema global. Igualmente cada salida de cada componente puede aplicar unas operaciones sobre las entradas que reciba el componente.

Los sitemas tienen propietario. Cada sistema definido en el sistema es propiedad de un usuario. Solo este usuario puede modificar este sistema, pero puede publicarlo a otros usuarios para que pueda ser visualizado y clonado por otros usuarios para hacer modificaciones sobre él.

## Listado de sistemas.

En el menu Sistemas se encuentran toda las opciones de acceso a la información de los sistemas por parte del usuario. Existen para cada usuario dos listados diferentes de sistemas disponibles:

* Listado Mis Sistemas: Muestra unicamente los sistemas de los que el usuario conectado es el propietario.
* Listado Todos los Sistemas Visibles. Muestra el resto de sistemas sobre los que el usuario tiene visibilidad. Estos son los que otros usuarios establezcan como visibilidad pública o que otros usuarios del mismo grupo del usuario conectado establezcan su visibilidad como de grupo.

En estos listado de sistemas visible se muestran los siguientes campos:

* Nombre del sistema.
* Descripcion del sistema.
* Nombre y apellidos del usuario propietario.
* Visibilidad del sistema, Siendo alguno de los valores: (Privado, Público o Grupo)

Las acciones que se pueden aplicar al simtema son Editar, Visualizar y Clonar. Editar abre la ventana de edición del sistema seleccionado, Visualizar abre la ventana de visualización y estudio estático del sistema y Clonar crea una copia del sistema seleccionado con el mismo nombre seguido del texto (COPIA).

<center><img src="/SiSToFa/ayuda/ayuda_sistemaslistado.png" width="70%"><br/>Imagen: Ventana del listado de sistemas.</center>

##Crear un nuevo sistema.

Para crear un nuevo sistema, seleccionar la opcion "Crear un nuevo sistema" desde el menu Sistemas. Tambien se puede
acceder a esta opción desde el listado de Sistemas, pulsando la botón "Nuevo". situado en la cabecera del listado.

Una vez situado en esta pantalla de creación de sistemas, es necesario completar los siguientes datos:

* Nombre por el que se identifica el sistema (Obligatorio).
* Visibilidad del sistema, se pueden seleccionar tres opciones:
    * Privado, solo es visible para el usuario que crea el sistema.
    * Privado del grupo. El sistema es solo visible para el grupo al que pertenece el usuario.
    * Publico. TOdos los usuarios con acceso al sistema pueden ver el sistema.
* Propietario: Se establece solo con la información del usuario actualmente conectado si es un usuario nuevo o del propietario actual si se esta consultando un sistema ya existente.
* Texto descriptivo del sistema.

Pulsar el boton guardar y los datos se guardaran en la base de datos y se estará en situación de editar el Sistema.

<center><img src="/SiSToFa/ayuda/ayuda_sistemaseditar.png" width="70%"><br/>Imagen: Ventana del listado de sistemas.</center>

###Editar un sistema existente.

Para editar un sistema, se accede desde la pantalla de selección de Sistemas o al guardar uno nuevo.

Para acceder a la pantalla de selección de Sistemas, pulsar sobre la opción del menu Sistemas->Listado de Sistemas.

En esta pantalla se muestran todos los sistemas para los que el usuario tiene visibilidad. Se puede filtrar por algunos 
de los campos:

* Nombre del sistema.
* Autor del Sistema.
* Visibilidad del Sistema.

Para ordenar el listado por alguno de los campos, pulsar sobre el nombre de la columna. Aparecerá un
triangulo con el vertice hacia arriba para indicar que se esta ordenando por ese campo de forma descendente.

Si se vuelve a pulsar sobre el nombre  de la columna, aparecerá un triangulo con el vertice hacia abajo
indicnado que se esta ordenando por ese campo de forma descendente.

Para ordendar por otro campo, pulsar sobre el nombre de la columna de la tabla deseada.

### Añadir componentes al sistema.

La lista de componentes de un sistema esta en el segundo marco de datos de la pagina de edición de sistema, con el título "COMPONENTES DEL SISTEMA'. Se pueden añadir componentes al sistema desde las pantallas de edición del Sistema o desde el visualizador de Sistemas.

Desde la pantalla de edición del Sistema, ir a la sección de Componentes del Sistema, y pulsar sobre el botón "Nuevo componente". Se mostrará una ventana modal con la información del nuevo componente.

<center><img src="/SiSToFa/ayuda/ayuda_sistemascomponente.png" width="70%"><br/>Imagen: Ventana modal de edición de componentes.</center>

Para crear uno nuevo, completar los datos del componente. Deben completarse los campos marcados como obligarios, los 
otos campos son opcionales para completar.

Los campos de un componente son:

* Nombre del componente (Requerido).
* Fiabilidad del componente. (Requerido)
* Configuración. (Requerido)
* Color del componente. (Opcional)
* Color del texto del componente. (Opcional)
* Descripción del componente (Opcional).

Para guardar el componente pulsar sobre el boton guardar.
Si se desea eliminar el componente, pulsar sobre el boton Eliminar. El sistema no dejará eliminar un 
componente si tiene dependencias de entradas y salidas con otros componentes.

Para editar componente, seleccionar el componente en la pantalla de sistemas.

En la pantalla modal de edición de componentes, se pueden modificar sus valores y añadir entradas y salidas.

### Añadir o modificar Entradas.

Las entradas del componente permiten la llegada de información al mismo para su posterior tratamiento.

La lista de entradas configuradas para cada componente del sistema se pueden consultar en la ventana modal de edición de componentes. Debajo del marco 
de datos prinicipales del objeto se situan dos solapas con la información de Entradas del Componente y Salidas del Componente. Para consultar las entradas seleccionar las solapa "ENTRADAS".

La lista de entradas muestra todas las entradas configuradas para el sistema. De cada entrada se muestra el nombre, el valor inicial que tiene esa entrada y
la conexión de salida de otro componente que provee de valor esta entrada.

<center><img src="/SiSToFa/ayuda/ayuda_entradaeditar.png" width="70%"><br/>Imagen: Ventana modal de edición de entrada de componentes.</center>

Para añadir una nueva entrada pulsar sobre el botón "Nueva Entrada" y para editarla, seleccionarla de la lista de entradas. Las entradas tienen la siguiente información:

* Nombre de la entrada (Requerido).
* Valor Inicial de la entrada (Requerido).
* Conexión. Salida de otro componente a la que esta conectada esta entrada. (Opcional).
* Color de representación de la entrada en la visualización del sistema. (Opcional)
* Color del texto de la entrada en la visualización del sistema. (Opcional)
* Descripción de la entrada (Opcional).

Las acciones que se pueden realizar sobre las entradas son:

* Crear una nueva. Pulsando el botón "Nueva".
* Guardar los datos nuevos o modificados de la entrada pulsando el botón "Guardar".
* Clonar los datos de la entrada, creando una nueva entrada con los mismos datos que la origen pero añadiendo " COPIA" al nombre.
* Eliminar una entrada pulsando el botón "Eliminar".

### Añadir o modificar Salidas.

Las salidas del componente permiten realizar una operación matemática sobre el conjunto de entradas del componente. Un componente puede tener tantas salidas como se necesiten y sobre cada una se puede aplicar una operación matematica diferente.

La lista de salidas configuradas para cada componente del sistema se pueden consultar en la ventana modal de edición de componentes. Debajo del marco 
de datos prinicipales del objeto se situan dos solapas con la información de Entradas del Componente y Salidas del Componente. Para consultar las entradas seleccionar las solapa "SALIDAS".

La lista de salidas muestra todas las salidas configuradas para el sistema. De cada salida se muestra el nombre y la operación que aplica.

<center><img src="/SiSToFa/ayuda/ayuda_salidaseditar.png" width="70%"><br/>Imagen: Ventana modal de edición de entrada de componentes.</center>

Para añadir una nueva salida pulsar sobre el botón "Nueva salida" y para editarla, seleccionarla de la lista de salidas. Las salidas tienen la siguiente información:

* Nombre de la salida (Requerido).
* Operación que se aplica a las entradas del componente.
* Color de representación de la salida en la visualización del sistema. (Opcional)
* Color del texto de la salida en la visualización del sistema. (Opcional)
* Descripción de la salida (Opcional).

Las acciones que se pueden realizar sobre las entradas son:

* Crear una nueva. Pulsando el botón "Nueva".
* Guardar los datos nuevos o modificados de la salida pulsando el botón "Guardar".
* Clonar los datos de la salida, creando una nueva entrada con los mismos datos que la origen pero añadiendo " COPIA" al nombre.
* Eliminar una salida pulsando el botón "Eliminar".

Las operaciones disponibles para aplicar en las salidas son las siguientes:

* Suma
* Media
* Mediana
* Valor más bajo.
* Valor más alto
* Valor más repetido

## Visualizar un sistema

Los sistemas pueden ser visualizados de forma gráfica lo que facilita el estudio de su fiabilidad de forma estática.

A la pantalla de visualización de un sistema se puede acceder desde:

* El listado de Sistemas, tanto Sistemas propios como Sistemas visibles, pulsando sobre la acción "Visualizar"
* Desde la pantalla de edición de Sistemas, pulsar sobre el botón "VER SISTEMA".

La pantalla de visualización de un sistema presenta una representación gráfica del sistema, mostrando sus componentes con entradas y salidas y las conexiones existentes entre ellos.

<center><img src="/SiSToFa/ayuda/ayuda_sistemavisualizar.png" width="70%"><br/>Imagen: Ventana de visualización de sistemas.</center>

Situado a la derecha del marco que contiene el gráfico se muestra otro marco con los valores de los componentes y la fiabilidad calculada. Este marco muestra todos los valores de las entradas y las salidas de los componentes que forman el sistema. Tambien se muestan las metricas de fiabilidad calculada sobre la situación actual del sistema.

Debajo del marco de visualización de gráfico se muestran los botones de control de la visualización del gráfico. Estos botones son:

* Zoom +: Para acercar el punto de vista del gráfico.
* Zoom -: Para alejar el punto de vista del gráfico.
* Ajustar: Ajusta el punto de vista para que todo el gráfico sea visible dentro del marco.

En el marco inferior de la ventana de visualización se muestra la formula que se ha generado para calcular la fiablidad del sistema basandose en la forma en la que estan conectatos los componentes y la configuración establecida para ellos.

### Simulación estática.

Desde la pantalla de visualización de sistemas se puede modificar el estado de los componentes para estudiar el comportamiento del sistema y su fiabilidad.

<center><img src="/SiSToFa/ayuda/ayuda_sistemasimular.png" width="70%"><br/>Imagen: Ventana de visualización y simulación estática de sistemas.</center>

Para modificar el estado de un componente, pulsar sobre el con el botón derecho del ratón para que aparezca el menú contextual del elemento. Este menú tiene las siguientes opciones:

* Editar. Abre el cuadro de dialogo para editar el componente.
* Poner en Fallo. Abre el cuadro de dialogo para modificar el estado del componente a en fallo.
* Activar componente. Abre el cuadro de dialogo para modificar el estado del componente a en fallo.
* Añadir Salida. Abre el cuadro de dialogo para añadir una salida al componente.
* Añadir Entrada. Abre el cuadro de dialogo para añadir una entrada al componente.

El cuadro de dialogo para modificar el estado de un componente permite alterar la situación de funcionamiento optimo del sistema y añadir errores en los componentes deseados y de esta forma estudiar su fiabilidad y su comportamiento. Para cambiar el estado del componente a fallo, seleccionar
Estado En fallo y seleccionar el tipo de error que se aplica a las salidas.

Para volver a activar el componente, se selecciona la opción Activo en el Estado y se pulsa Aceptar.

Si no se quiere hacer ningun cambio se pulsa sobre el botón "Cancelar" del cuadro de diaglogo y este se cierra.

<center><img src="/SiSToFa/ayuda/ayuda_sistemasestadocomponente.png" width="70%"><br/>Imagen: Ventana de modificación del estado de un componente.</center>

Despues de hacer cambios en el estado de los componentes, se recalculan todos los valores de entradas y salidas de los componententes del sistema. Tambien se recalculan todas las metricas de fiabilidad disponibles
para que el usuario pueda comprobar como afectan la situación de los componentes a la fiabilidad total del sistema.

<hr/>
[ [Volver al inicio de la ayuda](inicio) ] [ [Siguiente: Simulaciones](simulaciones) ]