[ [Volver al inicio de la ayuda](inicio) ] [ [Siguiente: Consideraciones generales](general) ]

# Acceso al Sistema

Al acceder a la aplicación, la primera pantalla que se muestra al usuario es la pantalla de login. En esta pantalla el usuario puede indentificarse para acceder al sistema
mediante el usuario y la contraseña o haciendo uso de un certificado digital utilizando el componente de autoforma.

Para acceder la sistema, tanto usando usuario y contraseña, como con certificado digital, el usuario previamente tiene que hacer sido registrado en el sistema. Para poder acceder al sistema haciendo uso
del certificado digital, el usuario tiene que tener registrado su numero de NIF.

<center><img src="/SiSToFa/ayuda/ayuda01_login.png" width="70%"><br/>Imagen: Ventana de acceso al sistema</center>

Para un primer acceso al sistema debe hacerse como usuario administrador con la contraseña por defecto indicada en el manual de instalación de la aplicación.

# Pantalla de bienvenida.

Al acceder a la aplicación, se presenta la pantalla de bienvenida, con enlaces a las opciones mas importantes de la aplicación. En la cabecera de página se muestra el nombre de la aplicación y el logotipo de la Universidad de Córdoba.

Otra información disponible en la cabecera es el entorno de Trabajo al que esta conectado el usuario, asi como el nombre del usuario que esta actualmente conectado. Este nombre de usuario es un enlace a la pagina "Mis datos" del usuario.

<center><img src="/SiSToFa/ayuda/ayuda_bienvenida.png" width="70%"><br/>Imagen: Pantalla de inicio de la aplicación.</center>

Los accesos que se muestran en esta pantalla de bienvenida o de inicio son:

* Manual de Usuario: Acceso directo a esta documentación.
* Mis datos: Muestra la pantalla de datos del usuario actualmente conectaado. Desde aqui permite la modificación de la contraseña.
* Ver mis sistemas: Muestra el listado de los sistemas de los que el usuario actual es el propietario.
* Ver mis simulaciones: Muestra el listado de definición de simulaciones de las que el usuario actual es el propietario.
* Ver el resultado de las simulaciones: Muestra el listado de ejecuciones de simulaciones de las que el usuario actual es el propietario.

# Menus

Debajo de la cabecera esta el menú de la aplicación:

<center><img src="/SiSToFa/ayuda/ayuda_menusistemas.png" width="70%"><br/>Imagen: Menú Simulaciones de la aplicación.</center>

El total de las opciones del menú son:

* Inicio: Vuelve a la pantalla de inicio.
* Sistemas: Opciones del menú para el diseño de Sistemas.
    * Nuevo Sistema: Abre la página de diseño de sistemas con los campos vacios preparada para crear un nuevo sistema.
    * Mis Sistemas: Abre el listado de sistemas, mostrando unicamente los sistemas de los que el usuario actualmente conectado es propietario.
    * Todos los Sistemas visibles: Abre el listado de sistemas, mostrando todos los sistemas sobre los que el usuario actualmente conectado tiene visibilidad.

* Simulaciones
    * Nueva Simulación: Abre la página de definición de simulaciones con los campos vacios preparada para crear una nueva.
    * Mis Simulaciones: Abre el listado de simulaciones, mostrando unicamente las simulaciones de los que el usuario actualmente conectado es propietario.
    * Todas las Simulaciones visibles: Abre el listado de simulaciones, mostrando todas las simulaciones sobre las que el usuario actualmente conectado tiene visibilidad.
    * Mis operaciones: Abre el listado de operaciones y calculos personalizados del usuario.
    * Ver resultados de la simulacion: Abre el listado de ejecuciones realizadas por el usuario.

* Aplicacion
    * Usuarios: Abre la página que muestra el listado de Usuarios del sistema.
    * Grupos: Abre el listado de grupos. Desde esta opción se permite crear, editar y modificar grupos.
    * Listados: Abre el listado de Listados. Desde esta opción se permite crear, editar y modificar listados de la aplicación que añadan nuevas opciones de menú.
    * Catagorias genéricas: Abre la página con el listado de categorias y tipos utilizados en la aplicación.
    * Manual de usuario: Muestra este manual de usuario con las instrucciones de uso de la aplicación.
    * Acerca de: Información acerca de la aplicación y licencia.
* Salir: Termina la sesión del usuario en la aplicación.

<hr/>
[ [Volver al inicio de la ayuda](inicio) ] [ [Siguiente: Consideraciones generales](general) ]
