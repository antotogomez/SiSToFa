[ [Volver al inicio de la ayuda](inicio) ]

# Simulaciones

Una simulación es un conjunto de acciones programadas sobre un sistema y la definición de un conjunto de metricas que obtener de ese
sistema.

## Listado de Simulaciones.

En el menu Simulaciones se encuentran toda las opciones de acceso a la información de las simulaciones por parte del usuario. Existen para cada usuario dos listados diferentes de simulaciones disponibles:

* Listado Mis Simulaciones: Muestra unicamente las Simulaciones de los que el usuario conectado es el propietario.
* Listado Todos las Simulaciones Visibles. Muestra el resto de Simulaciones sobre los que el usuario tiene visibilidad. Estas simulaciones son los que otros usuarios establezcan como visibilidad pública o que otros usuarios del mismo grupo del usuario conectado establezcan su visibilidad como de grupo.

En estos listados de simulaciones se muestran los siguientes campos:

* Nombre de la simulación.
* Sistema sobre el que se aplica la simulación.
* Descripcion de la simulación.
* Nombre y apellidos del usuario propietario.
* Visibilidad de la simulacion, Siendo alguno de los valores: (Privado, Público o Grupo)

Las acciones que se pueden aplicar a la simulación son Editar y Ejecutar. Editar abre la ventana de edición de la simulación seleccionada, Ejecutar abre la ventana de visualización y ejecución de simulaciones.

<center><img src="/SiSToFa/ayuda/ayuda_simulacioneslistado.png" width="70%"><br/>Imagen: Ventana del listado de simulaciones.</center>

##Crear una nueva Simulación.

Para crear una nueva simulación, seleccionar la opcion "Crear una nueva simulación" desde el menu Simulaciones. Tambien se puede
acceder a esta opción desde los listados de Smulaciones, pulsando la botón "Nuevo". situado en la cabecera del listado.

<center><img src="/SiSToFa/ayuda/ayuda_simulacioneseditar.png" width="70%"><br/>Imagen: Ventana del edición de simulaciones.</center>

Una vez situado en esta pantalla de creación de simulaciones, es necesario completar los siguientes datos:

* Nombre por él que se identifica la simulación.(Obligatorio).
* Sistema que se va a simular (Obligatorio). Seleccionar uno de la lista. Solo se muestran aquellos sobre los que el usuario conectado tiene visibilidad.
* Pasos (Opcional). Número máximo de pasos que se realizará en la simulación.
* Visibilidad de la simulación, se pueden seleccionar tres opciones:
    * Privado, solo es visible para el usuario que crea la simulación.
    * Privado del grupo. La simulación es solo visible para el grupo al que pertenece el usuario.
    * Publico. Todos los usuarios con acceso a la aplicación pueden ver la simulación.
* Propietario: Se establece solo con la información del usuario actualmente conectado si es un usuario nuevo o del propietario actual si se esta consultando una simulación ya existente.
* Descripción. Texto descriptivo e información adicional de la simulación.

Pulsar el boton guardar y los datos se guardaran en la base de datos y se estará en situación de editar la simulación.

###Editar una simulación existente.

Para editar una simulación, se accede desde la pantalla de selección de Simulaciones o al guardar una nueva.

Para acceder a la pantalla de selección de Simulaciones, pulsar sobre la opción del menu Simulaciones->Listado de Simulaciones.

En esta pantalla se muestran todas las Simulaciones para las que el usuario tiene visibilidad. Se puede filtrar por algunos 
de los campos:

* Nombre de la simulación.
* Sistema sobre la que actual la simulación.
* Autor del Simulación.
* Grupo del autor de la simulación.
* Visibilidad de la simulación.

Para ordenar el listado por alguno de los campos, pulsar sobre el nombre de la columna. Aparecerá un
triangulo con el vertice hacia arriba para indicar que se esta ordenando por ese campo de forma descendente.

Si se vuelve a pulsar sobre el nombre  de la columna, aparecerá un triangulo con el vertice hacia abajo
indicnado que se esta ordenando por ese campo de forma descendente.

Para ordendar por otro campo, pulsar sobre el nombre de la columna de la tabla deseada.

## Detalles de la simulación.
Ademas de los datos principales de la simulación, en la pantalla de edición de simualaciones se muestran los eventos y metricas que incluyen esa simulación:

* Una métrica es un valor que se va a ir recogiendo durante la ejecución de la simulación.
* Un evento es algo que va a ocurrir durante la ejecución de la simulación.

<center><img src="/SiSToFa/ayuda/ayuda_simulacioneseditar_detalles.png" width="70%"><br/>Imagen: Ventana de detalles de la simulación.</center>

### Añadir Metricas a la simulación.

La lista de métricas que se van a recoger en una simulación estan en el segundo marco de datos de la pagina de edición de simulaciones, con el título "METRICAS'. Se pueden añadir nuevas métricas a la simulación desde las pantallas de edición de la simulación.

Desde la pantalla de edición de la Simulación, ir a la sección de Métricas, y pulsar sobre el botón "Nueva Métrica". Se mostrará una ventana modal con la información de la nueva métrica.

<center><img src="/SiSToFa/ayuda/ayuda_simulacionmetrica.png" width="70%"><br/>Imagen: Ventana modal de edición de metricas.</center>

Para crear una nueva metrica, deben completarse los campos marcados como obligatorios, los 
otos campos son opcionales para completar.

Los campos para las métricas son los siguientes:

* Nombre de la métrica (Requerido).
* Color de la métrica. (Requerido). Por defecto es negro. Es el color con el que se presentará la métrica en el gráfico.
* Metrica que se desea recopilar. Solo se debe seleccionar una de las cuatro opciones. Cuando se selecciona una se elimina la selección de otra métrica. Las opciones son:
    * Estado del componente. Guarda en cada instante el estado del componente en valor númerico. 1 es activo y 0 es fallo.
    * Entradas. Guarda en cada instante el valor de una entrada de cualquier componente del sistema.
    * Salidas. Guarda en cada instante el valor de una salida de cualquier componente del sistema.
    * Metricas de fiabilidad. Guarda en cada instante el valor de una metrica de fiabilidad personalizada y seleccionada de la lista entre las opciones disponibles.
* Descripción de la métrica. Descripción e información adicional de la métrica. (Opcional).

Para guardar la métrica pulsar sobre el boton guardar.
Si se desea eliminar la métrica, pulsar sobre el boton Eliminar. 

Tambien es posible clonar la metrica pulsando sobre el botón "Clonar". Se creará una copia de esta métrica con todos los valores iguales excepto el campo nombre que será igual que el original añadiendo el texto "(COPIA)"

### Añadir Eventos a la simulación.

La lista de Eventos que se van a aplicar durante la ejecución de una simulación estan en el tercer marco de datos de la pagina de edición de simulaciones, con el título "EVENTOS'. Se pueden añadir nuevos eventos a la simulación desde la pantalla de edición de la simulación.

Desde la pantalla de edición de la Simulación, ir a la sección de Eventos, y pulsar sobre el botón "Nuevo Evento". Se mostrará una ventana modal con la información del nuevo evento.

<center><img src="/SiSToFa/ayuda/ayuda_simulacionevento.png" width="70%"><br/>Imagen: Ventana modal de edición de eventos.</center>

Para crear un nuevo evento, deben completarse los campos marcados como obligatorios, los 
otros campos son opcionales para completar.

Los campos para los eventos son los siguientes:

* Nombre del Evento (Requerido).
* Instante. (Requerido). Momento de la ejecución de la simulación en el que se aplica el evento.
* Evento de tipo estado de Componente. Se va a cambiar el estado y la fiabilidad de un componente:
    * Componente al que se le vá a cambiar el estado y la fiabilidad.
    * Nuevo estado que se va a aplicar.
    * Nueva fiabilidad para el componente. Si es
    * Metricas de fiabilidad. Guarda en cada instante el valor de una metrica de fiabilidad personalizada y seleccionada de la lista entre las opciones disponibles.
* Evento de tipo Valor de entrada. Se va a cambiar el valor de una entrada, y afectará al valor calculado de las salidas. Debe especificarse los siguientes datos:
    * Entrada  a la que se le vá a cambiar el valor.
    * Nuevo valor que se esteblece al aplicar este evento.
* Descripción del evento. Descripción e información adicional del evento. (Opcional).

Para guardar el evento pulsar sobre el boton guardar.
Si se desea eliminar el evento, pulsar sobre el boton Eliminar. 

Tambien es posible clonar el evento pulsando sobre el botón "Clonar". Se creará una copia de este evento con todos los valores iguales excepto el campo nombre que será igual que el original añadiendo el texto "(COPIA)"

## Pantalla de ejecución de una simulación.

Una vez la simulación esta definifa, se puede iniciar su ejecución para ir obteniendo resultados. El inicio de la ejecución de una simulación se puede iniciar desde varios puntos diferentes de la aplicación.

* Desde el listado de simulaciones, tanto las simulaciones del usuario como el listado de simulaciones visibles. Pulsando sobre el icono "Iniciar ejecución", se accede a la pantalla de inicio de una ejecución.
* Desde la ventana de edición de simulaciones, pulsando sobre el icono "Iniciar ejecución", se accede a la pantalla de inicio de una ejecución.

Al abrir la ventana de inicio de una ejecución, se muestra lo siguiente:

<center><img src="/SiSToFa/ayuda/ayuda_ejecutarsimulacion.png" width="70%"><br/>Imagen: Ventana Inicio de una ejecución.</center>

Desde esta pantalla se puede acceder a las pantallas de Edición y creación de nuevas simulaciones o volver al listado de Simulaciones haciendo uso de los botones a la derecha del titulo de la ventana.

La pantalla de ejecución de simulaciones se divide en seis paneles, cada uno con la siguiente información y funcionalidades.

### Panel de control de la ejecución.

En este panel se muestra el estado de la ejecución, mediante texto y un icono representativo del estado. Los estados en los que puede estar una ejecución son:

|Icono |Estado  | Descripción|
|:---: | :---: | :---|
|<img src="/SiSToFa/img/sim-stopped.gif" style="width: 40px;">|SIN INICIAR|La ejecución no se ha iniciado nunca, esta en espera de ser iniciado por primera vez.|
|<img src="/SiSToFa/img/sim-paused.gif" style="width: 40px;">|PAUSADA|La ejecución se ha iniciado pero ahora esta detenida momentaneamente y puede ser reinciada despues.|
|<img src="/SiSToFa/img/sim-running.gif" style="width: 40px;">|EN EJECUCIÓN|La ejecución esta actualmente en ejecución y se van aplicando eventos y obteniendo metricas.|

En los cuadros de texto se muestra el número de ejecución y el paso en el que actualmente esta la ejecución. Estos campos son de solo lectura.

Los botones permiten controlar la ejecución de la simulación, son los siguientes y tienen estas funciones:

|Icono |Acción  | Descripción|
|:---: | :---: | :---|
|<img src="/SiSToFa/img/play-fill-white.png" style="background-color: #940010; width: 40px;">|Iniciar|Inicia la ejecución de la simulación seleccionada.|
|<img src="/SiSToFa/img/pause-fill-white.png" style="background-color: #940010; width: 40px;">|Pausar|Pausa la ejecución de la simulación.|
|<img src="/SiSToFa/img/stop-fill-white.png" style="background-color: #940010; width: 40px;">|Parar|Detiene la ejecución de la simulación|
|<img src="/SiSToFa/img/restart-line-white.png" style="background-color: #940010; width: 40px;">|Reiniciar|Inicia una nueva ejecución para esta simulación.|
|<img src="/SiSToFa/img/line-chart-fill-white.png" style="background-color: #940010; width: 40px;">|Ver resultados|Abre la ventana de visualización de resultados de esta ejecución.|

### Panel de visualización y estado del sistema.

Este panel muestra una representación gráfica del sistema que se esta simulando, la situación de esta y los valores de cada uno de los componentes que lo forman.

Debajo del panel hay tres botones que permiten acercar o alejar el punto de vista del sistema, y otro que permite 

### Panel de visualización y estado del sistema.

Este panel muestra una representación gráfica del sistema que se esta simulando, la situación de esta y los valores de cada uno de los componentes que lo forman.

Desde el arbol de Información del Sistema se puede consultar el valor y estado de cada uno de los componentes y de sus entradas y salidas. Pulsando sobre cada elemento se destaca en el gráfico para mostrar al usuario su posición.

Debajo del panel hay tres botones que permiten acercar o alejar el punto de vista del sistema, y otro que permite 

En el marco inferior de la ventana de visualización se muestra la formula que se ha generado para calcular la fiablidad del sistema basandose en la forma en la que estan conectatos los componentes y la configuración establecida para ellos.

### Panel de Eventos a Aplicar.

Debajo del marco que muestra la formula de Calculo de la fiablidad del sistema se muestra el marco de Eventos a Aplicar.

Este marco muestra en una lista los eventos que se tienen que aplicar durante la ejecución de una simulación, ordenador por el momento en el que se aplican.

<center><img src="/SiSToFa/ayuda/ayuda_simulacioneventosaplicar.png" width="70%"><br/>Imagen: Marco de Eventos a aplicar.</center>

Para cada evento se muestra la siguiente información:

* Aplicado: Si el Evento esta aplicado o no. Si la columna no tiene valor aún no se ha aplicado. Si tiene un tick de color verde, el evento ya se ha aplicado.
* Nombre: Nombre del evento a aplicar.
* Instante: Momento de la ejecución de la simulación en el que se tiene que aplicar el evento.
* Tipo: Tipo de evento que se aplica. Si es un evento de cambio de valor de entrada o de estado de un componente.
* Evento: Descripcion del evento que se aplica.

### Panel de Metricas.

Debajo del marco que muestra los eventos a Aplicar se muestran las métricas que se estan recogiendo.

Para cada métrica se muestra la siguiente información:

* Nombre: Nombre de la métrica que se esta recogiendo.
* Color: Color con el que se mostrará la grafica representativa de esta métrica.
* Descripción: Información adicional sobre la métrica que se está recogiendo.


## Ejecución de una simulación.

Desde la pantalla de ejecuciónde simulaciones se puede iniciar una simulación pulsando sobre el boton "Iniciar". Se creará una nueva ejecución que lanzará un nuevo paso cada 5 segundos.

Cada nuevo paso o instante implica la aplicación de los eventos, recalcular la fiabilidad y los valores de las entradas y salidas de los componentes. La información actualizada de la ejecución de la simulación se recibe en la pantala de ejecución de simulaciones y se actualizan los datos.

En el panel de control de la ejecución se muestra la situación en la que esta la ejecución de la simulación, el identificador de la simulación, el paso actual en el que se encuentra la simulación y el total de pasos.

A medida que avanza la simulación se va incremente el Paso en el que esta esta encuentra y se van mostrando como aplicados los eventos de la lista.

En cualquier momento se puede pausar la simulación y continuarla despues. Incluso despues de cerrar la sesión del usuario conectado. Estando la simulación pausada se puede reanudar en cualquier momento desde el instante en el que se encontraba.

Una vez que se ha llegado al numero de pasos máximos indicados en la simulación, la ejecución se detiene y se muestra un mensaje indicado este hecho.

<center><img src="/SiSToFa/ayuda/ayuda_simulacionfinalizada.png" width="70%"><br/>Imagen: Ventana modal de fin de simulación.</center>

## Listado de ejecuciones.

El listado de simulaciones ejecutadas por el usuario se puede consultar en la opción de menú Simulaciones &rarr; Ver resultados de la simulación. Este listado muestra unicamente los resultados de ejecuciones realizadas por el propio usuario conectado.

De cada ejecución, la información que se muestra en el listado es la siguiente:

* Simulación. Nombre de la simulación que se ha ejecutado.
* Sistema. Nombre del sistema sobre el que se ha ejecutado la simulación.
* Fecha de inicio. Fecha en la que se inicio la ejecución por primera vez.
* Decripción de la simulación.

<center><img src="/SiSToFa/ayuda/ayuda_ejecucionlistado.png" width="70%"><br/>Imagen: Listado de ejecuciones.</center>

Las acciones que se pueden realizar sobre las ejecuciones son las siguientes:
* Continuar. Se reanuda la ejecución por el último paso realizado.
* Resultados. Muestra los resultados de la ejecución de la simulación.
* Comparar. Realiza la comparativa de dos ejecuciones de la misma simulación.
* Eliminar. Elimina la ejecución de la simulación y todas las métricas recopiladas.

### Ver resultados.

La ventana de visualización de resultados muestra en forma de gráfico y en tabla los valores de las métricas recopiladas durante la ejecución de la simulación.

En el marco de Información de la simulación se muestran los datos principales de la ejecución. La información que se muestra de la ejecución es la siguiente:

* Nombre de la simulación.
* Nombre del sistema sobre el que se ha ejecutado la simulación.
* Usuario. Usuario de la aplicación que ha iniciado la ejecución.
* Fecha de inicio de la simulación.
* Numero total de pasos que tiene la simulación.
* Numero total de pasos que se han ejecutado.

<center><img src="/SiSToFa/ayuda/ayuda_simulacionverresultados.png" width="70%"><br/>Imagen: Ventana de ejecución.</center>

Bajo el marco de Información de la Simulación, se muestran los marcos con cada una de las métricas que se han recopilado durante la ejecución de la simulación.

De cada métrica se muestra un marco con un gráfico de lineas con los valores obtenidos durante la ejecución de la simulación en el eje Y siendo el eje X del gráfico los instantes en los que se ha ido desarrollando la ejecución.

Tambien se muestra una tabla con los valores obtenidos para la métrica en cada instante de ejecución. Estos datos pueden ser descargados en formato csv para su tratamiento externo.

### Comparar resultados.

Los resultados de una ejecución de una simulación pueden ser comparados con los resultados de otra ejecución de la misma simulación. Realzar esta acción es posible desde dos puntos diferentes de la aplicación:

* Listado de ejecuciones. Pulsando el botón de acción "Comparar".
* Ventana de Visualización de Resultados. Pulsando el botón "Comparar Simulaciones"

Al seleccionar esta acción, se abre una ventana modal con todas las ejecuciones disponibles de la misma simulación de la ejecución seleccionada. Para realizar la comparación de resultados con otra ejecución, seleccionarla de la lista de opciones.

Se recargará la página de resultados mostrando la información de las dos ejecuciones.

Para cada métrica se mostrarán dos lineas en la gráfica, una por cada ejecución. En la tabla de resultados se muestran los valores en cada instante de las dos ejecuciones.

<center><img src="/SiSToFa/ayuda/ayuda_simulacioncomparar2.png" width="70%"><br/>Imagen: Ventana de comparación de resultados.</center>

<hr/>

[ [Volver al inicio de la ayuda](inicio) ]