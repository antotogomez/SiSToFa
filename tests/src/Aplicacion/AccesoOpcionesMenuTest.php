<?php
/**
 * This file is part of SiSToFa - Simulador de Sistemas Tolerantes a Fallos.
 *
 *  Copyright (c) 2022  Departamento de Ingeniería Electrónica y de Computadores. 
 *                       Universidad de Córdoba
 *
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl-3.0).
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code in legal directory.
 *
 * @author: Antonio Gomez <i52goloa@uco.es>
 * 
 */

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Description of pruebasCargaSimuSitfa
 *
 * @author Antonio
 */
class AccesoOpcionesMenuTest extends WebTestCase
{
    
    
//    public function testFooBar()
//    {
//        $client = static::createClient();
//        $client->request('GET', '/foo/bar/1');
//
//        $this->assertEquals(404, $client->getResponse()->getStatusCode());
//    }
    
//      public function testPaginaLogin()
//    {
//        $client = static::createClient();
//        $crawler = $client->request('GET', '/login');
//
//        $this->assertSame(200, $client->getResponse()->getStatusCode());
//        $this->assertTrue( str_contains( $crawler->filter('h1')->text(),'SimuSiTFa'));
//
//    }
    
    private $client = null;

    public function providerUsuarios() {
        return [
            ["i52goloa", "passwordfalso",null,"/login"],
            ["i52goloa", "1234",10, "/inicio"],
            ["i52mocaa", "hola",1, "/inicio"],
            ["i52heolm", "hola",0, "/inicio"],
            ["i52gobap", "adios",null, "/login"],
        ];
    }
     
    public function setUp(): void {
         
       $this->client = static::createClient();
           
    }
 
    public function testAccesoNoAutorizado()
    {
        
        $crawler = $this->client->request('GET', '/inicio');
        
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->followRedirect();
        $this->assertEquals(1, $crawler->filter('form')->count());
         
    }
    
    /**
     * @dataProvider providerUsuarios
     */
     public function testLogin($usuario, $password, $nivel,$url)
    {
        
        $crawler = $this->client->request('GET', '/login');
        
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(1, $crawler->filter('form')->count());
         
        $form = $crawler->selectButton('botonLogin')->form();
           
        $form['username'] = $usuario;
        $form['password'] = $password;
                    
        $this->client->submit($form);
        $bag = $this->client->getResponse()->headers;
        $this->assertSame($bag->get("Location"), $url);
        
        echo $usuario . " :: " . $bag->get("Location") . "=" . $url . "\n";
        
        $crawler = $this->client->followRedirect();
         
    }
    
  
    /**
     * @dataProvider providerUsuarios
     */
    public function testNuevoSistema($usuario, $password, $nivel,$url)
    {

        $this->testLogin($usuario, $password, $nivel,$url);
        $crawler = $this->client->request('GET', '/sistemas/crear');
        if(is_null($nivel)){
            $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        }
        else {
            $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        }
       
      

    }
    
    /**
     * @dataProvider providerUsuarios
     */
    public function testListadoUsuarios($usuario, $password, $nivel,$url)
    {
        
        $this->testLogin($usuario, $password, $nivel,$url);
        $crawler = $this->client->request('GET', '/listado/usuarios');
        if($nivel >= 1){
            $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        }
        else {
            $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        }
    }
    
    /**
     * @dataProvider providerUsuarios
     */
    public function testListadoGrupos($usuario, $password, $nivel,$url)
    {
        
        $this->testLogin($usuario, $password, $nivel,$url);
        $crawler = $this->client->request('GET', '/listado/grupos');
        
        if($nivel >= 1){
            $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        }
        else {
            $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        }
    }
    
    /**
     * @dataProvider providerUsuarios
     */
    public function testListadoListados($usuario, $password, $nivel,$url)
    {
        
        $this->testLogin($usuario, $password, $nivel,$url);
        $crawler = $this->client->request('GET', '/listado/listados');
        if($nivel >= 10){
            $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        }
        else {
            $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        }
    }
    
    /**
     * @dataProvider providerUsuarios
     */
    public function testListadoCategorias($usuario, $password, $nivel,$url)
    {
        
        $this->testLogin($usuario, $password, $nivel,$url);
        $crawler = $this->client->request('GET', '/listado/categoriasgenericas');
        if($nivel >= 10){
            $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        }
        else {
            $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        }
    }
    
    /**
     * @dataProvider providerUsuarios
     */
    public function testListadoMisSistemas($usuario, $password, $nivel,$url)
    {
        
        $this->testLogin($usuario, $password, $nivel,$url);
        
        $crawler = $this->client->request('GET', '/listado/missistemas');
        
        if(is_null($nivel)) {
                $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        }
        else {
            $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        }
       
    }
   /**
     * @dataProvider providerUsuarios
     */ 
    public function testListadoSistemas($usuario, $password, $nivel,$url)
    {
        
        $this->testLogin($usuario, $password, $nivel,$url);
        $crawler = $this->client->request('GET', '/listado/sistemas');
        
        if(is_null($nivel)) {
                $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        }
        else {
            $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        }

    }
    /**
     * @dataProvider providerUsuarios
     */
    public function testNuevaSimulacion($usuario, $password, $nivel,$url)
    {
        
        $this->testLogin($usuario, $password, $nivel,$url);
        
        $crawler = $this->client->request('GET', '/simulaciones/crear');
        
        if(is_null($nivel)) {
                $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        }
        else {
            $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        }

    }
    /**
     * @dataProvider providerUsuarios
     */
    public function testMisSimulaciones($usuario, $password, $nivel,$url)
    {
        
        $this->testLogin($usuario, $password, $nivel,$url);
        
        $crawler = $this->client->request('GET', '/listado/missimulaciones');
        
        if(is_null($nivel)) {
                $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        }
        else {
            $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        }

    }
    /**
     * @dataProvider providerUsuarios
     */
    public function testListadoSimulaciones($usuario, $password, $nivel,$url)
    {
        
        $this->testLogin($usuario, $password, $nivel,$url);
        
        $crawler = $this->client->request('GET', '/listado/simulaciones');
        
        if(is_null($nivel)) {
                $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        }
        else {
            $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        }
    }
    /**
     * @dataProvider providerUsuarios
     */
    public function testListadoEjecuciones($usuario, $password, $nivel,$url)
    {
        
        $this->testLogin($usuario, $password, $nivel,$url);
        
        $crawler = $this->client->request('GET', '/listado/misejecuciones');
        
       if(is_null($nivel)) {
                $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        }
        else {
            $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        }

    }
}

