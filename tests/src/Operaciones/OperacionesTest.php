<?php
namespace App\Operaciones;

use PHPUnit\Framework\TestCase;
use App\Entity\Entradas;


/**
 * Generated by PHPUnit_SkeletonGenerator on 2021-10-22 at 22:50:56.
 */
class OperacionesTest extends TestCase
{
    
    private $entradas;
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp(): void {
        $this->entradas = array();
        
        $entrada = new Entradas();
        $entrada->setValor(7);
        
        array_push($this->entradas, $entrada);
        
        $entrada = new Entradas();
        $entrada->setValor(3);
        
        array_push($this->entradas, $entrada);
        
         $entrada = new Entradas();
        $entrada->setValor(12);
        
        array_push($this->entradas, $entrada);
        
        $entrada = new Entradas();
        $entrada->setValor(7);
        
        array_push($this->entradas, $entrada);
        
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown(): void {
        
    }

    public function testOperacionMayor() {
        
        $object = new OperacionMayor();
        
        $valor = $object->operacion($this->entradas);
        
        $this->assertIsNumeric($valor);
        $this->assertEquals($valor, 12);
        
    }
    
    public function testOperacionMedia() {
        
        $object = new OperacionMedia();
        
        $valor = $object->operacion($this->entradas);
                
        $this->assertEquals($valor, 7.25);
        
    }
    
    public function testOperacionMediana() {
        
        $object = new OperacionMediana();
        
        $valor = $object->operacion($this->entradas);
        
        $this->assertIsNumeric($valor);
        $this->assertEquals($valor, 7);
        
    }
    
    public function testOperacionMenor() {
        
        $object = new OperacionMenor();
        
        $valor = $object->operacion($this->entradas);
        
        $this->assertIsNumeric($valor);
        $this->assertEquals($valor, 3);
        
    }
    
    public function testOperacionSuma() {
        
        $object = new OperacionSuma();
        
        $valor = $object->operacion($this->entradas);
        
        $this->assertIsNumeric($valor);
        $this->assertEquals($valor, 29);
        
    }
    
     public function testOperacionVotador() {
        
        $object = new OperacionVotador();
        
        $valor = $object->operacion($this->entradas);
        
        $this->assertIsNumeric($valor);
        $this->assertEquals($valor, 7);
        
    }
    
   
    
    
}
