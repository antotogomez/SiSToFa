
SiSToFa: Simulador de Sistemas Tolerantes a Fallos.
===================================================
Trabajo de Fin de Grado. Grado en Ingeniería Informática

Escuela Politecnica Superior.

Autor: Antonio Gómez López.

Directores: Jose Manuel Palomares. Fernando León García

Licencia
========

SiSToFa esta publicado bajo licencia AGPL v3

Funcionalidades.
================

Herramienta para permitir el analisis y simulación de sistemas tolerante a fallos en
el ámbito académico.

Herramienta de análisis y simulación basada en web.
Incluye las siguientes configuraciones de análisis y simulación:

- Componentes en serie.
- Componentes en paralelo.
- Componentes en configuraciones k-de-n.
- Utilización de votadores configurables de n entradas (mayoría, media, mediana, etc.)

Calculo de las siguientes métricas (una vez se le
proporcionen los parámetros necesarios en cada una de ellas):

- Fiabilidad.
- Tiempo Medio hasta un Fallo (MTTF)
- Tiempo Medio entre Fallos (MTBF)
- Disponibilidad

Gráficas que permitan el análisis posterior de las simulaciones. Permite la exportación de los resultados a formatos de ficheros que
permitan su análisis en aplicaciones externas.

Permite la simulación de los sistemas con respuestas interactivas
(activando o desactivando componentes, inyectando errores en algún componente), en su
caso.

Instalación
===========

Para instalar la aplicación, descargar el código desde gitlab:

```
git -clone https://gitlab.com/antotogomezlopez/sistofa.git
```

Una vez descargado, configurar en un servidor web con php habilitado el directorio de la aplicación. Para Apache HTTP sería asi:

```
Alias /SiSToFa "DIRECTORIO_DE_SISTOFA/web/"

<Directory "DIRECTORIO_DE_SISTOFA/web/">
    Options Indexes FollowSymLinks MultiViews
    AllowOverride all
    <ifDefine APACHE24>
	Require local
    </ifDefine>

    <ifDefine !APACHE24>
        Order Deny,Allow
        Allow from all
    </ifDefine>
</Directory>
```

Crear la base de datos de la aplicación. 


Para configurar el fichero de parametros de la aplicacion:

```
# mysql -u root -d sisit -p -i install/sistofa.sql
```

Contacto
========

Para solicitar información puede contactar con:   <a href="mailto://i52goloa@uco.es">i52goloa@uco.es</a>
